#ifndef ALF_DECIMAL_HXX
#define ALF_DECIMAL_HXX

#include <ext/atomicity.h>

#include "integer.hxx"

namespace alf {

class decimal {
public:
    // rounding modes.
    enum {
        NEAREST_TIES_TO_EVEN,
        NEAREST_TIES_AWAY_ZERO,
        NEAREST_TIES_TO_ZERO,
        DOWN,
        UP,
        TO_ZERO,
        AWAY_ZERO,
    };

    // number classification.
    enum {
        ZERO = 1,
        REGULAR = 2,
        INF = 3,
        QNAN = 4,
        SNAN = 5,

        POS = 0x40,
        NEG = 0x80,
        NINF = NEG | INF,
        NEGZERO = NEG | ZERO,
        PINF = POS | INF,
        PZERO = POS | ZERO,
        PREGULAR = POS | REGULAR,
        NREGULAR = NEG | REGULAR,
    };

	typedef unsigned int digit;
	typedef unsigned long long int dbl_digit;

    struct parse_info {
        // Note: If these strings are utf-8, the
        // number string can also be utf-8.
        // strings that are valid exponent markers
        // terminated by a NULL pointer.
        // default is { "e", "E", 0 };
        const char ** exp;
        // signs, sign[0] should be positive sign and
        // sign[1] should be negative sign.
        const char * sign[2];
        // separator.
        const char * sep;
        // decimal dot.
        const char * dot;
    };

    struct format_info {
        // signs, sign[0] should be positive sign and
        // sign[1] should be negative sign.
        const char * sign[2];
        // separator.
        const char * sep;
        // decimal dot.
        const char * dot;
        // exponent marker, usually "e" or "E",
        const char * exp;
        int ip_gsize; // integer part group size.
        int fp_gsize; // fraction part group size.
    };

    struct parse_data {
        // on output: a copy of the input pointer
        // where we start.
        const char * start;
        // on output: p is either were some error
        // occurred or it is the position where parsing
        // stopped.
        const char * p;
        // on input: Pointer to where parsing should end
        // can be 0 in which case we continue until
        // '\0' is detected on input.
        const char * end;
    };

	class builder {
	public:
		builder();
        builder(const parse_info & pi,
                    parse_data & pd,
                    const char * str);
        builder(const char * str, char ** endptr = 0);
        builder(const char * str, size_t sz, char ** endptr = 0);
		builder(const builder & b);
		builder(const integer::builder & b);
		builder(const decimal & d);
		builder(const integer & k);
		builder(int k);
		builder(unsigned int k);
		builder(long k);
		builder(unsigned long k);
		builder(long long k);
		builder(unsigned long long k);
		builder(float x);
		builder(double x);
		builder(long double x);
		builder(const digit * xp, size_t xn,
					ssize_t xs, ssize_t xx, int kls);
		builder(const digit * xp, ssize_t xs, ssize_t xx, int kls);
		~builder();

        builder & mkclass();
		builder & ensure(size_t k);
		builder & length_at_least(size_t k);
		builder & exp_at_most(ssize_t xx);
        builder & normalize();

        builder & setqnan() { n = 0; s = x = 0; klass = QNAN; return *this; }
        builder & setsnan() { n = 0; s = x = 0; klass = SNAN; return *this; }
        builder & setpinf() { n = 0; s = x = 0; klass = PINF; return *this; }
        builder & setninf() { n = 0; s = x = 0; klass = NINF; return *this; }
        builder & setnzero()
        { n = 1; s = -1; x = 0; *p = 0; klass = NEGZERO; return *this; }
        builder & setpzero()
        { n = 1; s = 1; x = 0; *p = 0; klass = PZERO; return *this; }
        builder & clear() { return setpzero(); }
        builder & add_expon(ssize_t xx) { x += xx; return *this; }
        builder & set_expon(ssize_t xx) { x = xx; return *this; }

        builder & parse(const parse_info & pi,
                            parse_data & pd,
                            const char * str);
        builder & parse(const char * str, char ** endptr = 0);
        builder & parse (const char * str, size_t sz,
                            char ** endptr = 0);
		builder & assign(const builder & b);
		builder & assign(const integer::builder & b);
		builder & assign(const decimal & d);
		builder & assign(const integer & k);
		builder & assign(int k);
		builder & assign(unsigned int k);
		builder & assign(long k);
		builder & assign(unsigned long k);
		builder & assign(long long k);
		builder & assign(unsigned long long k);
		builder & assign(float x);
		builder & assign(double x);
		builder & assign(long double x);
		builder & assign(const digit * xp, size_t xn,
							ssize_t xs, ssize_t xx, int kls);
		builder & assign(const digit * xp,
							ssize_t xs, ssize_t xx, int kls);

        bool get(integer & k) const;
        bool get(integer::builder & k) const;
        bool get(signed char & k) const;
        bool get(short & k) const;
        bool get(int & k) const;
        bool get(long & k) const;
        bool get(long long & k) const;
        bool get(unsigned char & k) const;
        bool get(unsigned short & k) const;
        bool get(unsigned int & k) const;
        bool get(unsigned long & k) const;
        bool get(unsigned long long int & k) const;
        bool get(char & k) const;
        bool get(bool & k) const;
        bool get(float & k) const;
        bool get(double & k) const;
        bool get(long double & k) const;

		digit add_(const digit * ap, size_t an);

        static void add_(digit * cp, size_t cn, const digit * ap, size_t an,
                            const digit * bp, size_t bn);
        static void sub_(digit * cp, size_t cn, const digit * ap, size_t an,
                            const digit * bp, size_t bn);

		builder & add_(const digit * ap, size_t an, ssize_t as, ssize_t ax,
						const digit * bp, size_t bn, ssize_t bs, ssize_t bx,
						int neg);
        digit sub_(const digit * ap, size_t an);
		builder & sub_(const digit * ap, size_t an, ssize_t as, ssize_t ax,
						const digit * bp, size_t bn, ssize_t bs, ssize_t bx,
						int neg);
		builder & neg_();

        static size_t normalize___(const digit * p, size_t n)
        { return integer::builder::normalize_(p, n); }

        static bool is_zero_(const digit * p, size_t n)
        {
            while (n > 0) if (p[--n] != 0) return false;
            return true;
        }

        size_t normalize__() const { return normalize___(p, n); }
        builder & normalize_() { n = normalize___(p, n); }

		builder & add(const digit * ap, size_t an,
                        ssize_t as, ssize_t ax, int akls,
                        const digit * bp, size_t bn,
                        ssize_t bs, ssize_t bx, int bkls);

        static int cmp(const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls,
                            const digit * bp, size_t bn,
                            ssize_t bs, ssize_t bx, int bkls);

        int cmp(const digit * ap, size_t an, ssize_t as,
                    ssize_t ax, int akls) const
        {
            return cmp(p, n, s, x, klass, ap, an, as, ax, akls);
        }

        int cmp(const builder & b) const
        {
            return cmp(p, n, s, x, klass, b.p, b.n, b.s, b.x, b.klass);
        }

        int cmp(const decimal & d) const
        {
            return cmp(p, n, x, s, klass, d.data(),
                        d.length(), d.slength(), d.expon(), d.klass());
        }

        builder & sub(const digit * ap, size_t an,
                        ssize_t as, ssize_t ax, int akls,
                        const digit * bp, size_t bn,
                        ssize_t bs, ssize_t bx, int bkls);
        builder & neg();
        builder & abs();
        builder & snan_();
        bool isneg() const;
        bool ispos() const;
        bool iszero() const;
        bool isinf() const;
        bool isnan() const;
        bool isqnan() const { return klass == QNAN; }
        bool issnan() const { return klass == SNAN; }
        bool ispzero() const { return klass == PZERO; }
        bool isnzero() const { return klass == NEGZERO; }

        // *this = *this * f + z;
        builder & mul_(digit f, digit z);

        builder & muladd_(size_t k, const digit * ap, size_t an, digit b);
        builder & k_muladd_(size_t k,
                            const digit * ap, size_t an,
                            const digit * bp, size_t bn);
        builder & q_muladd_(size_t k,
                            const digit * ap, size_t an,
                            const digit * bp, size_t bn);
        builder & s_muladd_(size_t k,
                            const digit * ap, size_t an,
                            const digit * bp, size_t bn);
        builder & muladd_(size_t k,
                            const digit * ap, size_t an,
                            const digit * bp, size_t bn);

        builder & mul_(const digit * ap, size_t an,
                        const digit * bp, size_t bn,
                        int neg);

        builder & mul_(const digit * ap, size_t an, ssize_t ax,
                        const digit * bp, size_t bn, ssize_t bx,
                        int neg);

        builder & mul(const digit * ap, size_t an,
                        ssize_t as, ssize_t ax, int akls, digit b);

        builder & mul(const digit * ap, size_t an,
                        ssize_t as, ssize_t ax, int akls,
                        const digit * bp, size_t bn,
                        ssize_t bs, ssize_t bx, int bkls);

        // *this = *this * R**j.
        builder & mulR(size_t j);

        static int cmp__(const digit * ap, size_t an,
                            const digit * bp, size_t bn);

		// *this = *this / f, return *this % f.
		digit div_(digit f);

        builder & div_(size_t prec, int rnd,
                        const digit * ap, size_t an, digit b);

        builder & div_(size_t prec, int rnd,
                        const digit * ap, size_t an,
                        const digit * bp, size_t bn);

        builder & div_(size_t prec, int rnd,
                        const digit * ap, size_t an, ssize_t ax,
                        const digit * bp, size_t bn, ssize_t bx,
                        int neg);

		builder & div(size_t prec, int rnd,
                        const digit * ap, size_t an,
                        ssize_t as, ssize_t ax, int akls,
						const digit * bp, size_t bn,
                        ssize_t bs, ssize_t bx, int bkls);

        builder & div(size_t prec, int rnd,
                        const digit * ap, size_t an,
                        ssize_t as, ssize_t ax, int akls,
                        digit b);

		builder & frem(size_t prec, int rnd,
                        const digit * ap, size_t an,
                        ssize_t as, ssize_t ax, int akls,
						const digit * bp, size_t bn,
                        ssize_t bs, ssize_t bx, int bkls);

        // compute integer part.
        builder & intp(int rnd,
                        const digit * ap, size_t an,
                        ssize_t as, ssize_t ax, int akls);

        // compute fraction.
        builder & fractionp(size_t prec, int rnd,
                                const digit * ap, size_t an,
                                ssize_t as, ssize_t ax, int akls);

        // round to prec decimals, round down.
        builder & floor(const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls);

        builder & ceil(const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls);

        builder & round(size_t prec, int rnd,
                            const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls);

		builder & pow(unsigned int j);
		builder & pow(int j);
		builder & pow(const digit * ap, size_t an, ssize_t as, ssize_t ax,
						const digit * bp, size_t bn, ssize_t bs, ssize_t bx);

        static int cmp(const digit * ap, size_t an, ssize_t ax,
                        const digit * bp, size_t bn, ssize_t bx);

        size_t length() const { return n; }
        ssize_t slength() const { return s; }
        ssize_t expon() const { return x; }
        int kls() const { return klass; }
        const digit * data() const { return p; }

        builder & sqrt(const builder & b, size_t prec);
        builder & sqrt(size_t prec) { return sqrt(*this, prec); }

	private:

		friend class decimal;

		enum {
			INI_SIZE = 16,
			LARGE_SIZE = 4096,
			R = 1000000000,
			N = 9, // R == 10**N
		};

		digit * mk() const
        { return decimal::mk(p, n, x, klass); }

		digit arr[INI_SIZE];
		digit * p;
		size_t n;
		size_t m;
		ssize_t s;
		ssize_t x;
		int klass;
	};

	decimal() : dataptr(pzerov()) { }
	decimal(const builder & b) : dataptr(mk(b)) { }
	decimal(const integer::builder & b) : dataptr(mk(b)) { }
	decimal(const decimal & d) : dataptr(incref(d.dataptr.d)) { }
	decimal(const integer & k) : dataptr(mk(k)) { }
	decimal(int k) : dataptr(mk(k)) { }
	decimal(unsigned int k) : dataptr(mk(k)) { }
	decimal(long int k) : dataptr(mk(k)) { }
	decimal(unsigned long int k) : dataptr(mk(k)) { }
	decimal(long long int k) : dataptr(mk(k)) { }
	decimal(unsigned long long int k) : dataptr(mk(k)) { }
	decimal(float x) : dataptr(mk(x)) { }
	decimal(double x) : dataptr(mk(x)) { }
	decimal(long double x) : dataptr(mk(x)) { }

	decimal(const digit * xp, size_t xn, ssize_t xx, int kls)
        : dataptr(mk(xp, xn, xx, kls))
    { }

	decimal(const digit * xp, ssize_t xs, ssize_t xx, int kls)
        : dataptr(mk(xp, size_t(xs >= 0 ? xs : -xs), xx, kls))
    { }

	~decimal() { decref(dataptr.d); }

    bool get(integer & k) const;
    bool get(integer::builder & k) const;
    bool get(signed char & k) const;
    bool get(short & k) const;
    bool get(int & k) const;
    bool get(long & k) const;
    bool get(long long & k) const;
    bool get(unsigned char & k) const;
    bool get(unsigned short & k) const;
    bool get(unsigned int & k) const;
    bool get(unsigned long & k) const;
    bool get(unsigned long long int & k) const;
    bool get(char & k) const;
    bool get(bool & k) const;
    bool get(float & k) const;
    bool get(double & k) const;
    bool get(long double & k) const;

    decimal & operator = (const decimal & d)
    { assign(d.dataptr.d); return *this; }

    decimal & assign(const builder & b)
    { decref(dataptr.d); dataptr.d = b.mk(); return *this; }

    decimal & operator = (const builder & b) { return assign(b); }
    decimal & operator = (integer k) { builder B(k); return assign(B); }
    decimal & operator = (int k) { builder B(k); return assign(B); }
    decimal & operator = (unsigned int k) { builder B(k); return assign(B); }
    decimal & operator = (long int k) { builder B(k); return assign(B); }
    decimal & operator = (long long int k) { builder B(k); return assign(B); }
    decimal & operator = (float x) { builder B(x); return assign(B); }
    decimal & operator = (double x) { builder B(x); return assign(B); }
    decimal & operator = (long double x) { builder B(x); return assign(B); }

    decimal & operator = (const integer::builder & b)
    { builder B(b); return assign(B); }

    decimal & operator = (unsigned long int k)
    { builder B(k); return assign(B); }

    decimal & operator = (unsigned long long int k)
    { builder B(k); return assign(B); }

    decimal operator - () const
    {
        builder B(dataptr.d, length(), expon(), klass());
        B.neg();
        return decimal(B);
    }

    decimal & operator + () { return *this; }

    decimal & operator += (const decimal & d)
    {
        builder B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                d.dataptr.d, d.length(), d.slength(), d.expon(),
                d.klass());
        return assign(B);
    }

    decimal & operator += (const builder & b)
    {
        builder B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (const integer & k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (const integer::builder & k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (int k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (unsigned int k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (long int k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (unsigned long int k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (long long int k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (unsigned long long int k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (float k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (double k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator += (long double k)
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal operator + (const decimal & d) const
    {
        builder B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                d.dataptr.d, d.length(), d.slength(), d.expon(),
                d.klass());
        return decimal(B);
    }

    decimal operator + (const builder & b) const
    {
        builder B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (const integer & k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (const integer::builder & k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (int k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (unsigned int k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (long int k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (unsigned long int k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (long long int k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (unsigned long long int k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (float k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (double k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator + (long double k) const
    {
        builder b(k), B;
        B.add(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal & operator -= (const decimal & d)
    {
        builder B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                d.dataptr.d, d.length(), d.slength(), d.expon(),
                d.klass());
        return assign(B);
    }

    decimal & operator -= (const builder & b)
    {
        builder B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (const integer & k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (const integer::builder & k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (int k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (unsigned int k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (long int k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (unsigned long int k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (long long int k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (unsigned long long int k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (float k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (double k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator -= (long double k)
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal operator - (const decimal & d) const
    {
        builder B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                d.dataptr.d, d.length(), d.slength(), d.expon(),
                d.klass());
        return decimal(B);
    }

    decimal operator - (const builder & b) const
    {
        builder B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (const integer & k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (const integer::builder & k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (int k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (unsigned int k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (long int k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (unsigned long int k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (long long int k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (unsigned long long int k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (float k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (double k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator - (long double k) const
    {
        builder b(k), B;
        B.sub(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal & operator *= (const decimal & d)
    {
        builder B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                d.dataptr.d, d.length(), d.slength(), d.expon(),
                d.klass());
        return assign(B);
    }

    decimal & operator *= (const builder & b)
    {
        builder B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (const integer & k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (const integer::builder & k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (int k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (unsigned int k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (long int k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (unsigned long int k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (long long int k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (unsigned long long int k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (float k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (double k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & operator *= (long double k)
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal operator * (const decimal & d) const
    {
        builder B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                d.dataptr.d, d.length(), d.slength(), d.expon(),
                d.klass());
        return decimal(B);
    }

    decimal operator * (const builder & b) const
    {
        builder B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (const integer & k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (const integer::builder & k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (int k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (unsigned int k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (long int k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (unsigned long int k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (long long int k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (unsigned long long int k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (float k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (double k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator * (long double k) const
    {
        builder b(k), B;
        B.mul(dataptr.d, length(), slength(), expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    static size_t prec;
    static int rnd;
    static parse_info parseinfo;

    decimal & diveq(const decimal & d, size_t prec, int rnd)
    {
        builder B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(),
                d.dataptr.d, d.length(), d.slength(),
                d.expon(), d.klass());
        return assign(B);
    }

    decimal & diveq(const builder & b, size_t prec, int rnd)
    {
        builder B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(const integer & k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(const integer::builder & k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(int k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(unsigned int k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(long int k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(unsigned long int k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(long long int k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(unsigned long long int k,
                        size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(float k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(double k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal & diveq(long double k, size_t prec, int rnd)
    {
        builder b(k), B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(), b.p, b.n, b.s, b.x, b.klass);
        return assign(B);
    }

    decimal div(const decimal & d, size_t prec, int rnd) const
    {
        builder B;
        B.div(prec, rnd, dataptr.d, length(), slength(),
                expon(), klass(),
                d.dataptr.d, d.length(), d.slength(),
                d.expon(), d.klass());
        return decimal(B);
    }

    decimal div(const builder & b, size_t prec, int rnd) const
    {
        builder B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(const integer & k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(const integer::builder & k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(int k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(unsigned int k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(long int k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(unsigned long int k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(long long int k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(unsigned long long int k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(float k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(double k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal div(long double k, size_t prec, int rnd) const
    {
        builder b(k), B;
        B.div(prec, rnd,
                dataptr.d, length(), slength(),
                expon(), klass(),
                b.p, b.n, b.s, b.x, b.klass);
        return decimal(B);
    }

    decimal operator /= (const decimal & d)
    { return diveq(d, prec, rnd); }

    decimal operator /= (const builder & b)
    { return diveq(b, prec, rnd); }

    decimal operator /= (const integer & k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (const integer::builder & k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (int k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (unsigned int k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (long int k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (unsigned long int k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (long long int k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (unsigned long long int k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (float k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (double k)
    { return diveq(k, prec, rnd); }

    decimal operator /= (long double k)
    { return diveq(k, prec, rnd); }

    decimal operator / (const decimal & d) const
    { return div(d, prec, rnd); }

    decimal operator / (const builder & b) const
    { return div(b, prec, rnd); }

    decimal operator / (const integer & k) const
    { return div(k, prec, rnd); }

    decimal operator / (const integer::builder & k) const
    { return div(k, prec, rnd); }

    decimal operator / (int k) const
    { return div(k, prec, rnd); }

    decimal operator / (unsigned int k) const
    { return div(k, prec, rnd); }

    decimal operator / (long int k) const
    { return div(k, prec, rnd); }

    decimal operator / (unsigned long int k) const
    { return div(k, prec, rnd); }

    decimal operator / (long long int k) const
    { return div(k, prec, rnd); }

    decimal operator / (unsigned long long int k) const
    { return div(k, prec, rnd); }

    decimal operator / (float k) const
    { return div(k, prec, rnd); }

    decimal operator / (double k) const
    { return div(k, prec, rnd); }

    decimal operator / (long double k) const
    { return div(k, prec, rnd); }

    int cmp(const decimal & d) const
    {
        return builder::cmp(dataptr.d, length(), slength(),
                                expon(), klass(),
                                d.dataptr.d, d.length(),
                                d.slength(), d.expon(),
                                d.klass());
    }

    int cmp(const builder & b) const
    {
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(const integer & k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(const integer::builder & k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(int k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(unsigned int k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(long int k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(unsigned long int k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(long long int k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(unsigned long long int k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(float k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(double k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    int cmp(long double k) const
    {
        builder b(k);
        return builder::cmp(dataptr.d, length(), slength(),
                            expon(), klass(),
                            b.p, b.n, b.s, b.x, b.klass);
    }

    bool operator == (const decimal & d) const
    { return cmp(d) == 0; }

    bool operator == (const builder & b) const
    { return cmp(b) == 0; }

    bool operator == (const integer & k) const
    { return cmp(k) == 0; }

    bool operator == (const integer::builder & b) const
    { return cmp(b) == 0; }

    bool operator == (int k) const
    { return cmp(k) == 0; }

    bool operator == (unsigned int k) const
    { return cmp(k) == 0; }

    bool operator == (long int k) const
    { return cmp(k) == 0; }

    bool operator == (unsigned long int k) const
    { return cmp(k) == 0; }

    bool operator == (long long int k) const
    { return cmp(k) == 0; }

    bool operator == (unsigned long long int k) const
    { return cmp(k) == 0; }

    bool operator == (float x) const
    { return cmp(x) == 0; }

    bool operator == (double x) const
    { return cmp(x) == 0; }

    bool operator == (long double x) const
    { return cmp(x) == 0; }

    bool operator != (const decimal & d) const
    { return cmp(d) != 0; }

    bool operator != (const builder & b) const
    { return cmp(b) != 0; }

    bool operator != (const integer & k) const
    { return cmp(k) != 0; }

    bool operator != (const integer::builder & b) const
    { return cmp(b) != 0; }

    bool operator != (int k) const
    { return cmp(k) != 0; }

    bool operator != (unsigned int k) const
    { return cmp(k) != 0; }

    bool operator != (long int k) const
    { return cmp(k) != 0; }

    bool operator != (unsigned long int k) const
    { return cmp(k) != 0; }

    bool operator != (long long int k) const
    { return cmp(k) != 0; }

    bool operator != (unsigned long long int k) const
    { return cmp(k) != 0; }

    bool operator != (float x) const
    { return cmp(x) != 0; }

    bool operator != (double x) const
    { return cmp(x) != 0; }

    bool operator != (long double x) const
    { return cmp(x) != 0; }

    bool operator >= (const decimal & d) const
    { return cmp(d) >= 0; }

    bool operator >= (const builder & b) const
    { return cmp(b) >= 0; }

    bool operator >= (const integer & k) const
    { return cmp(k) >= 0; }

    bool operator >= (const integer::builder & b) const
    { return cmp(b) >= 0; }

    bool operator >= (int k) const
    { return cmp(k) >= 0; }

    bool operator >= (unsigned int k) const
    { return cmp(k) >= 0; }

    bool operator >= (long int k) const
    { return cmp(k) >= 0; }

    bool operator >= (unsigned long int k) const
    { return cmp(k) >= 0; }

    bool operator >= (long long int k) const
    { return cmp(k) >= 0; }

    bool operator >= (unsigned long long int k) const
    { return cmp(k) >= 0; }

    bool operator >= (float x) const
    { return cmp(x) >= 0; }

    bool operator >= (double x) const
    { return cmp(x) >= 0; }

    bool operator >= (long double x) const
    { return cmp(x) >= 0; }

    bool operator > (const decimal & d) const
    { return cmp(d) > 0; }

    bool operator > (const builder & b) const
    { return cmp(b) > 0; }

    bool operator > (const integer & k) const
    { return cmp(k) > 0; }

    bool operator > (const integer::builder & b) const
    { return cmp(b) > 0; }

    bool operator > (int k) const
    { return cmp(k) > 0; }

    bool operator > (unsigned int k) const
    { return cmp(k) > 0; }

    bool operator > (long int k) const
    { return cmp(k) > 0; }

    bool operator > (unsigned long int k) const
    { return cmp(k) > 0; }

    bool operator > (long long int k) const
    { return cmp(k) > 0; }

    bool operator > (unsigned long long int k) const
    { return cmp(k) > 0; }

    bool operator > (float x) const
    { return cmp(x) > 0; }

    bool operator > (double x) const
    { return cmp(x) > 0; }

    bool operator > (long double x) const
    { return cmp(x) > 0; }

    bool operator <= (const decimal & d) const
    { int q = cmp(d); return q == 0 || q == -1; }

    bool operator <= (const builder & b) const
    { int q = cmp(b); return q == 0 || q == -1; }

    bool operator <= (const integer & k) const
    { int q = cmp(k); return q == 0 || q == -1; }

    bool operator <= (const integer::builder & b) const
    { int q = cmp(b); return q == 0 || q == -1; }

    bool operator <= (int k) const
    { int q = cmp(k); return q == 0 || q == -1; }

    bool operator <= (unsigned int k) const
    { int q = cmp(k); return q == 0 || q == -1; }

    bool operator <= (long int k) const
    { int q = cmp(k); return q == 0 || q == -1; }

    bool operator <= (unsigned long int k) const
    { int q = cmp(k); return q == 0 || q == -1; }

    bool operator <= (long long int k) const
    { int q = cmp(k); return q == 0 || q == -1; }

    bool operator <= (unsigned long long int k) const
    { int q = cmp(k); return q == 0 || q == -1; }

    bool operator <= (float x) const
    { int q = cmp(x); return q == 0 || q == -1; }

    bool operator <= (double x) const
    { int q = cmp(x); return q == 0 || q == -1; }

    bool operator <= (long double x) const
    { int q = cmp(x); return q == 0 || q == -1; }

    bool operator < (const decimal & d) const
    { return cmp(d) == -1; }

    bool operator < (const builder & b) const
    { return cmp(b) == -1; }

    bool operator < (const integer & k) const
    { return cmp(k) == -1; }

    bool operator < (const integer::builder & b) const
    { return cmp(b) == -1; }

    bool operator < (int k) const
    { return cmp(k) == -1; }

    bool operator < (unsigned int k) const
    { return cmp(k) == -1; }

    bool operator < (long int k) const
    { return cmp(k) == -1; }

    bool operator < (unsigned long int k) const
    { return cmp(k) == -1; }

    bool operator < (long long int k) const
    { return cmp(k) == -1; }

    bool operator < (unsigned long long int k) const
    { return cmp(k) == -1; }

    bool operator < (float x) const
    { return cmp(x) == -1; }

    bool operator < (double x) const
    { return cmp(x) == -1; }

    bool operator < (long double x) const
    { return cmp(x) == -1; }

    const digit * data() const throw() { return dataptr.d; }
    ssize_t slength() const { return Rep()->len; }

    size_t length() const
    {
        ssize_t s = slength();
        return s >= 0 ? s : -s;
    }

    ssize_t expon() const { return Rep()->x; }
    int klass() const { return Rep()->klass; }

    bool isneg() const;
    bool ispos() const;
    bool isinf() const;
    bool ispinf() const { return Rep()->klass == PINF; }
    bool isninf() const { return Rep()->klass == NINF; }
    bool ispzero() const { return Rep()->klass == PZERO; }
    bool isnzero() const { return Rep()->klass == NEGZERO; }
    bool iszero() const;
    bool isstrictpos() const;
    bool isstrictneg() const;
    bool isqnan() const { return Rep()->klass == QNAN; }
    bool issnan() const { return Rep()->klass == SNAN; }
    bool isnan() const;
    bool isregpos() const { return Rep()->klass == PREGULAR; }
    bool isregneg() const { return Rep()->klass == NREGULAR; }

    static decimal snanres();

    decimal sqrt_(size_t prec) const;

private:

    typedef std::allocator<digit> _Alloc;

	/*
	** this implementation stores the integer in a string object.
	** just so that we can make use of std::string's ref counting
	** etc. Just because we are too lazy to implement our own.
	*/

    struct rep_base {
        _Atomic_word r;
        ssize_t len;
        ssize_t x;
        int klass;
    };

    struct rep : rep_base {
        bool is_leaked() { return r < 0; }
        bool is_shared() { return r > 0; }
        void set_leaked() { r = -1; }
        void set_sharable() { r = 0; }

        void set_length_sign_and_sharable(ssize_t n)
        {
            set_sharable();
            len = n;
        }

        digit * data() throw() { return reinterpret_cast<digit *>(this + 1); }

        digit * grab(const _Alloc & alloc1, const _Alloc & alloc2)
        {
            return (! is_leaked() && alloc1 == alloc2) ?
                        copy() : clone(alloc1);
        }

        static rep *
        create(ssize_t n, ssize_t sign, const _Alloc & alloc);

        void dispose(const _Alloc & a)
        {
            if (__gnu_cxx::__exchange_and_add_dispatch(& r, -1) < 0)
                delete [] (char *)this;
        }

        digit * copy() throw()
        {
            __gnu_cxx::__atomic_add_dispatch(& r, 1);
            return data();
        }

        digit * clone(const _Alloc & a, ssize_t res = 0);

        static rep * getrep(digit * d) { return (rep *)d - 1; }
        static digit * getdig(rep * r) { return (digit *)(r + 1); }
        digit * getdig() { return (digit *)(this + 1); }

        digit * mkrep(ssize_t xlen, ssize_t xx, int kls)
        {
            r = 0;
            len = xlen;
            x = xx;
            klass = kls;
            return (digit *)(this + 1);
        }
    };

    struct repzero : rep {
    	digit z;

        digit * mkrepz(digit xd, ssize_t xlen, ssize_t xx, int kls)
        {
            digit * ret = rep::mkrep(xlen, xx, kls);
            z = xd;
            return ret;
        }
    };

    struct _Alloc_hider : _Alloc {
        _Alloc_hider(digit * dd, const _Alloc & a = _Alloc()) throw()
            : _Alloc(a), d(dd)
        { }

        digit * d;
    };

    digit * wdata() const throw() { return dataptr.d; }
    digit * wdata(digit * d) throw() { return dataptr.d = d; }
    rep * Rep() const { return reinterpret_cast<rep *>(wdata()) - 1; }

    static void rm(rep * r);

    void decref(digit * & p)
    {
        rep * r = rep::getrep(p);
        p = 0;
        if (__gnu_cxx::__exchange_and_add_dispatch(& r->r, -1) < 0)
            rm(r);
    }

    digit * incref(digit * p)
    {
        __gnu_cxx::__atomic_add_dispatch(& rep::getrep(p)->r, 1);
        return p;
    }

    static digit * mk() { return pzero_; }
    static digit * mk(const digit * d, size_t dn, ssize_t dx, int kls);

    static digit * mk(const digit * d, ssize_t ds, ssize_t dx, int kls)
    { return mk(d, size_t(ds < 0 ? -ds : ds), dx, kls); }

    static digit * mk(const builder & b)
    { return mk(b.p, b.n, b.x, b.klass); }

    static digit * mk(const integer::builder & b);
    static digit * mk(const integer & k);
    static digit * mk(int k);
    static digit * mk(unsigned int k);
    static digit * mk(long int k);
    static digit * mk(unsigned long int k);
    static digit * mk(long long int k);
    static digit * mk(unsigned long long int k);
    static digit * mk(float x);
    static digit * mk(double x);
    static digit * mk(long double x);
    decimal & assign(builder & b);

    decimal & assign(digit * q)
    {
        if (dataptr.d != q) {
            decref(dataptr.d);
            dataptr.d = incref(q);
        }
        return *this;
    }

    decimal(const digit * d, ssize_t ds, ssize_t dx)
        : dataptr(mk(d, size_t(ds < 0 ? -ds : ds), ds, dx), _Alloc())
    { }

    decimal(const digit * d, size_t dn, ssize_t ds, ssize_t dx)
        : dataptr(mk(d, dn, ds, dx), _Alloc())
    { }

    decimal(digit * d) : dataptr(incref(d), _Alloc()) { }

    static void initv();
	static digit * qnanv();
    static digit * snanv();
	static digit * pinfv();
	static digit * ninfv();
	static digit * pzerov();
	static digit * nzerov();

    static rep qnanr_;
    static rep snanr_;
    static rep ninfr_;
    static rep pinfr_; 
    static repzero nzeror_;
    static repzero pzeror_;
    static digit * qnan_;
	static digit * snan_;
	static digit * ninf_;
	static digit * pinf_;
	static digit * nzero_;
	static digit * pzero_;

    mutable _Alloc_hider dataptr;
};

inline
decimal
operator + (const decimal::builder & b, const decimal & d)
{
    decimal::builder B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (const integer & k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (const integer::builder & k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (unsigned int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (unsigned long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (long long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (unsigned long long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (float k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (double k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator + (long double k, const decimal & d)
{
    decimal::builder b(k), B;
    B.add(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (const decimal::builder & b, const decimal & d)
{
    decimal::builder B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (const integer & k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (const integer::builder & k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (unsigned int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (unsigned long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (long long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (unsigned long long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (float k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (double k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator - (long double k, const decimal & d)
{
    decimal::builder b(k), B;
    B.sub(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (const decimal::builder & b, const decimal & d)
{
    decimal::builder B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (const integer & k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (const integer::builder & k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (unsigned int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (unsigned long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (long long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (unsigned long long int k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (float k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (double k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

inline
decimal
operator * (long double k, const decimal & d)
{
    decimal::builder b(k), B;
    B.mul(b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return decimal(B);
}

}

inline
alf::decimal
div(const alf::decimal::builder & b, const alf::decimal & d,
        size_t prec, int rnd)
{
    alf::decimal::builder B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(const alf::integer & k, const alf::decimal & d,
        size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(const alf::integer::builder & k, const alf::decimal & d,
        size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(int k, const alf::decimal & d, size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(unsigned int k, const alf::decimal & d, size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(long int k, const alf::decimal & d, size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(unsigned long int k, const alf::decimal & d,
        size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(long long int k, const alf::decimal & d,
        size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(unsigned long long int k, const alf::decimal & d,
        size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(float k, const alf::decimal & d, size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(double k, const alf::decimal & d, size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

inline
alf::decimal
div(long double k, const alf::decimal & d, size_t prec, int rnd)
{
    alf::decimal::builder b(k), B;
    B.div(prec, rnd, b.data(), b.length(), b.slength(), b.expon(), b.kls(),
            d.data(), d.length(), d.slength(), d.expon(), d.klass());
    return alf::decimal(B);
}

namespace alf {

inline
decimal
operator / (const decimal::builder & b, const decimal & d)
{ return div(b, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (const integer & k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (const integer::builder & b, const decimal & d)
{ return div(b, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (int k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (unsigned int k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (long int k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (unsigned long int k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (long long int k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (unsigned long long int k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (float k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (double k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
operator / (long double k, const decimal & d)
{ return div(k, d, decimal::prec, decimal::rnd); }

inline
decimal
sqrt(decimal x, size_t prec = alf::decimal::prec)
{ return x.sqrt_(prec); }

}

#endif
