
//#include <bits/memoryfwd.h>
#include <bits/allocator.h>

#include <limits.h>

#include <stdio.h>
#include <string.h>

#include "integer.hxx"
#include "decimal.hxx"

template <typename T, int x, unsigned int p> 
struct tpow;

template <typename T, int x>
struct tpow<T, x, 0> {
    static constexpr T value = 1.0;
};

template <typename T, int x, unsigned int p>
struct tpow {
    static constexpr T vv = tpow<T, x, (p>>1)>::value;
    static constexpr T value = vv*vv*T(p & 1 ? x : 1);
};

// static
alf::integer::digit * alf::integer::builder::small_nums[SMALL_NUMS_SIZE];

// ============================
// integer

alf::integer::digit *
alf::integer::mk(const digit * d, size_t dn, ssize_t ds)
{
    enum {
        SMALL_NUMS_SIZE = builder::SMALL_NUMS_SIZE,
        SMALL_NUMS_BIAS = builder::SMALL_NUMS_BIAS,
        Z = builder::SMALL_NUMS_SIZE - builder::SMALL_NUMS_BIAS,
    };

    int x = -1;
    dn = builder::normalize_(d, dn);
    digit v;
    digit * q = 0;

    switch (dn) {
    case 0: x = SMALL_NUMS_BIAS; break;
    case 1:
        v = *d;
        if (ds < 0) {
            if (v <= SMALL_NUMS_BIAS) x = SMALL_NUMS_BIAS - v;
        } else if (v < Z) x = v + SMALL_NUMS_BIAS;
        break;
    }
    if (x >= 0 && (q = builder::small_nums[x]) != 0)
        return incref(q);

    size_t sz = sizeof(rep) + dn*sizeof(digit);
    char * p = new char[sz];
    rep * r = (rep *)p;
    q = (digit *)(r + 1);
    ssize_t s = (ssize_t)(ds < 0 ? -dn : dn);
    r->r = 0;
    r->len = s;
    if (dn) memcpy(q, d, dn*sizeof(digit));
    if (x >= 0)
        builder::small_nums[x] = incref(q);
    return q;
}

alf::integer::digit * alf::integer::mk(const digit * d, ssize_t ds)
{
    return mk(d, size_t(ds < 0 ? -ds : ds), ds);
}

alf::integer::digit * alf::integer::mk()
{
    return mk(0, 0, 0);
}

alf::integer::digit * alf::integer::mk(const builder & b)
{ return mk(b.a, b.n, b.s); }

alf::integer::digit * alf::integer::mk(int k)
{
    builder b(k);
    return mk(b.a, b.n, b.s);
}

alf::integer::digit * alf::integer::mk(unsigned int k)
{
    builder b(k);
    return mk(b.a, b.n, b.s);
}

alf::integer::digit * alf::integer::mk(long int k)
{
    builder b(k);
    return mk(b.a, b.n, b.s);
}

alf::integer::digit * alf::integer::mk(unsigned long int k)
{
    builder b(k);
    return mk(b.a, b.n, b.s);
}

alf::integer::digit * alf::integer::mk(long long int k)
{
    builder b(k);
    return mk(b.a, b.n, b.s);
}

alf::integer::digit * alf::integer::mk(unsigned long long int k)
{
    builder b(k);
    return mk(b.a, b.n, b.s);
}

alf::integer::digit *
alf::integer::mk(const decimal & d)
{
    builder b;
    if (b.assign(d) < 0) return mk();
    return mk(b);
}

alf::integer::digit *
alf::integer::mk(float k)
{
    builder b;
    if (b.assign(k) < 0) return mk();
    return mk(b);
}

alf::integer::digit *
alf::integer::mk(double k)
{
    builder b;
    if (b.assign(k) < 0) return mk();
    return mk(b);
}

alf::integer::digit *
alf::integer::mk(long double k)
{
    builder b;
    if (b.assign(k) < 0) return mk();
    return mk(b);
}

bool
alf::integer::get(char & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(signed char & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(short & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(int & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(long & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(long long & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(unsigned char & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(unsigned short & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(unsigned int & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(unsigned long & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(unsigned long long & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(float & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(double & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

bool
alf::integer::get(long double & x) const
{
    builder b(data(), u_len(), s_len());
    return b.get(x);
}

alf::integer::digit *
alf::integer::assign(builder & b)
{
    digit * q = mk(b);
    if (q != dataptr.d) {
        decref(dataptr.d);
        dataptr.d = incref(q);
    }
    decref(q);
    return dataptr.d;
}

alf::integer &
alf::integer::operator += (const integer & k)
{
    builder b(*this);
    b.add(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator += (const builder & bb)
{
    builder b(*this);
    b.add(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator += (int k)
{
    builder b(*this);
    b.add(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator += (unsigned int k)
{
    builder b(*this);
    b.add(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator += (long int k)
{
    builder b(*this);
    b.add(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator += (unsigned long int k)
{
    builder b(*this);
    b.add(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator += (long long int k)
{
    builder b(*this);
    b.add(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator += (unsigned long long int k)
{
    builder b(*this);
    b.add(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator + (const integer & k) const
{
    builder b(*this);
    b.add(k);
    return integer(b);
}

alf::integer
alf::integer::operator + (const builder & bb) const
{
    builder b(*this);
    b.add(bb);
    return integer(b);
}

alf::integer
alf::integer::operator + (int k) const
{
    builder b(*this);
    b.add(k);
    return integer(b);
}

alf::integer
alf::integer::operator + (unsigned int k) const
{
    builder b(*this);
    b.add(k);
    return integer(b);
}

alf::integer
alf::integer::operator + (long int k) const
{
    builder b(*this);
    b.add(k);
    return integer(b);
}

alf::integer
alf::integer::operator + (unsigned long int k) const
{
    builder b(*this);
    b.add(k);
    return integer(b);
}

alf::integer
alf::integer::operator + (long long int k) const
{
    builder b(*this);
    b.add(k);
    return integer(b);
}

alf::integer
alf::integer::operator + (unsigned long long int k) const
{
    builder b(*this);
    b.add(k);
    return integer(b);
}

alf::integer &
alf::integer::operator -= (const integer & k)
{
    builder b(*this);
    b.sub(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator -= (const builder & bb)
{
    builder b(*this);
    b.sub(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator -= (int k)
{
    builder b(*this);
    b.sub(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator -= (unsigned int k)
{
    builder b(*this);
    b.sub(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator -= (long int k)
{
    builder b(*this);
    b.sub(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator -= (unsigned long int k)
{
    builder b(*this);
    b.sub(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator -= (long long int k)
{
    builder b(*this);
    b.sub(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator -= (unsigned long long int k)
{
    builder b(*this);
    b.sub(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator - (const integer & k) const
{
    builder b(*this);
    b.sub(k);
    return integer(b);
}

alf::integer
alf::integer::operator - (const builder & bb) const
{
    builder b(*this);
    b.sub(bb);
    return integer(b);
}

alf::integer
alf::integer::operator - (int k) const
{
    builder b(*this);
    b.sub(k);
    return integer(b);
}

alf::integer
alf::integer::operator - (unsigned int k) const
{
    builder b(*this);
    b.sub(k);
    return integer(b);
}

alf::integer
alf::integer::operator - (long int k) const
{
    builder b(*this);
    b.sub(k);
    return integer(b);
}

alf::integer
alf::integer::operator - (unsigned long int k) const
{
    builder b(*this);
    b.sub(k);
    return integer(b);
}

alf::integer
alf::integer::operator - (long long int k) const
{
    builder b(*this);
    b.sub(k);
    return integer(b);
}

alf::integer
alf::integer::operator - (unsigned long long int k) const
{
    builder b(*this);
    b.sub(k);
    return integer(b);
}

alf::integer &
alf::integer::operator *= (const integer & k)
{
    builder b(*this);
    b.mul(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator *= (const builder & bb)
{
    builder b(*this);
    b.mul(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator *= (int k)
{
    builder b(*this);
    b.mul(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator *= (unsigned int k)
{
    builder b(*this);
    b.mul(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator *= (long int k)
{
    builder b(*this);
    b.mul(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator *= (unsigned long int k)
{
    builder b(*this);
    b.mul(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator *= (long long int k)
{
    builder b(*this);
    b.mul(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator *= (unsigned long long int k)
{
    builder b(*this);
    b.mul(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator * (const integer & k) const
{
    builder b(*this);
    b.mul(k);
    return integer(b);
}

alf::integer
alf::integer::operator * (const builder & bb) const
{
    builder b(*this);
    b.mul(bb);
    return integer(b);
}

alf::integer
alf::integer::operator * (int k) const
{
    builder b(*this);
    b.mul(k);
    return integer(b);
}

alf::integer
alf::integer::operator * (unsigned int k) const
{
    builder b(*this);
    b.mul(k);
    return integer(b);
}

alf::integer
alf::integer::operator * (long int k) const
{
    builder b(*this);
    b.mul(k);
    return integer(b);
}

alf::integer
alf::integer::operator * (unsigned long int k) const
{
    builder b(*this);
    b.mul(k);
    return integer(b);
}

alf::integer
alf::integer::operator * (long long int k) const
{
    builder b(*this);
    b.mul(k);
    return integer(b);
}

alf::integer
alf::integer::operator * (unsigned long long int k) const
{
    builder b(*this);
    b.mul(k);
    return integer(b);
}

alf::integer &
alf::integer::operator /= (const integer & k)
{
    builder b(*this);
    b.div(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator /= (const builder & bb)
{
    builder b(*this);
    b.div(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator /= (int k)
{
    builder b(*this);
    b.div(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator /= (unsigned int k)
{
    builder b(*this);
    b.div(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator /= (long int k)
{
    builder b(*this);
    b.div(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator /= (unsigned long int k)
{
    builder b(*this);
    b.div(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator /= (long long int k)
{
    builder b(*this);
    b.div(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator /= (unsigned long long int k)
{
    builder b(*this);
    b.div(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator / (const integer & k) const
{
    builder b(*this);
    b.div(k);
    return integer(b);
}

alf::integer
alf::integer::operator / (const builder & bb) const
{
    builder b(*this);
    b.div(bb);
    return integer(b);
}

alf::integer
alf::integer::operator / (int k) const
{
    builder b(*this);
    b.div(k);
    return integer(b);
}

alf::integer
alf::integer::operator / (unsigned int k) const
{
    builder b(*this);
    b.div(k);
    return integer(b);
}

alf::integer
alf::integer::operator / (long int k) const
{
    builder b(*this);
    b.div(k);
    return integer(b);
}

alf::integer
alf::integer::operator / (unsigned long int k) const
{
    builder b(*this);
    b.div(k);
    return integer(b);
}

alf::integer
alf::integer::operator / (long long int k) const
{
    builder b(*this);
    b.div(k);
    return integer(b);
}

alf::integer
alf::integer::operator / (unsigned long long int k) const
{
    builder b(*this);
    b.div(k);
    return integer(b);
}

alf::integer &
alf::integer::operator %= (const integer & k)
{
    builder b(*this);
    b.mod(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator %= (const builder & bb)
{
    builder b(*this);
    b.mod(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator %= (int k)
{
    builder b(*this);
    b.mod(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator %= (unsigned int k)
{
    builder b(*this);
    b.mod(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator %= (long int k)
{
    builder b(*this);
    b.mod(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator %= (unsigned long int k)
{
    builder b(*this);
    b.mod(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator %= (long long int k)
{
    builder b(*this);
    b.mod(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator %= (unsigned long long int k)
{
    builder b(*this);
    b.mod(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator % (const integer & k) const
{
    builder b(*this);
    b.mod(k);
    return integer(b);
}

alf::integer
alf::integer::operator % (const builder & bb) const
{
    builder b(*this);
    b.mod(bb);
    return integer(b);
}

alf::integer
alf::integer::operator % (int k) const
{
    builder b(*this);
    b.mod(k);
    return integer(b);
}

alf::integer
alf::integer::operator % (unsigned int k) const
{
    builder b(*this);
    b.mod(k);
    return integer(b);
}

alf::integer
alf::integer::operator % (long int k) const
{
    builder b(*this);
    b.mod(k);
    return integer(b);
}

alf::integer
alf::integer::operator % (unsigned long int k) const
{
    builder b(*this);
    b.mod(k);
    return integer(b);
}

alf::integer
alf::integer::operator % (long long int k) const
{
    builder b(*this);
    b.mod(k);
    return integer(b);
}

alf::integer
alf::integer::operator % (unsigned long long int k) const
{
    builder b(*this);
    b.mod(k);
    return integer(b);
}

// static
int
alf::integer::divmod(integer & q, integer & r,
                        const integer & a, const integer & b)
{
    builder qq, rr;
    int ret = builder::divmod(qq, rr,
                                a.data(), a.u_len(), a.s_len(),
                                b.data(), b.u_len(), b.s_len());
    q = qq; r = rr;
    return ret;
}

// static
alf::integer
alf::integer::pow(const integer & a, const integer & b)
{
    builder x;
    int j = x.pow(a.data(), a.u_len(), a.s_len(),
                    b.data(), b.u_len(), b.s_len(), 0, 0, 0);
    if (j < 0) return 0; // TODO: throw error.
    return integer(x);
}

// static
alf::integer
alf::integer::pow(const integer & a, const integer & b, const integer & c)
{
    builder x;
    int j = x.pow(a.data(), a.u_len(), a.s_len(),
                    b.data(), b.u_len(), b.s_len(),
                    c.data(), c.u_len(), c.s_len());
    if (j < 0) return 0; // TODO: throw error.
    return integer(x);
}

// static
alf::integer
alf::integer::fact(integer n)
{
    builder x;
    int j = x.fact(n.data(), n.u_len(), n.s_len());
    if (j < 0) return 0; // TODO: throw error.
    return integer(x);
}

alf::integer &
alf::integer::operator &= (const integer & k)
{
    builder b(*this);
    b.bit_and(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator &= (const builder & bb)
{
    builder b(*this);
    b.bit_and(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator &= (int k)
{
    builder b(*this);
    b.bit_and(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator &= (unsigned int k)
{
    builder b(*this);
    b.bit_and(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator &= (long int k)
{
    builder b(*this);
    b.bit_and(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator &= (unsigned long int k)
{
    builder b(*this);
    b.bit_and(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator &= (long long int k)
{
    builder b(*this);
    b.bit_and(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator &= (unsigned long long int k)
{
    builder b(*this);
    b.bit_and(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator & (const integer & k) const
{
    builder b(*this);
    b.bit_and(k);
    return integer(b);
}

alf::integer
alf::integer::operator & (const builder & bb) const
{
    builder b(*this);
    b.bit_and(bb);
    return integer(b);
}

alf::integer
alf::integer::operator & (int k) const
{
    builder b(*this);
    b.bit_and(k);
    return integer(b);
}

alf::integer
alf::integer::operator & (unsigned int k) const
{
    builder b(*this);
    b.bit_and(k);
    return integer(b);
}

alf::integer
alf::integer::operator & (long int k) const
{
    builder b(*this);
    b.bit_and(k);
    return integer(b);
}

alf::integer
alf::integer::operator & (unsigned long int k) const
{
    builder b(*this);
    b.bit_and(k);
    return integer(b);
}

alf::integer
alf::integer::operator & (long long int k) const
{
    builder b(*this);
    b.bit_and(k);
    return integer(b);
}

alf::integer
alf::integer::operator & (unsigned long long int k) const
{
    builder b(*this);
    b.bit_and(k);
    return integer(b);
}

alf::integer &
alf::integer::operator |= (const integer & k)
{
    builder b(*this);
    b.bit_or(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator |= (const builder & bb)
{
    builder b(*this);
    b.bit_or(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator |= (int k)
{
    builder b(*this);
    b.bit_or(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator |= (unsigned int k)
{
    builder b(*this);
    b.bit_or(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator |= (long int k)
{
    builder b(*this);
    b.bit_or(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator |= (unsigned long int k)
{
    builder b(*this);
    b.bit_or(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator |= (long long int k)
{
    builder b(*this);
    b.bit_or(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator |= (unsigned long long int k)
{
    builder b(*this);
    b.bit_or(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator | (const integer & k) const
{
    builder b(*this);
    b.bit_or(k);
    return integer(b);
}

alf::integer
alf::integer::operator | (const builder & bb) const
{
    builder b(*this);
    b.bit_or(bb);
    return integer(b);
}

alf::integer
alf::integer::operator | (int k) const
{
    builder b(*this);
    b.bit_or(k);
    return integer(b);
}

alf::integer
alf::integer::operator | (unsigned int k) const
{
    builder b(*this);
    b.bit_or(k);
    return integer(b);
}

alf::integer
alf::integer::operator | (long int k) const
{
    builder b(*this);
    b.bit_or(k);
    return integer(b);
}

alf::integer
alf::integer::operator | (unsigned long int k) const
{
    builder b(*this);
    b.bit_or(k);
    return integer(b);
}

alf::integer
alf::integer::operator | (long long int k) const
{
    builder b(*this);
    b.bit_or(k);
    return integer(b);
}

alf::integer
alf::integer::operator | (unsigned long long int k) const
{
    builder b(*this);
    b.bit_or(k);
    return integer(b);
}

alf::integer &
alf::integer::operator ^= (const integer & k)
{
    builder b(*this);
    b.bit_xor(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator ^= (const builder & bb)
{
    builder b(*this);
    b.bit_xor(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator ^= (int k)
{
    builder b(*this);
    b.bit_xor(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator ^= (unsigned int k)
{
    builder b(*this);
    b.bit_xor(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator ^= (long int k)
{
    builder b(*this);
    b.bit_xor(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator ^= (unsigned long int k)
{
    builder b(*this);
    b.bit_xor(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator ^= (long long int k)
{
    builder b(*this);
    b.bit_xor(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator ^= (unsigned long long int k)
{
    builder b(*this);
    b.bit_xor(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator ^ (const integer & k) const
{
    builder b(*this);
    b.bit_xor(k);
    return integer(b);
}

alf::integer
alf::integer::operator ^ (const builder & bb) const
{
    builder b(*this);
    b.bit_xor(bb);
    return integer(b);
}

alf::integer
alf::integer::operator ^ (int k) const
{
    builder b(*this);
    b.bit_xor(k);
    return integer(b);
}

alf::integer
alf::integer::operator ^ (unsigned int k) const
{
    builder b(*this);
    b.bit_xor(k);
    return integer(b);
}

alf::integer
alf::integer::operator ^ (long int k) const
{
    builder b(*this);
    b.bit_xor(k);
    return integer(b);
}

alf::integer
alf::integer::operator ^ (unsigned long int k) const
{
    builder b(*this);
    b.bit_xor(k);
    return integer(b);
}

alf::integer
alf::integer::operator ^ (long long int k) const
{
    builder b(*this);
    b.bit_xor(k);
    return integer(b);
}

alf::integer
alf::integer::operator ^ (unsigned long long int k) const
{
    builder b(*this);
    b.bit_xor(k);
    return integer(b);
}

alf::integer &
alf::integer::operator <<= (const integer & k)
{
    builder b(*this);
    b.shl(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator <<= (const builder & bb)
{
    builder b(*this);
    b.shl(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator <<= (int k)
{
    builder b(*this);
    b.shl(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator <<= (unsigned int k)
{
    builder b(*this);
    b.shl(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator <<= (long int k)
{
    builder b(*this);
    b.shl(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator <<= (unsigned long int k)
{
    builder b(*this);
    b.shl(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator <<= (long long int k)
{
    builder b(*this);
    b.shl(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator <<= (unsigned long long int k)
{
    builder b(*this);
    b.shl(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator << (const integer & k) const
{
    builder b(*this);
    b.shl(k);
    return integer(b);
}

alf::integer
alf::integer::operator << (const builder & bb) const
{
    builder b(*this);
    b.shl(bb);
    return integer(b);
}

alf::integer
alf::integer::operator << (int k) const
{
    builder b(*this);
    b.shl(k);
    return integer(b);
}

alf::integer
alf::integer::operator << (unsigned int k) const
{
    builder b(*this);
    b.shl(k);
    return integer(b);
}

alf::integer
alf::integer::operator << (long int k) const
{
    builder b(*this);
    b.shl(k);
    return integer(b);
}

alf::integer
alf::integer::operator << (unsigned long int k) const
{
    builder b(*this);
    b.shl(k);
    return integer(b);
}

alf::integer
alf::integer::operator << (long long int k) const
{
    builder b(*this);
    b.shl(k);
    return integer(b);
}

alf::integer
alf::integer::operator << (unsigned long long int k) const
{
    builder b(*this);
    b.shl(k);
    return integer(b);
}

alf::integer &
alf::integer::operator >>= (const integer & k)
{
    builder b(*this);
    b.shr(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator >>= (const builder & bb)
{
    builder b(*this);
    b.shr(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator >>= (int k)
{
    builder b(*this);
    b.shr(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator >>= (unsigned int k)
{
    builder b(*this);
    b.shr(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator >>= (long int k)
{
    builder b(*this);
    b.shr(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator >>= (unsigned long int k)
{
    builder b(*this);
    b.shr(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator >>= (long long int k)
{
    builder b(*this);
    b.shr(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::operator >>= (unsigned long long int k)
{
    builder b(*this);
    b.shr(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::operator >> (const integer & k) const
{
    builder b(*this);
    b.shr(k);
    return integer(b);
}

alf::integer
alf::integer::operator >> (const builder & bb) const
{
    builder b(*this);
    b.shr(bb);
    return integer(b);
}

alf::integer
alf::integer::operator >> (int k) const
{
    builder b(*this);
    b.shr(k);
    return integer(b);
}

alf::integer
alf::integer::operator >> (unsigned int k) const
{
    builder b(*this);
    b.shr(k);
    return integer(b);
}

alf::integer
alf::integer::operator >> (long int k) const
{
    builder b(*this);
    b.shr(k);
    return integer(b);
}

alf::integer
alf::integer::operator >> (unsigned long int k) const
{
    builder b(*this);
    b.shr(k);
    return integer(b);
}

alf::integer
alf::integer::operator >> (long long int k) const
{
    builder b(*this);
    b.shr(k);
    return integer(b);
}

alf::integer
alf::integer::operator >> (unsigned long long int k) const
{
    builder b(*this);
    b.shr(k);
    return integer(b);
}

alf::integer &
alf::integer::andnot(const integer & k)
{
    builder b(*this);
    b.bit_andnot(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::andnot(const builder & bb)
{
    builder b(*this);
    b.bit_andnot(bb);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::andnot(int k)
{
    builder b(*this);
    b.bit_andnot(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::andnot(unsigned int k)
{
    builder b(*this);
    b.bit_andnot(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::andnot(long int k)
{
    builder b(*this);
    b.bit_andnot(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::andnot(unsigned long int k)
{
    builder b(*this);
    b.bit_andnot(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::andnot(long long int k)
{
    builder b(*this);
    b.bit_andnot(k);
    assign(b);
    return *this;
}

alf::integer &
alf::integer::andnot(unsigned long long int k)
{
    builder b(*this);
    b.bit_andnot(k);
    assign(b);
    return *this;
}

alf::integer
alf::integer::andnot(const integer & k) const
{
    builder b(*this);
    b.bit_andnot(k);
    return integer(b);
}

alf::integer
alf::integer::andnot(const builder & bb) const
{
    builder b(*this);
    b.bit_andnot(bb);
    return integer(b);
}

alf::integer
alf::integer::andnot(int k) const
{
    builder b(*this);
    b.bit_andnot(k);
    return integer(b);
}

alf::integer
alf::integer::andnot(unsigned int k) const
{
    builder b(*this);
    b.bit_andnot(k);
    return integer(b);
}

alf::integer
alf::integer::andnot(long int k) const
{
    builder b(*this);
    b.bit_andnot(k);
    return integer(b);
}

alf::integer
alf::integer::andnot(unsigned long int k) const
{
    builder b(*this);
    b.bit_andnot(k);
    return integer(b);
}

alf::integer
alf::integer::andnot(long long int k) const
{
    builder b(*this);
    b.bit_andnot(k);
    return integer(b);
}

alf::integer
alf::integer::andnot(unsigned long long int k) const
{
    builder b(*this);
    b.bit_andnot(k);
    return integer(b);
}

alf::integer
alf::integer::operator - () const
{
    builder b(*this);
    b.neg();
    return integer(b);
}

alf::integer
alf::integer::operator ~ () const
{
    builder b(*this);
    b.invbits();
    return integer(b);
}

alf::integer &
alf::integer::operator ++ ()
{
    builder b(*this);
    b.add1();
    return *this;
}

alf::integer &
alf::integer::operator -- ()
{
    builder b(*this);
    b.sub1();
    return *this;
}

alf::integer
alf::integer::operator ++ (int)
{
    builder b(*this);
    integer ret(*this);
    b.add1();
    assign(b);
    return ret;
}

alf::integer
alf::integer::operator -- (int)
{
    builder b(*this);
    integer ret(*this);
    b.add1();
    assign(b);
    return ret;
}

int
alf::integer::cmp(int k) const
{
    builder b(k);
    return cmp(b);
}

int alf::integer::cmp(unsigned int k) const
{
    builder b(k);
    return cmp(b);
}

int alf::integer::cmp(long int k) const
{
    builder b(k);
    return cmp(b);
}

int alf::integer::cmp(unsigned long int k) const
{
    builder b(k);
    return cmp(b);
}

int alf::integer::cmp(long long int k) const
{
    builder b(k);
    return cmp(b);
}

int alf::integer::cmp(unsigned long long int k) const
{
    builder b(k);
    return cmp(b);
}

// ============================
// builder

void
alf::integer::builder::ensure(size_t k)
{
    size_t x = n + k;
    if (x > m) {
        while (m < x && m < LARGER_SIZE)
            m <<= 1;
        while (m < x) m += LARGER_SIZE;
        digit * u = new digit[m];
        memcpy(u, a, n*sizeof(digit));
        memset(u + n, 0, (m-n)*sizeof(digit));
        if (a != arr) delete [] a;
        a = u;
    }
}

alf::integer::builder &
alf::integer::builder::assign(const builder & b)
{
    ensure(b.n);
    memcpy(a, b.a, b.n*sizeof(digit));
    n = b.n;
    s = b.s;
    return *this;
}

int
alf::integer::builder::assign(const decimal & d)
{
    return assign(d.data(), d.length(), d.slength(),
                    d.expon(), d.klass());
}

int
alf::integer::builder::assign(const digit * ap,
                                size_t an, ssize_t as,
                                ssize_t ax, int akls)
{
    int neg = 0;
    static const digit tens[9] = { 1, 10, 100, 1000,
        10000, 100000, 1000000, 10000000, 100000000 };

    switch (akls) {
    case decimal::SNAN:
    case decimal::QNAN:
    case decimal::PINF:
    case decimal::NINF:
        return -1;
    case decimal::PZERO:
    case decimal::NEGZERO:
        n = 0; s = 0; return 0;
    case decimal::NREGULAR:
        neg = 1;
        break;
    }
    // We first need to arrange so that ax == 0.
    // if ax < 0 we strip off lower digits
    // if ax <= -9 we can even remove whole digits
    // of a.
    ssize_t ax9;

    if (ax <= -9) {
        ax9 = (-ax)/ 9;
        if (ax9 >= an) {
            n = 0; s = 0; return 0;
        }
        // ax9 < an
        ap = ap + ax9;
        an = an - ax9;
        ax += 9 * ax9;
    }
    // ax >= -8.

    // decimal numbers have base 1000000000,
    // integer numbers have base 1 << 30.
    // time to convert.
    // as 1 << 30 is slightly larger than 10**9
    // we can simply do it this way:
    dbl_digit w = 0;

    n = 0;
    ssize_t j = an;
    while (j > 0)
        mul_(1000000000, ap[--j]);
    // if ax < 0 we need to divide by 10**-ax
    // and if ax > 0 we need to multiply by 10**ax.
    if (ax < 0) {
        div_(tens[-ax]);
    } else {
        if (ax >= 9) {
            ax9 = ax / 9;
            for (j = 0; j < ax9; ++j)
                mul_(1000000000, 0);
            ax = ax % 9;
        }
        if (ax > 0)
            mul_(tens[ax % 9], 0);
    }
    n = normalize_(a, n);
    s = neg ? -n : n;
    return n;
}

int
alf::integer::builder::assign(float k)
{
    return assign(double(k));
}

int
alf::integer::builder::assign(double k)
{
    enum {
        EXP    = 0x7ff0000000000000ULL,
        MANT   = 0x000fffffffffffffULL,
        HIDDEN = 0x0010000000000000ULL,
    };
    enum { BIAS = 1023, };

    long long int i = *(long long int *) & k;
    unsigned long long int F = (unsigned long long int)i;
    unsigned long long int X = F & EXP;
    unsigned long long int M = F & MANT;
    unsigned long long int H;
    int x;
    int neg = i < 0;

    switch (X) {
    case EXP:
        // nan or inf.
        return -1;
    case 0:
        // subnormal.
        if ((H = M) == 0) {
            n = 0; s = 0; return 0;
        }
        X = 1; // and H == M and H != 0.
        break;
    default:
        H = HIDDEN | M;
        break;
    }
    // value is 2**((X >> 52) - BIAS) * 1/0.M
    // or 2**x * H/ 2**52
    // or 2**(x-52) * H
    x = int(X >> 52) - (BIAS + 52);
    // value is 2**x * H

    // if x < 0 we need to drop bits.
    // if x < -52 the value is zero.
    if (x < -52) {
        n = 0; s = 0; return 0;
    }
    if (x < 0) {
        H = H >> -x;
        x = 0;
    }
    if (H == 0) {
        n = 0; s = 0; return 0;
    }
    // x >= 0
    assign(H);
    int x30 = x / 30;
    int x30mod = x % 30;
    ensure(x30);
    memmove(a + x30, a, n*sizeof(digit));
    memset(a, 0, x30*sizeof(digit));
    n += x30;
    if (x30mod) {
        digit x2 = 1;
        digit xx = 2;
        do {
            if (x30mod & 1) x2 *= xx;
            xx *= xx;
        } while ((x30mod >>= 1) != 0);
        mul_(x2,0);
    }
    n = normalize_(a, n);
    s = neg ? -n : n;
    return n;
}

int
alf::integer::builder::assign(long double k)
{
    enum {
        EXP    = 0x7fff,
        HIDDEN = 0x8000000000000000ULL,
    };
    enum { BIAS = 16383, };

    struct ldbl_s {
        unsigned long long int m;
        short x;
        char notused[6];
    };
    union ldbl_u {
        ldbl_s s;
        long double x;
    };

    volatile ldbl_u y;
    unsigned long long int H;
    short X;
    int x, neg;

    y.x = k;
    H = y.s.m;
    X = y.s.x;
    neg = X < 0;
    X &= EXP;

    switch (X) {
    case EXP:
        // nan or inf.
        return -1;
    case 0:
        // subnormal.
        if (H == 0) {
            n = 0; s = 0; return 0;
        }
        X = 1; // and H == M and H != 0.
        break;
    default:
        break;
    }
    // value is 2**(X- BIAS) * H/2**63
    // or 2**(x-63) * H
    x = int(X) - (BIAS + 63);
    // value is 2**x * H

    // if x < 0 we need to drop bits.
    // if x < -63 the value is zero.
    if (x < -63) {
        n = 0; s = 0; return 0;
    }
    if (x < 0) {
        H = H >> -x;
        x = 0;
    }
    if (H == 0) {
        n = 0; s = 0; return 0;
    }
    // x >= 0
    assign(H);
    int x30 = x / 30;
    int x30mod = x % 30;
    ensure(x30);
    memmove(a + x30, a, n*sizeof(digit));
    memset(a, 0, x30*sizeof(digit));
    n += x30;
    if (x30mod) {
        digit x2 = 1;
        digit xx = 2;
        do {
            if (x30mod & 1) x2 *= xx;
            xx *= xx;
        } while ((x30mod >>= 1) != 0);
        mul_(x2,0);
    }
    n = normalize_(a, n);
    s = neg ? -n : n;
    return n;
}

alf::integer::builder &
alf::integer::builder::assign(int k)
{
    unsigned int u = (unsigned int)(k < 0 ? -k : k);
    n = 0;
    if (u) {
        a[n++] = digit(u & BITS_MASK);
        u >>= BITS;
        if (u) a[n++] = digit(u);
    }
    s = k < 0 ? -n : n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::assign(unsigned int k)
{
    unsigned int u = k;
    n = 0;
    if (u) {
        a[n++] = digit(u & BITS_MASK);
        u >>= BITS;
        if (u) a[n++] = digit(u);
    }
    s = n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::assign(long int k)
{
    unsigned long long int u = (unsigned long long int)(k < 0 ? -k : k);
    n = 0;
    if (u) {
        a[n++] = digit(u & BITS_MASK);
        u >>= BITS;
        if (u) {
            a[n++] = digit(u & BITS_MASK);
            u >>= BITS;
            if (u) a[n++] = digit(u);
        }
    }
    s = k < 0 ? -n : n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::assign(unsigned long int k)
{
    unsigned long long int u = k;
    n = 0;
    if (u) {
        a[n++] = digit(u & BITS_MASK);
        u >>= BITS;
        if (u) {
            a[n++] = digit(u & BITS_MASK);
            u >>= BITS;
            if (u) a[n++] = digit(u);
        }
    }
    s = n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::assign(long long int k)
{
    unsigned long long int u = (unsigned long long int)(k < 0 ? -k : k);
    n = 0;
    if (u) {
        a[n++] = digit(u & BITS_MASK);
        u >>= BITS;
        if (u) {
            a[n++] = digit(u & BITS_MASK);
            u >>= BITS;
            if (u) a[n++] = digit(u);
        }
    }
    s = k < 0 ? -n : n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::assign(unsigned long long int k)
{
    unsigned long long int u = k;
    n = 0;
    if (u) {
        a[n++] = digit(u & BITS_MASK);
        u >>= BITS;
        if (u) {
            a[n++] = digit(u & BITS_MASK);
            u >>= BITS;
            if (u) a[n++] = digit(u);
        }
    }
    s = n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::assign(const digit * x, size_t xn, ssize_t xs)
{
    ensure(xn);
    memcpy(a, x, xn*sizeof(digit));
    n = xn;
    s = xs;
    return *this;
}

alf::integer::builder &
alf::integer::builder::assign(const digit * x, ssize_t xs)
{
    size_t xn = (size_t)(xs < 0 ? -xs : xs);
    return assign(x, xn, xs);
}

alf::integer::builder &
alf::integer::builder::assign(const integer & k)
{
    return assign(k.data(), k.u_len(), k.s_len());
}

alf::integer::builder::builder()
    : a(arr), n(0), m(INI_SIZE), s(0)
{ }

alf::integer::builder::builder(const builder & b)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(b);
}

alf::integer::builder::builder(int k)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(k);
}

alf::integer::builder::builder(unsigned int k)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(k);
}

alf::integer::builder::builder(long int k)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(k);
}

alf::integer::builder::builder(unsigned long int k)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(k);
}

alf::integer::builder::builder(long long int k)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(k);
}

alf::integer::builder::builder(unsigned long long int k)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(k);
}

alf::integer::builder::builder(const digit * x, size_t xn, ssize_t xs)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(x, xn, xs);
}

alf::integer::builder::builder(const digit * x, ssize_t xs)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(x, size_t(xs < 0 ? -xs : xs), xs);
}

alf::integer::builder::builder(const integer & k)
    : a(arr), n(0), m(INI_SIZE), s(0)
{
    assign(k.data(), k.u_len(), k.s_len());
}

alf::integer::builder::~builder()
{
    if (a != arr) delete [] a;
}

bool
alf::integer::builder::get(signed char & k) const
{
    unsigned int z;

    switch (s) {
    case 0: k = 0; return true;
    case 1: z = *a; if (z > 127) return false; k = (signed char)z; return true;
    case -1:
        z = *a; if (z > 128) return false; k = -(signed char)z; return true;
    }
    return false;
}

bool
alf::integer::builder::get(unsigned char & k) const
{
    unsigned int z;

    switch (s) {
    case 0: k = 0; return true;
    case 1: z = *a; if (z > 255) return false; k = (signed char)z; return true;
    }
    return false;
}

bool
alf::integer::builder::get(char & k) const
{
#if CHAR_MIN == 0
    unsigned char ch;
#else
    signed char ch;
#endif
    if (! get(ch)) return false;
    k = (char)ch;
    return true;
}

bool
alf::integer::builder::get(short & k) const
{
    unsigned int z;

    switch (s) {
    case 0: k = 0; return true;
    case 1: z = *a; if (z > SHRT_MAX) return false; k = (short)z; return true;
    case -1:
        z = *a; if (z > -SHRT_MIN) return false; k = -(short)z; return true;
    }
    return false;
}

bool
alf::integer::builder::get(unsigned short & k) const
{
    unsigned int z;

    switch (s) {
    case 0: k = 0; return true;
    case 1:
        z = *a;
        if (z > USHRT_MAX) return false;
        k = (unsigned short)z;
        return true;
    }
    return false;
}

bool
alf::integer::builder::get(int & k) const
{
    unsigned int z;

    switch (s) {
    case 0: k = 0; return true;
    case 1: k = int(*a); return true;
    case -1: k = -int(*a); return true;
    case 2:
        if ((z = a[1]) >= 2) return false;
        z <<= BITS;
        z |= *a;
        k = int(z);
        return true;
    case -2:
        if ((z = a[1]) > 2) return false;
        if (z == 2 && *a != 0) return false;
        z <<= BITS;
        z |= *a;
        k = int(-z);
        return true;
    }
    return false;
}

bool
alf::integer::builder::get(unsigned int & k) const
{
    unsigned int z;

    switch (s) {
    case 0: k = 0; return true;
    case 1: k = (unsigned int)*a; return true;
    case 2:
        if ((z = a[1]) >= 4) return false;
        z <<= BITS;
        z |= *a;
        k = (unsigned int)z;
        return true;
    }
    return false;
}

bool
alf::integer::builder::get(long int & k) const
{
#if LONG_MAX == INT_MAX
    int j;
#else
    long long int j;
#endif
    if (! get(j)) return false;
    k = (long int)j;
    return true;
}

bool
alf::integer::builder::get(unsigned long int & k) const
{
#if LONG_MAX == INT_MAX
    unsigned int u;
#else
    unsigned long long int u;
#endif
    if (! get(u)) return false;
    k = (long int)u;
    return true;
}

bool
alf::integer::builder::get(long long int & k) const
{
    unsigned long long int z;
    unsigned int w;

    switch (s) {
    case 0: k = 0; return true;
    case 1: k = *a; return true;
    case -1: k = -*a; return true;
    case 2:
        z = a[1];
        z <<= BITS;
        z |= *a;
        k = (long long int)z;
        return true;
    case -2:
        z = a[1];
        z <<= BITS;
        z |= *a;
        k = (long long int)-z;
        return true;
    case 3:
        w = a[2];
        if (w >= 8) return false;
        z = w;
        z <<= BITS;
        z |= a[1];
        z <<= BITS;
        z |= *a;
        k = (long long int)z;
        return true;
    case -3:
        w = a[2];
        if (w > 8) return false;
        if (w == 8) {
            if (a[1] != 0 || *a != 0) return false;
        }
        z = w;
        z <<= BITS;
        z |= a[1];
        z <<= BITS;
        z |= *a;
        k = (long long int)-z;
        return true;
    }
    return false;
}

bool
alf::integer::builder::get(unsigned long long & k) const
{
    unsigned long long int z;
    unsigned int w;

    switch (s) {
    case 0: k = 0; return true;
    case 1: k = *a; return true;
    case 2:
        z = a[1];
        z <<= BITS;
        z |= *a;
        k = z;
        return true;
    case 3:
        w = a[2];
        if (w >= 16) return false;
        z = w;
        z <<= BITS;
        z |= a[1];
        z <<= BITS;
        z |= *a;
        k = z;
        return true;
    }
    return false;
}

bool
alf::integer::builder::get(float & k) const
{
    static const double Z =
                    tpow<double, 2, BITS>::value;

    double u = 0;
    ssize_t j = n;

    while (--j >= 0)
        u = u * Z + a[j];
    if (s < 0) u = -u;
    k = float(u);
    return true;
}

bool
alf::integer::builder::get(double & k) const
{
    static const double Z =
                tpow<double, 2, BITS>::value;

    double u = 0;
    ssize_t j = n;

    while (--j >= 0)
        u = u * Z + a[j];
    if (s < 0) u = -u;
    k = u;
    return true;
}

bool
alf::integer::builder::get(long double & k) const
{
    static const long double Z =
                tpow<long double, 2, BITS>::value;

    long double u = 0;
    ssize_t j = n;

    while (--j >= 0)
        u = u * Z + a[j];
    if (s < 0) u = -u;
    k = u;
    return true;
}

alf::integer::builder &
alf::integer::builder::mul_(digit f, digit d)
{
    digit u = muladd4_(a, n, a, n, f, d);
    if (u) {
        ensure(1);
        a[n++] = u;
        s = s < 0 ? -n : n;
    }
    return *this;
}

unsigned int
alf::integer::builder::div_(unsigned int f)
{
    digit m = div_(a, n, a, n, f);
    if (n > 0 && a[n-1] == 0) {
        --n;
        s = s < 0 ? -n : n;
    }
    return m;
}

// remove leading zeroes.
alf::integer::builder &
alf::integer::builder::normalize()
{
    n = normalize_(a, n);
    s = s < 0 ? -n : n;
    return *this;
}

// set length to be at least k long, insert leading zeroes
// if necessary. If length is already longer than k, do nothing.
alf::integer::builder &
alf::integer::builder::length_at_least(size_t k)
{
    if (n < k) {
        size_t x = k - n;
        ensure(x);
        memset(a+n, 0, x*sizeof(digit));
        n = k;
        s = s < 0 ? -n : n;
    }
    return *this;
}

// bits of the number are inverted + 1.
alf::integer::builder &
alf::integer::builder::negbits()
{
    negbits_(a, n);
    return *this;
}

alf::integer::builder &
alf::integer::builder::invbits()
{
    // if number is negative, inerting the bits makes it positive
    // because a negative number has an infinite string of 1's
    // which inverted becomes 0's and vice versa, so we also negate
    // the number.
    invbits_(a, n);
    s = -s;
    return *this;
}

alf::integer::builder &
alf::integer::builder::add1()
{
    if (s < 0) {
        if (sub1_(a, n)) {
            a[0] = 1;
            n = 1;
            s = 1;
        }
    } else if (digit u = add1_(a, n)) {
        a[n++] = u;
        s = n;
    }
    return *this;
}

alf::integer::builder &
alf::integer::builder::sub1()
{
    if (s > 0) {
        if (sub1_(a, n)) {
            a[0] = 1;
            n = 1;
            s = -1;
        }
    } else if (digit u = add1_(a, n)) {
        a[n++] = u;
        s = -n;
    }
    return *this;
}

// static
int
alf::integer::builder::cmp_(const digit * a, size_t an, ssize_t as,
                            const digit * b, size_t bn, ssize_t bs)
{
    an = normalize_(a, an);
    as = as < 0 ? -an : an;
    bn = normalize_(b, bn);
    bs = bs < 0 ? -bn : bs;
    ssize_t z = as - bs;
    if (z)
        return z > 0 ? 1 : -1;
    ssize_t k = an;
    while (--k >= 0 && (z = a[k] - b[k]) == 0);
    return z > 0 ? 1 : z < 0 ? -1 : 0;
}

int
alf::integer::builder::cmp(const digit * d, size_t dn, ssize_t ds) const
{
    return cmp_(a, n, s, d, dn, ds);
}

int
alf::integer::builder::cmp(int k) const
{
    builder other(k);
    return cmp(other.a, other.n, other.s);
}

int
alf::integer::builder::cmp(unsigned int k) const
{
    builder other(k);
    return cmp(other.a, other.n, other.s);
}

int
alf::integer::builder::cmp(long int k) const
{
    builder other(k);
    return cmp(other.a, other.n, other.s);
}

int
alf::integer::builder::cmp(unsigned long int k) const
{
    builder other(k);
    return cmp(other.a, other.n, other.s);
}

int
alf::integer::builder::cmp(long long int k) const
{
    builder other(k);
    return cmp(other.a, other.n, other.s);
}

int
alf::integer::builder::cmp(unsigned long long int k) const
{
    builder other(k);
    return cmp(other.a, other.n, other.s);
}

alf::integer::builder &
alf::integer::builder::add(const digit * aa, size_t an, ssize_t as,
                            const digit * bb, size_t bn, ssize_t bs)
{
    if (n < an)
        length_at_least(an);
    if (n < bn)
        length_at_least(bn);
    if (as < 0) {
        if (bs < 0) {
            if (addc_(a, n, aa, an, bb, bn, 0) != 0) {
                ensure(1);
                a[n++] = 1;
            }
            s = -n;
            return *this;
        }
        if (subb_(a, n, bb, bn, aa, an, 0) != 0) {
            negbits_(a, n);
            s = -(n = normalize_(a, n));
            return *this;
        }
        s = n = normalize_(a, n);
        return *this;
    }
    if (bs < 0) {
        if (subb_(a, n, aa, an, bb, bn, 0) != 0) {
            negbits_(a, n);
            s = -(n = normalize_(a, n));
            return *this;
        }
        s = n = normalize_(a, n);
        return *this;
    }
    if (addc_(a, n, aa, an, bb, bn, 0)) {
        ensure(1);
        a[n++] = 1;
    }
    s = n;
    return *this;
}

// *this = (neg ? -1 : 1) * (|*this| + |d|)
alf::integer::builder &
alf::integer::builder::add_(const digit * d, size_t dn, int neg)
{
    if (n < dn)
        length_at_least(dn);
    digit u = addc_(a, n, a, n, d, dn, 0);
    if (u) {
        ensure(1);
        a[n++] = u;
    }
    s = neg ? -n : n;
    return *this;
}

// *this = (neg ? -1 : 1) * (|*this| - |d|)
alf::integer::builder &
alf::integer::builder::sub_(const digit * d, size_t dn, int neg)
{
    if (dn > n)
        length_at_least(dn);
    digit u = subb_(a, n, a, n, d, dn, 0);
    if (u) {
        negbits_(a, n);
        neg = !neg;
    }
    s = neg ? -n : n;
    return *this;
}

// *this = (neg ? -1 : 1) * (|d| - |*this|)
alf::integer::builder &
alf::integer::builder::sub2_(const digit * d, size_t dn, int neg)
{
    if (n < dn)
        length_at_least(dn);
    digit u = subb_(a, n, d, dn, a, n, 0);
    if (u) {
        negbits_(a, n);
        neg = !neg;
    }
    s = neg ? -n : n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::add(const digit * d, size_t dn, ssize_t ds)
{
    return add(a, n, s, d, dn, ds);
}

alf::integer::builder &
alf::integer::builder::add(const builder & a, const builder & b)
{
    return add(a.a, a.n, a.s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::add(int k)
{
    builder b(k);
    return add(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::add(unsigned int k)
{
    builder b(k);
    return add(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::add(long int k)
{
    builder b(k);
    return add(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::add(unsigned long int k)
{
    builder b(k);
    return add(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::add(long long int k)
{
    builder b(k);
    return add(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::add(unsigned long long int k)
{
    builder b(k);
    return add(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::sub(const digit * aa, size_t an, ssize_t as,
                            const digit * bb, size_t bn, ssize_t bs)
{
    if (n < an)
        length_at_least(an);
    if (n < bn)
        length_at_least(bn);
    if (as < 0) {
        if (bs < 0) {
            if (subb_(a, n, bb, bn, aa, an, 0) != 0) {
                negbits_(a, n);
                s = -(n = normalize_(a, n));
                return *this;
            }
            s = n = normalize_(a, n);
            return *this;
        }
        if (addc_(a, n, aa, an, bb, bn, 0) != 0) {
            ensure(1);
            a[n++] = 1;
        }
        s = -n;
        return *this;
    }
    if (bs < 0) {
        if (addc_(a, n, aa, an, bb, bn, 0) != 0) {
            ensure(1);
            a[n++] = 1;
        }
        s = n;
        return *this;
    }
    if (subb_(a, n, aa, an, bb, bn, 0) != 0) {
        negbits_(a, n);
        s = -(n = normalize_(a, n));
        return *this;
    }
    s = n = normalize_(a, n);
    return *this;
}

alf::integer::builder &
alf::integer::builder::sub(const digit * d, size_t dn, ssize_t ds)
{
    return sub(a, n, s, d, dn, ds);
}

alf::integer::builder &
alf::integer::builder::sub(const builder & aa, const builder & bb)
{
    return sub(aa.a, aa.n, aa.s, bb.a, bb.n, bb.s);
}

alf::integer::builder &
alf::integer::builder::sub(int k)
{
    builder b(k);
    return sub(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::sub(unsigned int k)
{
    builder b(k);
    return sub(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::sub(long int k)
{
    builder b(k);
    return sub(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::sub(unsigned long int k)
{
    builder b(k);
    return sub(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::sub(long long int k)
{
    builder b(k);
    return sub(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::sub(unsigned long long int k)
{
    builder b(k);
    return sub(a, n, s, b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::mul(const digit * aa, size_t an, ssize_t as,
                            const digit * ba, size_t bn, ssize_t bs)
{
    int neg = (as < 0) != (bs < 0);
    size_t zn = an + bn;
    // if either or both of a and b is the same variable as this we need
    // to be careful. We therefore multiply into a new builder and then
    // assign it after.
    builder c;
    c.length_at_least(zn);
    muladd_(c.a, zn, aa, an, ba, bn);
    c.n = normalize_(c.a, c.n);
    assign(c);
    s = neg ? -n: n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::mul(int k)
{
    builder b(k);
    return mul(b);
}

alf::integer::builder &
alf::integer::builder::mul(unsigned int k)
{
    builder b(k);
    return mul(b);
}

alf::integer::builder &
alf::integer::builder::mul(long int k)
{
    builder b(k);
    return mul(b);
}

alf::integer::builder &
alf::integer::builder::mul(unsigned long int k)
{
    builder b(k);
    return mul(b);
}

alf::integer::builder &
alf::integer::builder::mul(long long int k)
{
    builder b(k);
    return mul(b);
}

alf::integer::builder &
alf::integer::builder::mul(unsigned long long int k)
{
    builder b(k);
    return mul(b);
}

// static
int
alf::integer::builder::divmod(digit * q, size_t * qnptr, ssize_t * qsptr,
                                digit * r, size_t * rnptr, ssize_t * rsptr,
                                const digit * a, size_t an, ssize_t as,
                                const digit * b, size_t bn, ssize_t bs)
{
    // |a| == A, |b| == B, |q| == Q and |r| == R

    //   a    b     q    r
    //   pos  pos   pos  pos  a = qb+r = QB+R
    //   neg  pos   neg  neg  a = qb+r = (-Q)B+-R
    //   pos  neg   neg  pos  a = qb+r = (-Q)(-B) + R
    //   neg  neg   pos  neg  a = qb+r = Q(-B) + (-R)

    if (q == 0 && r == 0) return -2;
    if (q != 0 && (qnptr == 0 || qsptr == 0)) return -2;
    if (r != 0 && (rnptr == 0 || rsptr == 0)) return -2;
    ssize_t z = an - bn;
    if (z < 0) z = 0;
    size_t zz = z + 1;
    if (q && *qnptr < zz) return -2; // not enough space in q.
    if (r && *rnptr < bn) return -2; // not enough space in r.
    int j = div_(q, qnptr, r, rnptr, a, an, b, bn);
    if (j < 0) return j;
    if (as < 0) {
        if (bs < 0) {
            if (q) *qsptr = *qnptr = z;
            if (r) *rsptr = -(*rnptr = bn);
        } else {
            if (q) *qsptr = -(*qnptr = z);
            if (r) *rsptr = -(*rnptr = bn);
        }
    } else if (bs < 0) {
        if (q) *qsptr = -(*qnptr = z);
        if (r) *rsptr = *rnptr = bn;
    } else {
        if (q) *qsptr = *qnptr = z;
        if (r) *rsptr = *rnptr = bn;
    }
    return j;
}

alf::integer::builder &
alf::integer::builder::div(const digit * d, size_t dn, ssize_t ds)
{
    int j = divmod(a, &n, &s, 0, 0, 0,
                    a, n, s, d, dn, ds);
    // TODO: throw exception on error.
    return *this;
}

alf::integer::builder &
alf::integer::builder::div(int k)
{
    builder b(k);
    return div(b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::div(unsigned int k)
{
    builder b(k);
    return div(b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::div(long int k)
{
    builder b(k);
    return div(b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::div(unsigned long int k)
{
    builder b(k);
    return div(b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::div(long long int k)
{
    builder b(k);
    return div(b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::div(unsigned long long int k)
{
    builder b(k);
    return div(b.a, b.n, b.s);
}

alf::integer::builder &
alf::integer::builder::mod(const digit * d, size_t dn, ssize_t ds)
{
    length_at_least(dn);
    int j = divmod(0, 0, 0, a, &n, &s,
                    a, n, s, d, dn, ds);
    // TODO: throw exception on error.
    return *this;
}

alf::integer::builder &
alf::integer::builder::mod(int k)
{
    builder b(k);
    return mod(b);
}

alf::integer::builder &
alf::integer::builder::mod(unsigned int k)
{
    builder b(k);
    return mod(b);
}

alf::integer::builder &
alf::integer::builder::mod(long int k)
{
    builder b(k);
    return mod(b);
}

alf::integer::builder &
alf::integer::builder::mod(unsigned long int k)
{
    builder b(k);
    return mod(b);
}

alf::integer::builder &
alf::integer::builder::mod(long long int k)
{
    builder b(k);
    return mod(b);
}

alf::integer::builder &
alf::integer::builder::mod(unsigned long long int k)
{
    builder b(k);
    return mod(b);
}

// static
int alf::integer::builder::divmod(builder & q, builder & r,
                                    const digit * a, size_t an, ssize_t as,
                                    const digit * b, size_t bn, ssize_t bs)
{
    ssize_t zs = an - bn;
    if (zs < 0) zs = 0;
    size_t zn = zs + 1;
    builder qq, rr;
    qq.length_at_least(zn);
    rr.length_at_least(bn);
    int j = divmod(qq.a, & qq.n, & qq.s,
                    rr.a, & rr.n, & rr.s,
                    a, an, as,
                    b, bn, bs);
    q.assign(qq);
    r.assign(rr);
    return j;
}

// *this = a**b % c, c can be 0 in which case we do no modulus.
// static
int
alf::integer::builder::pow_simple(builder & res, const builder & a,
                                    digit b, const builder & c)
{
    builder x(1);
    builder f(a);

    if (c.n > 0) f.mod(c);
    while (b) {
        if (b & 1) {
            x.mul(f);
            if (c.n > 0) x.mod(c);
        }
        f.mul(f);
        if (c.n > 0) f.mod(c);
        b >>= 1;
    }
    res.assign(x);
    return res.n;
}

// *this = a**b % c, c can be 0 in which case we do no modulus.
int
alf::integer::builder::pow(const digit * aa, size_t an, ssize_t as,
                                const digit * bb, size_t bn, ssize_t bs,
                                const digit * cc, size_t cn, ssize_t cs)
{
    builder a(aa, an, as), c, d, res, t1, tj;
    digit b;
    int cmpres;
    size_t i, j, k;

    // b cannot be too high and cannot be negative.
    switch (bs) {
    case 0:
        // a**0 is always 1, ignore c.
        assign(1);
        return 1;
    case 1:
        b = *bb;
        if (cc == 0 && b >= (1<<24)) return -2;
        if (b == 1) {
            // a**1 == a.
            res.assign(a);
            if (cc) {
                if (cs <= 0) return -2;
                res.mod(cc, cn, cn);
            }
            res.n = normalize_(res.a, res.n);
            assign(res);
            return n;
        }
        break;
    default:
        if (bs < 0 || cc == 0) return -2;
        break;
    }
    if (as == 0) {
        assign(0);
        return 0;
    }
    // move both a and c over to builders.
    if (cc) {
        // we also do not want c to be negative.
        if (cs <= 0) return -2;
        c.assign(cc, cn, cs);
        // if |a| >= c we take the modulus right away.
        if ((cmpres = c.cmp(aa, an, an)) < 0)
            a.mod(cc, cn, cs); // |a| >= c so we take mod.
        a.normalize();
        if (a.n == 0) {
            // a is 0 modulus c, 0**anything == 0.
            assign(0);
            return 0;
        }
    }
    if (bs == 1 && b <= 32) {
        // just compute the pow using simple algorithm.
        pow_simple(*this, a, b, c);
        return n;
    }
    k = 0;
    res.assign(1);
    t1.assign(a);
    bool need_t1 = false;
    while (k < bn) {
        // (*this * A**B) % c == a**b % c
        // initially A == a and B == b and *this == 1
        // so it is trivially true.
        // we examine the lowest 5 bits of B and multiply
        // *this by tbl[B & 31] and do the mod c if necessary.
        // if we then drop those bits from B by setting B = B >> 5
        // and also replace A with A**32 % c and repopulate the table
        // accordingly so that tbl is A**k % c for any k <= k <=32
        // A**32 % c is of course tbl[32].
        // each digit has 6 groups of 5 bits.
        // at end B is reduced to 0 so A**B is trivially 1
        // and *this == a**b % c as we want.
        b = bb[k++];
        for (i = 0; i < 6; ++i) {
            if (k == bn && b == 0) break;
            j = b & 31;
            if (need_t1) {
                tj.assign(t1);
                pow_simple(t1, tj, 32, c);
            }
            if (j) {
                pow_simple(tj, t1, j, c);
                res.mul(tj);
                if (c.n > 0) res.mod(c);
            }
            b >>= 5;
            need_t1 = true;
        }
    }
    assign(res);
    return n;
}

int
alf::integer::builder::fact(int kkk)
{
    unsigned long long int zz;
    unsigned long long int uu;
    unsigned long long int jj;
    unsigned long long int kk;
    unsigned int z = 1;
    unsigned int u;
    unsigned int k;
    unsigned int j = 0;

    if (kkk < 0) return -2;
    k = (unsigned int)kkk;
    assign(1);
    while (++j <= k) {
        u = z * j;
        if (u < z) {
            zz = z;
            jj = j;
            kk = k;
            while (jj <= kk) {
                uu = zz * jj;
                if (uu < zz) {
                    assign(zz);
                    j = jj;
                    while (j <= k)
                        mul(j++);
                    return n;
                }
                zz = uu;
                ++jj;
            }
            assign(zz);
            return n;
        }
        z = u;
    }
    assign(z);
    return n;
}

int
alf::integer::builder::fact(unsigned int k)
{
    unsigned long long int zz;
    unsigned long long int uu;
    unsigned long long int jj;
    unsigned long long int kk;
    unsigned int z = 1;
    unsigned int u;
    unsigned int j = 0;

    assign(1);
    while (++j <= k) {
        u = z * j;
        if (u < z) {
            zz = z;
            jj = j;
            kk = k;
            while (jj <= kk) {
                uu = zz * jj;
                if (uu < zz) {
                    assign(zz);
                    j = jj;
                    while (j <= k)
                        mul(j++);
                    return n;
                }
                zz = uu;
                ++jj;
            }
            assign(zz);
            return n;
        }
        z = u;
    }
    assign(z);
    return n;
}

int
alf::integer::builder::fact(unsigned long long int kk)
{
    unsigned long long int zz;
    unsigned long long int uu;
    unsigned long long int jj;
    unsigned int z = 1;
    unsigned int u;
    unsigned int k = kk >= UINT_MAX ? UINT_MAX - 1 : (unsigned int)kk;
    unsigned int j = 0;

    assign(1);
    while (++j <= k) {
        u = z * j;
        if (u < z) {
            zz = z;
            jj = j;
            while (jj <= kk) {
                uu = zz * jj;
                if (uu < zz) {
                    assign(zz);
                    while (jj <= kk)
                        mul(jj++);
                    return n;
                }
                zz = uu;
                ++jj;
            }
            assign(zz);
            return n;
        }
        z = u;
    }
    assign(z);
    return n;
}

int
alf::integer::builder::fact(const digit * a, size_t an, ssize_t as)
{
    unsigned long long int z;

    switch (as) {
    case 0: return fact(0);
    case 1: return fact(int(*a));
    case 2: get(z); return fact(z);
    }
    return -2;
}

// static
void
alf::integer::builder::bit_and_(digit * c, size_t * cnptr, ssize_t * csptr,
                                const digit * a, size_t an, ssize_t as,
                                const digit * b, size_t bn, ssize_t bs)
{
    // negation states.
    enum {
        NEG_NONE, // don't negate.
        NEG_ZERO = 0x01, // only seen 0 values so far.
        NEG_INV = 0x03, // invert rest.
    };

    size_t cn = *cnptr;
    size_t k = 0;
    digit ax, bx;
    int ans = NEG_NONE, bns = NEG_NONE, cns = NEG_NONE, neg = 0;
    if (cn < an) an = cn;
    if (cn < bn) bn = cn;
    size_t u = an < bn ? an : bn;
    if (as < 0) { ++neg; ans = NEG_ZERO; }
    if (bs < 0) { ++neg; bns = NEG_ZERO; }
    neg = (neg == 2);
    if (neg) cns = NEG_ZERO;
    while (k < u) {
        ax = a[k]; bx = b[k];
        switch (ans) {
        case NEG_ZERO:
            if (ax!=0) { ans = NEG_INV; ax = (-ax) & BITS_MASK; }
            break;
        case NEG_INV: ax ^= BITS_MASK; break;
        }
        switch (bns) {
        case NEG_ZERO:
            if (bx!=0) { bns = NEG_INV; bx = (-bx) & BITS_MASK; }
            break;
        case NEG_INV: bx ^= BITS_MASK; break;
        }
        ax &= bx & BITS_MASK;
        switch (cns) {
        case NEG_ZERO:
            if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
            break;
        case NEG_INV: ax ^= BITS_MASK; break;
        }
        c[k++] = ax;
    }
    if (k < an) {
        // an > bn and k == bn at this point. cn >= an.
        do {
            ax = a[k]; bx = bns == NEG_INV ? BITS_MASK : 0;
            switch (ans) {
            case NEG_ZERO:
                if (ax!=0) { ans = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK; break;
            }
            ax &= bx & BITS_MASK;
            switch (cns) {
            case NEG_ZERO:
                if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK;
            }
            c[k++] = ax;
        } while (k < an);
    } else if (k < bn) {
        // an < bn and k == an at this point. cn >= bn.
        do {
            ax = ans == NEG_INV ? BITS_MASK : 0; bx = b[k];
            switch (bns) {
            case NEG_ZERO:
                if (bx!=0) { bns = NEG_INV; bx = (-bx) & BITS_MASK; }
                break;
            case NEG_INV: bx ^= BITS_MASK; break;
            }
            ax &= bx & BITS_MASK;
            switch (cns) {
            case NEG_ZERO:
                if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK;
            }
            c[k++] = ax;
        } while (k < bn);
    }
    ax = ans == NEG_INV ? BITS_MASK : 0;
    bx = bns == NEG_INV ? BITS_MASK : 0;
    ax &= bx & BITS_MASK;
    if (k < cn && ax!=0 && cns == NEG_ZERO) {
        cns = NEG_INV; c[k++] = (-ax) & BITS_MASK;
    }
    if (k < cn && cns == NEG_INV) ax ^= BITS_MASK;
    while (k < cn)
        c[k++] = ax;
    ax = cns == NEG_NONE ? 0 : BITS_MASK;
    while (cn > 0 && c[cn-1] == ax) --cn;
    *cnptr = cn;
    *csptr = neg ? -cn : cn;
}

alf::integer::builder &
alf::integer::builder::bit_and(const digit * d, size_t dn, ssize_t ds)
{
    bit_and_(a, & n, & s, a, n, s, d, dn, ds);
    return *this;
}

alf::integer::builder &
alf::integer::builder::bit_and(int k)
{
    builder b(k);
    return bit_and(b);
}

alf::integer::builder &
alf::integer::builder::bit_and(unsigned int k)
{
    builder b(k);
    return bit_and(b);
}

alf::integer::builder &
alf::integer::builder::bit_and(long int k)
{
    builder b(k);
    return bit_and(b);
}

alf::integer::builder &
alf::integer::builder::bit_and(unsigned long int k)
{
    builder b(k);
    return bit_and(b);
}

alf::integer::builder &
alf::integer::builder::bit_and(long long int k)
{
    builder b(k);
    return bit_and(b);
}

alf::integer::builder &
alf::integer::builder::bit_and(unsigned long long int k)
{
    builder b(k);
    return bit_and(b);
}

// static
void
alf::integer::builder::bit_andnot_(digit * c, size_t * cnptr, ssize_t * csptr,
                                    const digit * a, size_t an, ssize_t as,
                                    const digit * b, size_t bn, ssize_t bs)
{
    // negation states.
    enum {
        NEG_NONE, // don't negate.
        NEG_ZERO = 0x01, // only seen 0 values so far.
        NEG_INV = 0x03, // invert rest.
    };

    size_t cn = *cnptr;
    size_t k = 0;
    digit ax, bx;
    // this one will treat result as negative iff a < 0 and b >= 0.
    int ans = NEG_NONE, bns = NEG_NONE, cns = NEG_NONE, neg = 0;
    if (cn < an) an = cn;
    if (cn < bn) bn = cn;
    size_t u = an < bn ? an : bn;
    if (as < 0) { ++neg; ans = NEG_ZERO; }
    if (bs < 0) bns = NEG_ZERO; else ++neg;
    neg = (neg == 2);
    if (neg) cns = NEG_ZERO;
    while (k < u) {
        ax = a[k]; bx = b[k];
        switch (ans) {
        case NEG_ZERO:
            if (ax!=0) { ans = NEG_INV; ax = (-ax) & BITS_MASK; }
            break;
        case NEG_INV: ax ^= BITS_MASK; break;
        }
        switch (bns) {
        case NEG_ZERO:
            if (bx!=0) { bns = NEG_INV; bx = (-bx) & BITS_MASK; }
            break;
        case NEG_INV: bx ^= BITS_MASK; break;
        }
        ax = ax & (~bx) & BITS_MASK;
        switch (cns) {
        case NEG_ZERO:
            if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
            break;
        case NEG_INV: ax ^= BITS_MASK; break;
        }
        c[k++] = ax;
    }
    if (k < an) {
        // an > bn and k == bn at this point. cn >= an.
        do {
            ax = a[k]; bx = bns == NEG_INV ? BITS_MASK : 0;
            switch (ans) {
            case NEG_ZERO:
                if (ax!=0) { ans = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK; break;
            }
            ax = ax & (~bx) & BITS_MASK;
            switch (cns) {
            case NEG_ZERO:
                if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK;
            }
            c[k++] = ax;
        } while (k < an);
    } else if (k < bn) {
        // an < bn and k == an at this point. cn >= bn.
        do {
            ax = ans == NEG_INV ? BITS_MASK : 0; bx = b[k];
            switch (bns) {
            case NEG_ZERO:
                if (bx!=0) { bns = NEG_INV; bx = (-bx) & BITS_MASK; }
                break;
            case NEG_INV: bx ^= BITS_MASK; break;
            }
            ax = ax & (~bx) & BITS_MASK;
            switch (cns) {
            case NEG_ZERO:
                if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK;
            }
            c[k++] = ax;
        } while (k < bn);
    }
    ax = ans == NEG_INV ? BITS_MASK : 0;
    bx = bns == NEG_INV ? BITS_MASK : 0;
    ax = ax & (~bx) & BITS_MASK;
    if (k < cn && ax!=0 && cns == NEG_ZERO) {
        cns = NEG_INV; c[k++] = (-ax) & BITS_MASK;
    }
    if (k < cn && cns == NEG_INV) ax ^= BITS_MASK;
    while (k < cn)
        c[k++] = ax;
    ax = cns == NEG_NONE ? 0 : BITS_MASK;
    while (cn > 0 && c[cn-1] == ax) --cn;
    *cnptr = cn;
    *csptr = neg ? -cn : cn;
}

alf::integer::builder &
alf::integer::builder::bit_andnot(const digit * d, size_t dn, ssize_t ds)
{
    bit_andnot_(a, & n, & s, a, n, s, d, dn, ds);
    return *this;
}

alf::integer::builder &
alf::integer::builder::bit_andnot(int k)
{
    builder b(k);
    return bit_andnot(b);
}

alf::integer::builder &
alf::integer::builder::bit_andnot(unsigned int k)
{
    builder b(k);
    return bit_andnot(b);
}

alf::integer::builder &
alf::integer::builder::bit_andnot(long int k)
{
    builder b(k);
    return bit_andnot(b);
}

alf::integer::builder &
alf::integer::builder::bit_andnot(unsigned long int k)
{
    builder b(k);
    return bit_andnot(b);
}

alf::integer::builder &
alf::integer::builder::bit_andnot(long long int k)
{
    builder b(k);
    return bit_andnot(b);
}

alf::integer::builder &
alf::integer::builder::bit_andnot(unsigned long long int k)
{
    builder b(k);
    return bit_andnot(b);
}

// static
void
alf::integer::builder::bit_or_(digit * c, size_t * cnptr, ssize_t * csptr,
                                const digit * a, size_t an, ssize_t as,
                                const digit * b, size_t bn, ssize_t bs)
{
    // negation states.
    enum {
        NEG_NONE, // don't negate.
        NEG_ZERO = 0x01, // only seen 0 values so far.
        NEG_INV = 0x03, // invert rest.
    };

    size_t cn = *cnptr;
    size_t k = 0;
    digit ax, bx;
    int ans = NEG_NONE, bns = NEG_NONE, cns = NEG_NONE, neg = 0;
    if (cn < an) an = cn;
    if (cn < bn) bn = cn;
    size_t u = an < bn ? an : bn;
    // result is negative if either a or b is negative. I.e. neg != 0.
    if (as < 0) { ++neg; ans = cns = NEG_ZERO; }
    if (bs < 0) { ++neg; bns = cns = NEG_ZERO; }
    while (k < u) {
        ax = a[k]; bx = b[k];
        switch (ans) {
        case NEG_ZERO:
            if (ax!=0) { ans = NEG_INV; ax = (-ax) & BITS_MASK; }
            break;
        case NEG_INV: ax ^= BITS_MASK; break;
        }
        switch (bns) {
        case NEG_ZERO:
            if (bx!=0) { bns = NEG_INV; bx = (-bx) & BITS_MASK; }
            break;
        case NEG_INV: bx ^= BITS_MASK; break;
        }
        ax = (ax | bx) & BITS_MASK;
        switch (cns) {
        case NEG_ZERO:
            if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
            break;
        case NEG_INV: ax ^= BITS_MASK; break;
        }
        c[k++] = ax;
    }
    if (k < an) {
        // an > bn and k == bn at this point. cn >= an.
        do {
            ax = a[k]; bx = bns == NEG_INV ? BITS_MASK : 0;
            switch (ans) {
            case NEG_ZERO:
                if (ax!=0) { ans = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK; break;
            }
            ax = (ax | bx) & BITS_MASK;
            switch (cns) {
            case NEG_ZERO:
                if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK;
            }
            c[k++] = ax;
        } while (k < an);
    } else if (k < bn) {
        // an < bn and k == an at this point. cn >= bn.
        do {
            ax = ans == NEG_INV ? BITS_MASK : 0; bx = b[k];
            switch (bns) {
            case NEG_ZERO:
                if (bx!=0) { bns = NEG_INV; bx = (-bx) & BITS_MASK; }
                break;
            case NEG_INV: bx ^= BITS_MASK; break;
            }
            ax = (ax | bx) & BITS_MASK;
            switch (cns) {
            case NEG_ZERO:
                if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK;
            }
            c[k++] = ax;
        } while (k < bn);
    }
    ax = ans == NEG_INV ? BITS_MASK : 0;
    bx = bns == NEG_INV ? BITS_MASK : 0;
    ax = (ax | bx) & BITS_MASK;
    if (k < cn && ax!=0 && cns == NEG_ZERO) {
        cns = NEG_INV; c[k++] = (-ax) & BITS_MASK;
    }
    if (k < cn && cns == NEG_INV) ax ^= BITS_MASK;
    while (k < cn)
        c[k++] = ax;
    ax = cns == NEG_NONE ? 0 : BITS_MASK;
    while (cn > 0 && c[cn-1] == ax) --cn;
    *cnptr = cn;
    *csptr = neg != 0 ? -cn : cn;
}

alf::integer::builder &
alf::integer::builder::bit_or(const digit * d, size_t dn, ssize_t ds)
{
    bit_or_(a, & n, & s, a, n, s, d, dn, ds);
    return *this;
}

alf::integer::builder &
alf::integer::builder::bit_or(int k)
{
    builder b(k);
    return bit_or(b);
}

alf::integer::builder &
alf::integer::builder::bit_or(unsigned int k)
{
    builder b(k);
    return bit_or(b);
}

alf::integer::builder &
alf::integer::builder::bit_or(long int k)
{
    builder b(k);
    return bit_or(b);
}

alf::integer::builder &
alf::integer::builder::bit_or(unsigned long int k)
{
    builder b(k);
    return bit_or(b);
}

alf::integer::builder &
alf::integer::builder::bit_or(long long int k)
{
    builder b(k);
    return bit_or(b);
}

alf::integer::builder &
alf::integer::builder::bit_or(unsigned long long int k)
{
    builder b(k);
    return bit_or(b);
}

// static
void
alf::integer::builder::bit_xor_(digit * c, size_t * cnptr, ssize_t * csptr,
                                const digit * a, size_t an, ssize_t as,
                                const digit * b, size_t bn, ssize_t bs)
{
    // negation states.
    enum {
        NEG_NONE, // don't negate.
        NEG_ZERO = 0x01, // only seen 0 values so far.
        NEG_INV = 0x03, // invert rest.
    };

    size_t cn = *cnptr;
    size_t k = 0;
    digit ax, bx;
    int ans = NEG_NONE, bns = NEG_NONE, cns = NEG_NONE, neg = 0;
    if (cn < an) an = cn;
    if (cn < bn) bn = cn;
    size_t u = an < bn ? an : bn;
    // result is negative if the two have different signs.
    // i.e. neg == 1.
    if (as < 0) { ++neg; ans = NEG_ZERO; }
    if (bs < 0) { ++neg; bns = NEG_ZERO; }
    neg = (neg == 1);
    if (neg) cns = NEG_ZERO;
    while (k < u) {
        ax = a[k]; bx = b[k];
        switch (ans) {
        case NEG_ZERO:
            if (ax!=0) { ans = NEG_INV; ax = (-ax) & BITS_MASK; }
            break;
        case NEG_INV: ax ^= BITS_MASK; break;
        }
        switch (bns) {
        case NEG_ZERO:
            if (bx!=0) { bns = NEG_INV; bx = (-bx) & BITS_MASK; }
            break;
        case NEG_INV: bx ^= BITS_MASK; break;
        }
        ax = (ax^bx) & BITS_MASK;
        switch (cns) {
        case NEG_ZERO:
            if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
            break;
        case NEG_INV: ax ^= BITS_MASK; break;
        }
        c[k++] = ax & BITS_MASK;
    }
    if (k < an) {
        // an > bn and k == bn at this point. cn >= an.
        do {
            ax = a[k]; bx = bns == NEG_INV ? BITS_MASK : 0;
            switch (ans) {
            case NEG_ZERO:
                if (ax!=0) { ans = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK; break;
            }
            ax = (ax^bx) & BITS_MASK;
            switch (cns) {
            case NEG_ZERO:
                if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK;
            }
            c[k++] = ax;
        } while (k < an);
    } else if (k < bn) {
        // an < bn and k == an at this point. cn >= bn.
        do {
            ax = ans == NEG_INV ? BITS_MASK : 0; bx = b[k];
            switch (bns) {
            case NEG_ZERO:
                if (bx!=0) { bns = NEG_INV; bx = (-bx) & BITS_MASK; }
                break;
            case NEG_INV: bx ^= BITS_MASK; break;
            }
            ax = (ax^bx) & BITS_MASK;
            switch (cns) {
            case NEG_ZERO:
                if (ax!=0) { cns = NEG_INV; ax = (-ax) & BITS_MASK; }
                break;
            case NEG_INV: ax ^= BITS_MASK;
            }
            c[k++] = ax;
        } while (k < bn);
    }
    ax = ans == NEG_INV ? BITS_MASK : 0;
    bx = bns == NEG_INV ? BITS_MASK : 0;
    ax = (ax^bx) & BITS_MASK;
    if (k < cn && ax!=0 && cns == NEG_ZERO) {
        cns = NEG_INV; c[k++] = (-ax) & BITS_MASK;
    }
    if (k < cn && cns == NEG_INV) ax ^= BITS_MASK;
    while (k < cn)
        c[k++] = ax;
    ax = cns == NEG_NONE ? 0 : BITS_MASK;
    while (cn > 0 && c[cn-1] == ax) --cn;
    *cnptr = cn;
    *csptr = neg ? -cn : cn;
}

alf::integer::builder &
alf::integer::builder::bit_xor(const digit * d, size_t dn, ssize_t ds)
{
    bit_xor_(a, & n, & s, a, n, s, d, dn, ds);
    return *this;
}

alf::integer::builder &
alf::integer::builder::bit_xor(int k)
{
    builder b(k);
    return bit_xor(b);
}

alf::integer::builder &
alf::integer::builder::bit_xor(unsigned int k)
{
    builder b(k);
    return bit_xor(b);
}

alf::integer::builder &
alf::integer::builder::bit_xor(long int k)
{
    builder b(k);
    return bit_xor(b);
}

alf::integer::builder &
alf::integer::builder::bit_xor(unsigned long int k)
{
    builder b(k);
    return bit_xor(b);
}

alf::integer::builder &
alf::integer::builder::bit_xor(long long int k)
{
    builder b(k);
    return bit_xor(b);
}

alf::integer::builder &
alf::integer::builder::bit_xor(unsigned long long int k)
{
    builder b(k);
    return bit_xor(b);
}

alf::integer::builder &
alf::integer::builder::shl(const digit * d, size_t dn, ssize_t ds)
{
    unsigned long long int kk;
    unsigned int k;

    switch (dn = normalize_(d, dn)) {
    case 0: return *this;
    case 1:
        k = *d;
        if (ds < 0) return shr(k);
        return shl(k);
    }
    // TODO throw error
    return *this;
}

alf::integer::builder &
alf::integer::builder::shl(int k)
{
    if (k < 0) return shr((unsigned int)-k);
    return shl((unsigned int)k);
}

alf::integer::builder &
alf::integer::builder::shl(unsigned int k)
{
    if (n == 0) return *this;
    if (k >= (1 << 24)) return *this; // TODO: throw error.
    unsigned int w = k / 30;
    unsigned int b = k % 30;
    ensure(w+1);
    size_t kk = n;
    size_t j = w + n;
    if (w) {
        while (kk > 0)
            a[--j] = a[--kk];
        memset(a, 0, w*sizeof(digit));
        n += w;
    }
    if (b) {
        kk = j = n++;
        digit v1 = --kk >= 0 ? a[kk] : 0;
        unsigned int r = 30 - b;
        a[j--] = v1 >> r;
        while (kk > w) {
            digit v2 = a[--kk];
            a[j--] = ((v2 >> r) | (v1 << b)) & BITS_MASK;
            v1 = v2;
        }
        a[j] = (v1 << b) & BITS_MASK;
    }
    s = s < 0 ? -n : n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::shl(long int k)
{
    if (k < 0) return shr((unsigned long int)-k);
    return shl((unsigned long int)k);
}

alf::integer::builder &
alf::integer::builder::shl(unsigned long int k)
{
    if (k >= (1 << 24)) return *this; // TODO: throw error.
    return shl((unsigned int)k);
}

alf::integer::builder &
alf::integer::builder::shl(long long int k)
{
    if (k < 0) return shr((unsigned long long int)-k);
    return shl((unsigned long long int)k);
}

alf::integer::builder &
alf::integer::builder::shl(unsigned long long int k)
{
    if (k >= (1 << 24)) return *this; // TODO: throw error.
    return shl((unsigned int)k);
}

alf::integer::builder &
alf::integer::builder::shr(const digit * d, size_t dn, ssize_t ds)
{
    unsigned long long int kk;
    unsigned int k;

    switch (dn = normalize_(d, dn)) {
    case 0: return *this;
    case 1:
        k = *d;
        if (ds < 0) return shl(k);
        return shr(k);
    }
    // TODO throw error
    return *this;
}

alf::integer::builder &
alf::integer::builder::shr(int k)
{
    if (k < 0) return shl((unsigned int)-k);
    return shr((unsigned int)k);
}

alf::integer::builder &
alf::integer::builder::shr(unsigned int k)
{
    if (n == 0) return *this;
    if (k >= (1 << 24)) return *this; // TODO: throw error.
    unsigned int w = k / 30;
    unsigned int b = k % 30;
    if (w >= n) {
        if (s < 0) { n = 1; s = -1; *a = 1; return *this; }
        n = 0; s = 0; return *this;
    }
    size_t kk = 0;
    size_t j = w;
    if (w) {
        while (j < n)
            a[kk++] = a[j++];
        n -= w;
    }
    if (b) {
        kk = j = n;
        digit v1 = a[--kk];
        a[--j] = v1 >> b;
        unsigned int r = 30 - b;
        while (kk > 0) {
            digit v2 = a[--kk];
            a[--j] = ((v1 << r) | (v2 >> b)) & BITS_MASK;
            v1 = v2;
        }
    }
    n = normalize_(a, n);
    s = s < 0 ? -n : n;
    return *this;
}

alf::integer::builder &
alf::integer::builder::shr(long int k)
{
    if (k < 0) return shl((unsigned long int)-k);
    return shr((unsigned long int)k);
}

alf::integer::builder &
alf::integer::builder::shr(unsigned long int k)
{
    if (k >= (1 << 24)) return *this; // TODO: throw error.
    return shr((unsigned int)k);
}

alf::integer::builder &
alf::integer::builder::shr(long long int k)
{
    if (k < 0) return shl((unsigned long long int)-k);
    return shr((unsigned long long int)k);
}

alf::integer::builder &
alf::integer::builder::shr(unsigned long long int k)
{
    if (k >= (1 << 24)) return *this; // TODO: throw error.
    return shr((unsigned int)k);
}


// ----------------
// static functions operator on digit arrays.

// static
size_t
alf::integer::builder::normalize_(const digit * a, size_t an)
{
    size_t j = an;
    while (j > 0 && a[j-1] == 0) --j;
    return j;
}

// we assume cn >= an.
// static
void
alf::integer::builder::negbits2_(digit * c, size_t cn,
                                    const digit * a, size_t an)
{
    size_t j = 0;
    if (a!=c)
        while (j < an && a[j] == 0) c[j++] = 0;
    else
        while (j < an && a[j] == 0) ++j;
    if (j == an) {
        while (j < cn)
            c[j++] = 0;
    } else {
        c[j] = (-a[j]) & BITS_MASK;
        while (++j < an)
            c[j] = a[j]^BITS_MASK;
        while (j < cn)
            c[j++] = BITS_MASK;
    }
}

// we assume cn >= an
// static
void
alf::integer::builder::invbits2_(digit * c, size_t cn,
                                    const digit * a, size_t an)
{
    size_t j = 0;
    while(j < an) {
        c[j] = a[j]^BITS_MASK;
        ++j;
    }
    while (j < cn)
        c[j++] = BITS_MASK;
}

// static
alf::integer::digit
alf::integer::builder::add1_(digit * a, size_t an)
{
    size_t k = 0;
    digit w = 1;
    while (k < an) {
        w += a[k];
        a[k++] = digit(w & BITS_MASK);
        w >>= BITS;
    }
    return w;
}

// static
bool
alf::integer::builder::sub1_(digit * a, size_t an)
{
    ssize_t k = 0;
    while (k < an && a[k] == 0) ++k;
    if (k == an) return true;
    --a[k];
    while (k > 0) a[--k] = BITS_MASK;
    return false;
}

// c = a + b + carry, return new carry if necessary.
// we must have cn >= max(an,bn).
// c == a or c == b or c == a and c == b are all ok.
// static
alf::integer::digit
alf::integer::builder::addc_(digit * c, size_t cn,
                                const digit * a, size_t an,
                                const digit * b, size_t bn, digit carry)
{
    digit w = carry;
    size_t k;
    if (an < bn) {
        const digit * tmp = a; a = b; b = tmp;
        k = an; an = bn; bn = k;
    }
    // cn >= an >= bn
    for (k=0; k < bn;) {
        w = w + a[k] + b[k];
        c[k++] = w & BITS_MASK;
        w >>= BITS;
    }
    while (k < an) {
        w = w + a[k];
        c[k++] = w & BITS_MASK;
        w >>= BITS;
    }
    while (k < cn && w != 0) {
        c[k++] = w & BITS_MASK;
        w >>= BITS;
    }
    return w;
}

// c = a - b - borrow, return new borrow if necessary.
// we must have cn >= max(an,bn).
// c == a or c == b or c == a and c == b are all ok.
// static
alf::integer::digit
alf::integer::builder::subb_(digit * c, size_t cn,
                                const digit * a, size_t an,
                                const digit * b, size_t bn, digit borrow)
{
    digit ax, bx, k = 0;
    size_t j = 0;
    int borrow_ = borrow;
    int w = 1;

    if (an < bn) {
        while (j < an) {
            w = w + BITS_MASK + a[j] - b[j] - borrow_;
            borrow_ = 0;
            c[j++] = w & BITS_MASK;
            w >>= BITS;
        }
        while (j < bn) {
            w = w + BITS_MASK - b[j] - borrow_;
            borrow_ = 0;
            c[j++] = w & BITS_MASK;
            w >>= BITS;
        }
    } else {
        while (j < bn) {
            w = w + BITS_MASK + a[j] - b[j] - borrow_;
            borrow_ = 0;
            c[j++] = w & BITS_MASK;
            w >>= BITS;
        }
        while (j < an) {
            w = w + BITS_MASK + a[j] - borrow_;
            borrow_ = 0;
            c[j++] = w & BITS_MASK;
            w >>= BITS;
        }
    }
    while (j < cn) {
        w = w + BITS_MASK - borrow_;
        borrow_ = 0;
        c[j++] = w & BITS_MASK;
        w >>= BITS;
    }
    return digit(borrow_ + 1 - w);
}

// d = a*b + c
// static
alf::integer::digit
alf::integer::builder::muladd4_(digit * d, size_t dn,
                                const digit * a, size_t an,
                                digit b, digit c)
{
    dbl_digit w = c;
    dbl_digit bb = b;
    size_t j = 0;
    while (j < an) {
        w = w + bb*a[j];
        d[j++] = digit(w & BITS_MASK);
        w >>= BITS;
    }
    if (j < dn) {
        d[j] = digit(w);
        w = 0;
        while (++j < dn) d[j] = 0;
    }
    return digit(w);
}

// c += a*b
// static
void
alf::integer::builder::muladd_(digit * c, size_t cn,
                                const digit * a, size_t an,
                                digit b)
{
    if ((an = normalize_(a, an)) == 0) return;
    if (b == 0) return;
    dbl_digit w = 0;
    dbl_digit bb = b;
    size_t j = 0;
    while (j < an) {
        w = w + bb*a[j] + c[j];
        c[j++] = digit(w & BITS_MASK);
        w >>= BITS;
    }
    if (j < cn && w != 0) {
        w = w + c[j];
        c[j] = digit(w & BITS_MASK);
        w >>= BITS;
        while (++j < cn && w != 0) {
            w = w + c[j];
            c[j++] = digit(w & BITS_MASK);
            w >>= BITS;
        }
    }
}

// c += a * b
// We assume cn >= an + bn.
// Either an or bn are assumed to be less than a certain cutoff
// value.
// static
void
alf::integer::builder::muladd_short_(digit * c, size_t cn,
                                        const digit * a, size_t an,
                                        const digit * b, size_t bn)
{
    size_t ak;
    if ((an = normalize_(a, an)) == 0) return;
    if ((bn = normalize_(b, bn)) == 0) return;
    if (an > bn) {
        const digit * tmp = a; a = b; b = tmp;
        ak = an; an = bn; bn = ak;
    }
    // an <= bn
    if (an == 1) {
        muladd_(c, cn, b, bn, *a);
        return;
    }
    ak = an >> 1;
    size_t bk = bn >> 1;
    size_t abk = ak + bk;
    muladd_short_(c, cn, a, ak, b, bk);
    muladd_short_(c + ak, cn - ak, a + ak, an - ak,
                    b, bk);
    muladd_short_(c + bk, cn - bk, a, ak, b + bk, bn - bk);
    muladd_short_(c + abk, cn - abk, a + ak, an - ak, b + bk, bn - bk);
}

// c += a * b
// cn >= an + bn.
// bn is assumed to be much smaller than an so that
// an is a sequence of (an + bn -1)/bn chunks.
// static
void
alf::integer::builder::muladd_seq_(digit * c, size_t cn,
                                    const digit * a, size_t an,
                                    const digit * b, size_t bn)
{
    if ((an = normalize_(a, an)) == 0) return;
    if ((bn = normalize_(b, bn)) == 0) return;
    size_t j = 0;
    while (j + bn <= an) {
        muladd_(c + j, cn - j, a + j, bn, b, bn);
        j += bn;
    }
    if (j < an)
        muladd_(c + j, cn - j, a + j, an - j, b, bn);
}

// c += a * b
// cn >= an + bn
// we assume an > bn/2 and bn > 70.
// static
void
alf::integer::builder::muladd_karatsuba_(digit * c, size_t cn,
                                            const digit * a, size_t an,
                                            const digit * b, size_t bn)
{
    size_t k;
    if ((an = normalize_(a, an)) == 0) return;
    if ((bn = normalize_(b, bn)) == 0) return;
    // we want a to be no greater in length than
    // b.
    if (an > bn) {
        const digit * tmp_ = a; a = b; b = tmp_;
        k = an; an = bn; bn = k;
    }
    // an <= bn
    if (an == 1) {
        muladd_(c, cn, b, bn, *a);
        return;
    }

    if ((an <<1) <= bn) {
        // an is less than half of b
        // so we use some other algorithm.
        muladd_seq_(c, cn, b, bn, a, an);
        return;
    }
    k = a == b ? KARATSUBA_SQURE_CUTOFF : KARATSUBA_MUL_CUTOFF;
    if (an <= k) {
        muladd_short_(c, cn, a, an, b, bn);
        return;
    }
    // (an << 1) > bn && an <= bn
    k = bn >> 1;
    // set: a = ah * Z + al and b = bh * Z + bl
    // where Z == R**k.
    // a*b = (ah*Z + al)*(bh*Z + bl)
    //     = ah*bh*ZZ + al*bl + (ah*bl+al*bh)*Z
    //     = ah*bh*ZZ + al*bl + Q*Z
    // where Q = ah*bl + al*bh
    // now, let: (al-ah)(bh-bl)
    // and we get:
    // (al-ah)(bh-bl) = al*bh - al*bl - ah*bh + ah*bl
    //       = Q - al*bl - ah*bh
    // or Q = (al-ah)(bh-bl) - ah*bh - al*bl
    // so that:
    // a*b = ah*bh*(ZZ - Z) - al*bl(Z - 1)
    //    + (al-ah)(bh-bl)*Z
    // create temporary space for albl, ahbh
    // and so on.

    // The formulate is as follows:
    // a = ah * Z + al, b = bh * Z + bl
    // ab = ahbh ZZ + albl + (ahbl + albh) Z == ahbh ZZ + albl + QZ
    // where Q == ahbl + albh
    // but we also have:
    // (al-ah)(bh-bl) == albh - albl - ahbh + ahbl == Q - ahbh - albl
    // so QZ == (al-ah)(bh-bl)Z + ahbhZ + alblZ
    // so:
    // ab = ahbh (ZZ + Z)ahbh + (Z + 1)albl + (al-ah)(bh-bl) Z

    builder albl, ahbh, Q;
    // subttract ah from it.
    int sign = 1;
    size_t abn = an + bn;
    size_t k2 = k + k;
    size_t ank = an - k;
    size_t bnk = bn - k;
    // compute (al - ah)(bh - bl)
    // start with al - ah
    albl.assign(a, k, k);
    albl.sub(a + k, ank, ank);
    if (albl.s < 0) {
        sign = -sign;
        albl.s = albl.n;
    }
    // now bh - bl
    // copy bh into buffer.
    Q.assign(b + k, bnk, bnk);
    Q.sub(b, k, k);
    if (Q.s < 0) {
        sign = -sign;
        Q.s = Q.n;
    }
    // compute (al - ah)(bh - bl)
    Q.mul(albl);
    size_t Qn = Q.n = normalize_(Q.a, Q.n);
    // compute albl
    albl.assign(a, k, k);
    albl.mul(b, k, k);
    albl.normalize();
    // and ahbh.
    ahbh.assign(a + k, ank, ank);
    ahbh.mul(b + k, bnk, bnk);
    ahbh.normalize();
    // now add into c.
    // we do not modify c before this incase a, b or c points
    // to the same data.
    digit carry = addc_(c, cn, c, cn, albl.a, albl.n, 0);
    // carry should be zero here.
    if (carry) {
        printf("carry is %u\n", carry);
        fflush(stdout);
        throw "carry should be zero here\n";
    }
    carry = addc_(c + k2, cn - k2, c + k2, cn - k2, ahbh.a, ahbh.n, 0);
    if (carry) {
        printf("carry is %u\n", carry);
        fflush(stdout);
        throw "carry should be zero here\n";
    }
    digit borrow = addc_(c + k, cn - k, c + k, cn - k, albl.a, albl.n, 0);
    borrow = addc_(c + k, cn - k, c + k, cn - k, ahbh.a, ahbh.n, 0);
    if (sign > 0)
        addc_(c + k, cn - k, c + k, cn - k, Q.a, Qn, 0);
    else
        subb_(c + k, cn - k, c + k, cn - k, Q.a, Qn, 0);
}

// c += a*b
// cn >= an + bn.
// static
void
alf::integer::builder::muladd_(digit * c, size_t cn,
                                const digit * a, size_t an,
                                const digit * b, size_t bn)
{
    size_t k;

    if ((an = normalize_(a, an)) == 0) return;
    if ((bn = normalize_(b, bn)) == 0) return;
    if (an == 1) {
        muladd_(c, cn, b, bn, *a);
        return;
    }
    if (bn == 1) {
        muladd_(c, cn, a, an, *b);
        return;
    }
    if (an > bn) {
        const digit * tmp_ = a; a = b; b = tmp_;
        k = an; an = bn; bn = k;
    }
    k = a == b ? KARATSUBA_SQURE_CUTOFF : KARATSUBA_MUL_CUTOFF;
    if (an <= k)
        muladd_short_(c, cn, a, an, b, bn);
    else if ((an <<1) <= bn)
        muladd_seq_(c, cn, b, bn, a, an);
    else
        muladd_karatsuba_(c, cn, a, an, b, bn);
}

// c = a * b
// cn >= an + bn
// we assume an > bn/2 and bn > 70.
// static
void
alf::integer::builder::mul_karatsuba_(digit * c, size_t cn,
                                        const digit * a, size_t an,
                                        const digit * b, size_t bn)
{
    builder d;
    d.length_at_least(cn);
    muladd_karatsuba_(d.a, cn, a, an, b, bn);
    memcpy(c, d.a, cn*sizeof(digit));
}

// c = a*b, cn >= an+bn.
// static
void
alf::integer::builder::mul_(digit * c, size_t cn,
                            const digit * a, size_t an,
                            const digit * b, size_t bn)
{
    builder d;
    d.length_at_least(cn);
    muladd_(d.a, cn, a, an, b, bn);
    memcpy(c, d.a, cn*sizeof(digit));
}

// c = a*b cn >= an+bn, bn is assumed to be much smaller than an
// so that the number a is considered to be a sequence of
// (an + bn - 1)/bn digits - each being bn in length.
// static
void
alf::integer::builder::mul_seq_(digit * c, size_t cn,
                                const digit * a, size_t an,
                                const digit * b, size_t bn)
{
    builder d;
    d.length_at_least(cn);
    muladd_seq_(d.a, cn, a, an, b, bn);
    memcpy(c, d.a, cn*sizeof(digit));
}

// c = a * b
// cn >= an + bn.
// Either an or bn are assumed to be less than a certain cutoff
// value.
// static
void
alf::integer::builder::mul_short_(digit * c, size_t cn,
                                    const digit * a, size_t an,
                                    const digit * b, size_t bn)
{
    builder d;
    d.length_at_least(cn);
    muladd_short_(d.a, cn, a, an, b, bn);
    memcpy(c, d.a, cn*sizeof(digit));
}

// c = a / b, return a % b.
// cn >= an
// static
alf::integer::digit
alf::integer::builder::div_(digit * c, size_t cn,
                            const digit * a, size_t an, digit b)
{
    dbl_digit w = 0;
    dbl_digit bb = b;
    ssize_t k = an;
    if (cn > an) memset(c + an, 0, (cn-an)*sizeof(digit));
    while (--k >= 0) {
        w = (w << BITS) | a[k];
        c[k] = digit(w / bb);
        w %= bb;
    }
    return digit(w);
}

// q = a / b, r = a % b.
// return -1 if b == 0.
// return -2 if any sizes are too short to hold results.
// Either q or r may be 0 meaning we're not interested in
// that result.
// Both q and r may equal either a or b but not same.
// if q == r the modulus is discarded.
// static
int
alf::integer::builder::div_(digit * q, size_t * qnptr,
                            digit * r, size_t * rnptr,
                            const digit * a, size_t an,
                            const digit * b, size_t bn)
{
    enum { HIGH_MASK = 1 << (BITS-1) };

    dbl_digit w, uj, ujj;
    ssize_t j;

    // we assume rn >= bn if r != 0.
    an = normalize_(a, an);
    bn = normalize_(b, bn);
    if (bn == 0) return -1;
    if (q == 0 && r == 0) return -2; // at least one must be non-zero.
    size_t qn = 0, rn = 0;
    ssize_t q_minsize = an - bn;
    if (q_minsize < 0) q_minsize = 0;
    ++q_minsize;

    if (q != 0) {
        if (qnptr == 0) return -2;
        if ((qn = *qnptr) < q_minsize) return -2;
    }
    if (r != 0) {
        if (rnptr == 0) return -2;
        if ((rn = *rnptr) < bn) return -2;
    }
    an = normalize_(a, an);
    if (an < bn) {
        // a < b, q == 0 and r == a.
        if (r) {
            memcpy(r, a, an);
            *rnptr = an;
        }
        if (q)
            *qnptr = 0;
        return 0;
    }
    if (bn == 1) {
        uj = 0;
        ujj = *b;
        if (q) {
            // we assume qn >= an.
            for (j = an; j > 0;) {
                uj = (uj << BITS) | a[--j];
                if ((q[j] = digit(uj / ujj)) == 0 && j+1 == an)
                    an = j;
                uj %= ujj;
            }
            *qnptr = normalize_(q, qn);
        } else {
            // don't bother to save q.
            for (j = an; j > 0;) {
                uj = (uj << BITS) | a[--j];
                if (digit(uj/ ujj) == 0 && j+1 == an)
                    an = j;
                uj %= ujj;
            }
        }
        if (r) {
            if (uj) {
                *r = digit(uj);
                *rnptr = 1;
            } else {
                *rnptr = 0;
            }
        }
        return an;
    }

    // an >= bn.
    size_t un = an + 1;
    size_t vn = bn;
    size_t qxn = un - bn;
    if (q && qn < qxn) return -2; // not enough room for q.
    digit * u = new digit[un + vn];
    digit * v = u + un;
    memcpy(u, a, an*sizeof(digit));
    u[an] = 0;
    memcpy(v, b, vn*sizeof(digit));
    size_t shift = 0;
    // shift v so that the most significant bit of v is one.
    // we know the leading number is non-zero so it has at least
    // one bit equal to 1.
    digit vmost = v[vn-1], v1 = vmost, v2;
    // find out how much to shift vmost to get high bit equal to one.
    if ((vmost & 0x3fffc000) == 0)
        vmost <<= (shift = 14);
    if ((vmost & 0x3fc00000) == 0) { vmost <<= 8; shift += 8; }
    if ((vmost & 0x3c000000) == 0) { vmost <<= 4; shift += 4; }
    if ((vmost & 0x30000000) == 0) { vmost <<= 2; shift += 2; }
    if ((vmost & 0x20000000) == 0) { vmost <<= 1; ++shift; }
    j = vn - 1;
    size_t rshift = 30 - shift;
    while (j > 0) {
        v2 = v[--j];
        v[j+1] = ((v1 << shift) | (v2 >> rshift)) & BITS_MASK;
        v1 = v2;
    }
    *v = (v1 << shift) & BITS_MASK;
    vmost = v[vn-1];

    // also shift u by the same amount.
    v1 = u[j = an];
    while (j > 0) {
        v2 = u[--j];
        u[j+1] = ((v1 << shift) | (v2 >> rshift)) & BITS_MASK;
        v1 = v2;
    }
    *u = (v1 << shift) & BITS_MASK;
    // now u and v are both shifted by shift so that
    // (HIGH_BIT & v[vn-1]) != 0.
    // the top two digits of u.
    digit umost = u[an];
    uj = umost;
    uj = (uj << BITS) | u[un-2];
    // to translate to Knuth:
    // bn  is n in knuth.
    // an-bn is m in knuth so an == n + m.
    // u is n + m + 1 == un long.
    // v is n = bn long.
    // q is either 0 or at least m + 1 long.
    // r is either 0 or at least bn long.
    // at least one of q and r is non-zero.

    // We will essentially divide u[j+vn:j] by v[vn-1..0]
    // to get a single digit q[j].
    dbl_digit vmostd = vmost;
    dbl_digit vnextd = v[vn-2];

    for (j = un - vn; --j >= 0;) {
        // guess qhat 
        // if u[j + vn] == v[vn-1] set qhat = 0x3fffffff;
        // else qhat = u[an-j]:u[an-1-j]/v[vn-1];
        ujj = (dbl_digit(u[j+bn]) << BITS) | u[j+bn-1];
        digit qhat = u[j+bn] == vmost ? BITS_MASK : digit(ujj/vmostd);

        // now test if v2*qhat > (uuj - qhat*vmost)*R + u[j + bn-2]
        dbl_digit qhatd = qhat;
        dbl_digit qhatvmost = qhatd*vmostd;
        dbl_digit qhatvnext = qhatd*vnextd;
        dbl_digit zz0 = ((ujj - qhatvmost) << BITS) | u[j+bn-2];
        if (qhatvnext > zz0) {
            --qhat; // then repeat the test.
            qhatd = qhat;
            qhatvmost = qhatd * vmostd;
            qhatvnext = qhatd * vnextd;
            zz0 = ((ujj - qhatvmost) << BITS) | u[j+bn-2];
            if (qhatvnext > zz0)
                qhatd = --qhat;
        }
        // multiply and subtract.
        // u[j+vn..j] -= qhat * v[vn-1..0]
        // starting with least significant digit first (u[j]), v[0]
        dbl_digit w = qhatd;
        zz0 = (qhatd << BITS) - qhatd;
        size_t k;
        if (qhat) {
            for (k = 0; k < vn; ++k) {
                w = zz0 + w + u[k+j] - qhatd * v[k];
                u[k+j] = digit(w & BITS_MASK);
                w >>= BITS;
            }
            // one extra round where v is zero.
            w = zz0 + w + u[j+vn];
            u[j+vn] = digit(w & BITS_MASK);
            w >>= BITS;
        }
        // if everything is right, w should equal qhatd now.
        if (w != qhatd) {
            // need to add back.
            qhatd = --qhat;
            zz0 = 0;
            for (k = 0; k < vn; ++k) {
                zz0 = zz0 + u[k+j] + v[k];
                u[k+j] = digit(zz0 & BITS_MASK);
                zz0 >>= BITS;
            }
            zz0 = zz0 + u[k+j];
            u[k+j] = digit(zz0 & BITS_MASK);
            zz0 >>= BITS;
            w += zz0;
            if (w != qhatd + 1) {
                printf( "j = %zd, w = %lld, qhatd = %lld\n", j, w, qhatd);
            }
        }
        // q[j] = qhat.
        if (q) q[j] = qhat;
    }
    if (q) *qnptr = qn = normalize_(q, qn);
    if (r) {
        // u is remainder but it is shifted by shift.
        // we need to shift it back.
        v1 = u[0];
        for (j = 0; ++j < bn;) {
            v2 = u[j];
            r[j-1] = ((v2 << rshift) & BITS_MASK) | (v1 >> shift);
            v1 = v2;
        }
        r[bn-1] = v1 >> shift;
        *rnptr = rn = normalize_(r, bn);
    }
    return un - bn;
}
