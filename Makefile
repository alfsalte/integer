
CXX := g++

CXXFLAGS := -g -std=c++11

SOURCES := integer.cxx test-integer.cxx

PROGS := test-integer test-decimal

OBJS := $(patsubst %.cxx,%.o,$(SOURCES))

%.o: %.cxx
	$(CXX) -c $(CXXFLAGS) -o $@ $<

all: $(PROGS)

test-integer: $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

test-decimal: test-decimal.o decimal.o integer.o
	$(CXX) $(CXXFLAGS) -o $@ $^

test-integer.o: test-integer.cxx integer.hxx

test-decimal.o: test-decimal.cxx decimal.hxx integer.hxx

integer.o: integer.cxx integer.hxx

decimal.o: decimal.cxx decimal.hxx integer.hxx
