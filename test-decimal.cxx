
#include <bits/allocator.h>

#include <ctype.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdlib.h>

#include "integer.hxx"
#include "decimal.hxx"

namespace {

class Worker {
public:
    typedef unsigned int digit;
    typedef unsigned long long int dbl_digit;
    typedef alf::decimal::builder dbuilder;
    typedef alf::integer::builder ibuilder;
    typedef alf::integer integer;
    typedef alf::decimal decimal;

    class Arr {
    public:
        Arr() : base(vec), end(vec + sizeof(vec))
        { *(ptr = --end) = 0; }

        ~Arr() { if (base != vec) delete [] base; }

        Arr & clear() { ptr = end; return *this; }
        Arr & ins(char x) { ensure(1); *--ptr = x; return *this; }

        Arr & ins(const char * s, size_t z)
        { ensure(z); ptr -= z; memcpy(ptr, s, z); return *this; }

        Arr & ins(const char * s)
        { if (s) ins(s, strlen(s)); return *this; }
        const char * data() const { return ptr; }
        size_t len() { return end - ptr; }
        const char * endptr() const { return end; }

    private:
        void ensure(size_t k);

        char vec[128];
        char * base;
        char * ptr;
        char * end;
    };

    class Number {
    public:
        virtual ~Number();
        virtual size_t write(Arr & buf, char f = 'f') const = 0;

        static Number * read(const char * str);
        static Number * mk(const decimal & d);
        static Number * mk(float x);
        static Number * mk(double x);
        static Number * mk(long double x);
        static Number * mk(const integer & k);
        static Number * mk(int k);
        static Number * mk(unsigned int k);
        static Number * mk(long long int k);
        static Number * mk(unsigned long long int k);

        virtual Number * to_decimal() const = 0;
        virtual Number * to_f32() const = 0;
        virtual Number * to_f64() const = 0;
        virtual Number * to_ldbl() const = 0;
        virtual Number * to_integer() const = 0;
        virtual Number * to_i32() const = 0;
        virtual Number * to_i64() const = 0;
        virtual Number * to_u32() const = 0;
        virtual Number * to_u64() const = 0;
        virtual Number * sqrt(size_t prec = alf::decimal::prec) const = 0;

    protected:
        Number() { }
    };

    class iNumber : public Number {
    public:
        virtual ~iNumber();
        virtual size_t write(Arr & buf, char f = 'f') const;

        virtual Number * to_decimal() const;
        virtual Number * to_f32() const;
        virtual Number * to_f64() const;
        virtual Number * to_ldbl() const;
        virtual Number * to_integer() const;
        virtual Number * to_i32() const;
        virtual Number * to_i64() const;
        virtual Number * to_u32() const;
        virtual Number * to_u64() const;
        virtual Number * sqrt(size_t prec) const;

    private:
        friend class Number;

        iNumber(integer kk) : k(kk) { }

        integer k;
    };

    class dNumber : public Number {
    public:
        virtual ~dNumber();
        virtual size_t write(Arr & buf, char f = 'f') const;

        virtual Number * to_decimal() const;
        virtual Number * to_f32() const;
        virtual Number * to_f64() const;
        virtual Number * to_ldbl() const;
        virtual Number * to_integer() const;
        virtual Number * to_i32() const;
        virtual Number * to_i64() const;
        virtual Number * to_u32() const;
        virtual Number * to_u64() const;
        virtual Number * sqrt(size_t prec) const;

    private:
        friend class Number;

        dNumber(decimal xx) : x(xx) { }

        decimal x;
    };

    class i32Number : public Number {
    public:
        virtual ~i32Number();
        virtual size_t write(Arr & buf, char f = 'f') const;

        virtual Number * to_decimal() const;
        virtual Number * to_f32() const;
        virtual Number * to_f64() const;
        virtual Number * to_ldbl() const;
        virtual Number * to_integer() const;
        virtual Number * to_i32() const;
        virtual Number * to_i64() const;
        virtual Number * to_u32() const;
        virtual Number * to_u64() const;
        virtual Number * sqrt(size_t prec) const;

    private:
        friend class Number;
        i32Number(int kk) : k(kk) { }

        int k;
    };

    class u32Number : public Number {
    public:
        virtual ~u32Number();
        virtual size_t write(Arr & buf, char f = 'f') const;

        virtual Number * to_decimal() const;
        virtual Number * to_f32() const;
        virtual Number * to_f64() const;
        virtual Number * to_ldbl() const;
        virtual Number * to_integer() const;
        virtual Number * to_i32() const;
        virtual Number * to_i64() const;
        virtual Number * to_u32() const;
        virtual Number * to_u64() const;
        virtual Number * sqrt(size_t prec) const;

    private:
        friend class Number;
        u32Number(unsigned int kk) : k(kk) { }

        unsigned int k;
    };

    class i64Number : public Number {
    public:
        virtual ~i64Number();
        virtual size_t write(Arr & buf, char f = 'f') const;

        virtual Number * to_decimal() const;
        virtual Number * to_f32() const;
        virtual Number * to_f64() const;
        virtual Number * to_ldbl() const;
        virtual Number * to_integer() const;
        virtual Number * to_i32() const;
        virtual Number * to_i64() const;
        virtual Number * to_u32() const;
        virtual Number * to_u64() const;
        virtual Number * sqrt(size_t prec) const;

    private:
        friend class Number;
        i64Number(long long int kk) : k(kk) { }

        long long int k;
    };

    class u64Number : public Number {
    public:
        virtual ~u64Number();
        virtual size_t write(Arr & buf, char f = 'f') const;

        virtual Number * to_decimal() const;
        virtual Number * to_f32() const;
        virtual Number * to_f64() const;
        virtual Number * to_ldbl() const;
        virtual Number * to_integer() const;
        virtual Number * to_i32() const;
        virtual Number * to_i64() const;
        virtual Number * to_u32() const;
        virtual Number * to_u64() const;
        virtual Number * sqrt(size_t prec) const;

    private:
        friend class Number;
        u64Number(unsigned long long int kk) : k(kk) { }

        unsigned long long int k;
    };

    class f32Number : public Number {
    public:
        virtual ~f32Number();
        virtual size_t write(Arr & buf, char f = 'f') const;

        virtual Number * to_decimal() const;
        virtual Number * to_f32() const;
        virtual Number * to_f64() const;
        virtual Number * to_ldbl() const;
        virtual Number * to_integer() const;
        virtual Number * to_i32() const;
        virtual Number * to_i64() const;
        virtual Number * to_u32() const;
        virtual Number * to_u64() const;
        virtual Number * sqrt(size_t prec) const;

    private:
        friend class Number;

        f32Number(float xx) : x(xx) { }

        float x;
    };

    class f64Number : public Number {
    public:
        virtual ~f64Number();
        virtual size_t write(Arr & buf, char f = 'f') const;

        virtual Number * to_decimal() const;
        virtual Number * to_f32() const;
        virtual Number * to_f64() const;
        virtual Number * to_ldbl() const;
        virtual Number * to_integer() const;
        virtual Number * to_i32() const;
        virtual Number * to_i64() const;
        virtual Number * to_u32() const;
        virtual Number * to_u64() const;
        virtual Number * sqrt(size_t prec) const;

    private:
        friend class Number;
        f64Number(double xx) : x(xx) { }

        double x;
    };

    class ldblNumber : public Number {
    public:
        virtual ~ldblNumber();
        virtual size_t write(Arr & buf, char f = 'f') const;

        virtual Number * to_decimal() const;
        virtual Number * to_f32() const;
        virtual Number * to_f64() const;
        virtual Number * to_ldbl() const;
        virtual Number * to_integer() const;
        virtual Number * to_i32() const;
        virtual Number * to_i64() const;
        virtual Number * to_u32() const;
        virtual Number * to_u64() const;
        virtual Number * sqrt(size_t prec) const;

    private:
        friend class Number;
        ldblNumber(long double xx) : x(xx) { }

        long double x;
    };

    int Main(int argc, char * argv[]);

    Number * read_num(const char * s) { return Number::read(s); }

    size_t write_num(Arr & buf, const Number * n, char f='f')
    { return n->write(buf, f); }

    static integer read_inum(const char * s);
    static decimal read_dnum(const char * s);
    static size_t write_inum(Arr & buf, const integer & a);
    static size_t write_numz(Arr & buf, const decimal & d, char f='f');
    static size_t write_numf(Arr & buf, const decimal & d, char f='f');
    static size_t write_nume(Arr & buf, const decimal & d, char f='e');
    static size_t write_nums(Arr & buf, const decimal & d, char f='s');
    static size_t write_numg(Arr & buf, const decimal & d, char f='g');
    static size_t write_dnum(Arr & buf, const decimal & d, char f='g');
};

};

int main(int argc, char * argv[])
{
    return Worker().Main(argc, argv);
}

// =====================
// Worker

int Worker::Main(int argc, char * argv[])
{
    enum {
        ABS, ADD, CMP, DECIMAL, DIV, F32, F64,
        I32, I64, INTEG,
        LONG_DBL, MUL, NEG, SUB, SQRT, U32, U64,
    };
    struct ent { const char * str; size_t mlen; int args, op; };
    static const ent optbl[] = {
        {"abs", 2, 1, ABS}, {"add", 1, 2, ADD},
        {"cmp", 1, 2, CMP}, {"decimal", 1, 1, DECIMAL},
        {"div", 2, 2, DIV}, {"f32", 2, 1, F32}, {"f64", 1, 1, F64},
        {"i32", 2, 1, I32}, {"i64", 2, 1, I64}, {"integer", 1, 1, INTEG},
        {"ldbl", 1, 1, LONG_DBL},
        {"mul", 1, 2, MUL}, {"neg", 2, 1, NEG},
        {"sqrt", 2, 1, SQRT}, {"sub", 1, 2, SUB},
        {"u32", 2, 1, U32}, {"u64", 1, 1, U64},
        { 0, 0, 0},
    };

    if (argc < 3) {
        fprintf(stderr, "Missing args\n");
        return 1;
    }

    Arr buf;
    dbuilder bb;
    Number * a = 0, * b = 0, * c = 0, * d = 0;
    const char * str = "";
    digit dig = 0;
    int i;
    char ch = 0;
    bool showc = false, showd = false, useb = true;

    char f1 = argv[1][0], f2 = 0;
    if (f1 && (f2 = argv[1][1]) == 0) f2 = f1;

    const char * opstr = argv[2];
    size_t oplen = strlen(opstr);
    if (oplen == 0) {
        fprintf(stderr, "Missing operation.\n");
        return 1;
    }
    const ent * opq = optbl;
    int op = -1;
    while (opq->str!=0) {
        if (oplen >= opq->mlen && (op = strncmp(opstr, opq->str, oplen)) <= 0)
            break;
        ++opq;
    }
    if (op) {
        fprintf(stderr, "Unknown operation '%s'\n", opstr);
        return 1;
    }
    op = opq->op;
    printf( "Operation is: %s (%d: args: %d)\n", opq->str, op, opq->args);
    if (argc < opq->args + 3) {
        fprintf(stderr, "Missing args\n");
        return 1;
    }

    const char * a_str = argv[3];
    const char * b_str = 0;
    a = read_num(a_str);
    if (opq->args >= 2) b = read_num(b_str = argv[4]);
    //if (opq->args >= 3) c = read_num(argv[4]);
    buf.clear();
    a->write(buf, f1);
    printf( "a (f:%c) = %s\n", f1, buf.data());
    if (b) {
        buf.clear();
        b->write(buf, f2);
        printf( "b (f:%c) = %s\n", f2, buf.data());
    }

    switch (op) {
#if 0
    case ABS: c = a.abs(); break;
    case ADD:
        c = a + b;
        break;
#endif
#if 0
    case CMP:
        j = a.cmp(b);
        if (j > 0) printf("first value is larger\n");
        else if (j == -1) printf("second value is larger\n");
        else if (j == -2) printf("The two values are not comparable\n");
        else printf("the two values are equal\n");
        break;
#endif
    case DECIMAL:
        c = a->to_decimal();
        break;
#if 0
    case DIV:
        c = a / b;
        break;
#endif
    case F32:
        c = a->to_f32();
        break;
    case F64:
        c = a->to_f64();
        break;
    case I32:
        c = a->to_i32();
        break;
    case I64:
        c = a->to_i64();
        break;
    case INTEG:
        c = a->to_integer();
        break;
    case LONG_DBL:
        c = a->to_ldbl();
        break;
#if 0
    case MUL:
        c = a * b;
        break;
    case NEG:
        c = -a;
        break;
    case SUB:
        c = a - b;
        break;
#endif
    case SQRT:
        c = a->sqrt();
        break;
    case U32:
        c = a->to_u32();
        break;
    case U64:
        c = a->to_u64();
        break;
    }

    if (c) {
        buf.clear();
        c->write(buf, f2);
        printf( "c (f:%c) = %s\n", f2, buf.data());
        delete c;
    }
    if (d) {
        buf.clear();
        d->write(buf, f2);
        printf("d (f:%c) = %s\n", f2, buf.data());
        delete d;
    }
    if (a) delete a;
    if (b) delete b;
    return 0;
}

// static
alf::integer
Worker::read_inum(const char * s)
{
    ibuilder b;

    int ch = *s++;
    int neg = 0;
    while (ch != 0 && isspace(ch)) ch = *s++;
    switch (ch) {
    case '-': neg = 1; /* FALLTHRU */
    case '+':
        if ((ch = *s++) != 0) break;
        /* FALLTHRU */
    case 0: return integer(0);
    }

    while (isdigit(ch)) {
        digit d = ch - '0';
        b.mul_(10, d);
        ch = *s++;
    }
    if (neg)
        b.neg();
    return integer(b);
}

// static
alf::decimal
Worker::read_dnum(const char * s)
{
    // given a state, what index in accept, action and nxt
    // do we start looking, also the following index
    // gives the index where to stop.
    // given state index, get first action index
    // for this state, state+1 give first action of next
    // state and thus the index where to stop.
    static const int st0[] = {
        // 1  2  3  4  5  6   7   8
        0, 4, 5, 6, 7, 8, 9, 10, 11,
        //  10  11
        11, 12, 13,
        //  13
        13, 14,
        // 14
        15
    };

    // given action index find the next accepted char.
    // we start from st0[state] and look for index that
    // matches next char until st0[state+1]. The resulting
    // index is action index.
    static const char accept[] = {
        // 0    1   2   3   4   5   6   7   8
        // 0..3 4   5   6   7   8   9   10 11
        "inqs" "n" "f" "i" "n" "i" "t" "y" ""
        // 9  10 11
        // 11 12 13
        "a" "n"  ""
        // 12 13
        // 13 14
        "n" "n"
        // 14
        // 15
    };

    // given action index, find action.
    // 0 - advance to next state, otherwise do nothing.
    // 1 - accept result is inf - do post processing.
    // 2 - accept result is nan - do post processing.
    // q - process q
    // s - process s.
    static const char action[] =
        // state 0  2   3   4   5   6   7  8
        //      4   5   6   7   8   9  10 11
        // inqs n   f   i   n   i   t   y
        "0000" "0" "0" "0" "0" "0" "0" "0" ""
        // state 9
        // 9  10 11
        //11  12 13
        //a   n 
        "0"  "0" ""
        // state 12
        // 13  14
        // n  n
        "q"  "s"
        // "state" 14
        // 15
        "";

    // given an action index get the next state.
    // states 0..11 incomplete states.
    // state 100 incomplete but no nan or inf.
    // state 101 error.
    // state 102 inf - complete.
    // state 103 nan - complete.
    static const int nxt[] = { 1, 9, 12, 13,
        // 5  6  7  8  9  10 11
        2, 3, 4, 5, 6, 7, 8,
        // state 9
        // 11 12 13
        10, 11,
        // state 12
        // 13  14
        9, 9
    };

    // 0 is incomplete, 1 is failed, 100 and above is
    // accepting states and the value indicate what state.
    // Given state return if we can accept this state and
    // what value we get.
    static const int accept_states[] = { 100, 1, 1,
        // 3:inf         8:inf      11:nan      12
        102, 1, 1, 1, 1, 102, 1, 1, 103, 1, 1, 101 };

    dbuilder b;

    int ch = *s++;
    int neg = 0;
    int x = 0;
    int state = 0;
    int qsnan = 0;
    int dot = 0, need_dig = 1;
    const char * xp = 0;

    while (ch != 0 && isspace(ch)) ch = *s++;
    switch (ch) {
    case '-':
    case '+':
        neg = ch;
        if ((ch = *s++) != 0) break;
        /* FALLTHRU */
    case 0: return decimal(0);
    }

    while (state < 100) {
        int start = st0[state];
        int stop = st0[state+1];
        if (isupper(ch)) ch = tolower(ch);
        while (start < stop && accept[start] != ch) ++start;
        if (start == stop) {
            int ast = accept_states[state];
            state = ast < 100 ? 101 : ast;
            continue;
        }
        // start is action index.
        // get next state.
        state = nxt[start];
        ch = *s++;
        switch (action[start]) {
        case '0':
        case '1':
        case '2': continue;
        case 'q': qsnan = 'q'; continue;
        case 's': qsnan = 's'; continue;
        default: state = 101; continue; // oops.
        }
    }
    if (state >= 102 && isalpha(ch)) state = 101;
    switch (state) {
    case 101:
        // error.
        return decimal(0);
    case 102: // inf, heed sign.
        if (neg == '-') b.setninf();
        else b.setpinf();
        return decimal(b);
    case 103:
        // sign before nan is ignored but we heed qsnan.
        // if just nan, we treat it as a quiet nan.
        if (qsnan == 's') b.setsnan();
        else b.setqnan();
        return decimal(b);
    }
    // no need to remember '+'' any more.
    neg = neg == '-';

    while (isdigit(ch)) {
        digit d = ch - '0';
        b.mul_(10, d);
        if (dot) --x;
        need_dig = 0;
        if ((ch = *s++) == '.') {
            if (dot) {
                fprintf(stderr, "two dots in number\n");
                throw "two dots in number";
            }
            ch = *s++;
            need_dig = dot = 1;
        } else if (ch == '\'') {
            ch = *s++;
            need_dig = 1;
        }
    }
    if (need_dig) {
        fprintf(stderr, "Digit required near %s\n", s-3);
        throw "Invalid number syntax";
    }
    b.add_expon(x);
    x = 0;
    if (ch == 'e' || ch == 'E') {
        int xneg = 0;
        xp = s - 1;
        switch (ch = *s++) {
        case '-': xneg = 1; /* FALLTHRU */
        case '+':
            if ((ch = *s++) != 0) break;
            /* FALLTHRU */
        case 0:
            fprintf(stderr, "Expected digits near %s\n", xp-1);
            throw "Invalid number syntax";
        }
        if (! isdigit(ch)) {
            fprintf(stderr, "Expected digits near %s\n", xp-1);
            throw "Invalid number syntax";
        }
        do {
            x = x * 10 + (ch - '0');
            if (x < 0) {
                fprintf(stderr, "Exponent too large for %s\n", xp);
                throw "Invalid number syntax";
            }
            ch = *s++;
        } while (isdigit(ch));
        if (xneg) x = -x;
        b.add_expon(x);
        x = 0;
    }
    b.mkclass();
    if (neg)
        b.neg();
    return decimal(b);
}

// static
size_t
Worker::write_inum(Arr & buf, const integer & a)
{
    ibuilder b(a);
    b.normalize();
    size_t bn = b.length();
    size_t w = 0;
    int gg = 4;
    do {
        digit z = b.div_(10);
        if (--gg == 0) {
            buf.ins(',');
            gg = 3;
            ++w;
        }
        buf.ins(z + '0');
        ++w;
    } while (bn > 0 && (b.data()[bn-1] != 0 || --bn > 0));
    if (a.s_len() < 0) {
        buf.ins('-');
        ++w;
    }
    return w;
}

// static
size_t
Worker::write_numz(Arr & buf, const decimal & a, char f)
{
    static const char * tbl[] = { "snan", "qnan",
    "+inf", "-inf", "+0", "-0"};
    static const char * TBL[] = { "SNAN", "QNAN",
    "+INF", "-INF", "+0", "-0"};

    int x = -1;
    size_t ret = 0;
    const char ** Tbl = f < 'a' ? TBL : tbl;

    switch (a.klass()) {
    case alf::decimal::SNAN: x = 0; ret = 4; break;
    case alf::decimal::QNAN: x = 1; ret = 4; break;
    case alf::decimal::PINF: x = 2; ret = 4; break;
    case alf::decimal::NINF: x = 3; ret = 4; break;
    case alf::decimal::PZERO: x = 4; ret = 2; break;
    case alf::decimal::NEGZERO: x = 5; ret = 2; break;
    }
    if (ret)
        buf.ins(Tbl[x], ret);
    return ret;
}

// static
size_t
Worker::write_numf(Arr & buf, const decimal & a, char f)
{
    dbuilder b(a);
    int neg = 0;

    switch (b.kls()) {
    case alf::decimal::SNAN:
    case alf::decimal::QNAN:
    case alf::decimal::PINF:
    case alf::decimal::NINF:
    case alf::decimal::PZERO:
    case alf::decimal::NEGZERO:
        return write_numz(buf, a, f);
    case alf::decimal::NREGULAR:
        neg = 1;
        break;
    }
    b.normalize();
    size_t bn = b.length();
    size_t w = 0;
    if (bn == 0) {
        buf.ins('0');
        buf.ins(neg ? '-' : '+');
        return 2;
    }
    // gg must be initialized so that the decimal dot
    // fall exactly on group separator.
    // if ax is negative and -ax % 3 == u
    // then gg should be initalized to 2, 3 or 4 depending
    // on u being 1 2 or 0.
    // if ax >= 0 then gg should be 4.
    ssize_t ax = a.expon();
    int v;
    int gg = ax >= 0 ? 4 : (v = -ax % 3) != 0 ? v + 1 : 4;
    ssize_t j = ax;
    if (j > 0) {
        // no decimal dot but we add a zero for each value
        // in ax inserting ',' at proper places.
        do {
            if (--gg == 0) {
                buf.ins(',');
                gg = 3;
                ++w;
            }
            buf.ins('0');
            ++w;
        } while (--j > 0);
    }
    do {
        digit z = b.div_(10);
        if (--gg == 0) {
            buf.ins(',');
            gg = 3;
            ++w;
        }
        buf.ins(z + '0');
        ++w;
        if (++j == 0) {
            buf.ins('.');
            gg = 4;
            ++w;
        }
    } while (bn > 0 && (b.data()[bn-1] != 0 || --bn > 0));
    if (j <= 0) {
        if (j < 0) {
            do {
                if (--gg == 0) {
                    buf.ins(',');
                    gg = 3;
                    ++w;
                }
                buf.ins('0');
                ++w;
            } while (++j < 0);
            buf.ins('.');
            ++w;
        }
        buf.ins('0');
        ++w;
    }
    if (neg) {
        buf.ins('-');
        ++w;
    }
    return w;
}

// static
size_t
Worker::write_nume(Arr & buf, const decimal & a, char f)
{
    // output number in the form d.dddddd E+/-dddd
    Arr Q;
    dbuilder b(a);
    int neg = 0;

    switch (b.kls()) {
    case alf::decimal::SNAN:
    case alf::decimal::QNAN:
    case alf::decimal::PINF:
    case alf::decimal::NINF:
    case alf::decimal::PZERO:
    case alf::decimal::NEGZERO:
        return write_numz(buf, a, f);
    case alf::decimal::NREGULAR:
        neg = 1;
        break;
    }
    b.normalize();
    size_t bn = b.length();
    if (bn == 0) {
        buf.ins('0');
        buf.ins(neg ? '-' : '+');
        return 2;
    }
    // we first write out the digits of a
    // in Q and wait with the group chars until we
    // insert into buf.
    size_t ndigs = 0;
    while (bn > 0){
        if (b.data()[bn-1] == 0) --bn;
        if (bn == 0) break;

        digit z = b.div_(10);
        Q.ins(z + '0');
        ++ndigs;
    }

    if (ndigs == 0) {
        buf.ins('0');
        buf.ins(neg ? '-' : '+');
        return 2;
    }
    // first digit is before dot, then comes dot
    // and the remaining digits in groups of 3.
    size_t fdigs = ndigs - 1;
    size_t pdigs = fdigs % 3;
    size_t w = 0;

    int gg = pdigs ? pdigs + 1 : 4;
    const char * Qp = Q.data();
    const char * Qd = Qp + 1; // fractional part.
    const char * QQ = Q.endptr();

    // exponent is simple enough except that
    // the b.x value is with the number as a whole number
    // while we regard it to be d.dddddd
    // if ndigs == 4 for example and ax == -2 the number
    // is really dd.dd and we display it as d.dddE+1
    // so adding fdigs to x should solve that issue.
    ssize_t ax = b.expon() + fdigs;
    // we first need to print exponent.
    int xneg = 0;
    if (ax < 0) {
        xneg = 1;
        ax = -ax;
    }
    size_t axx = ax;
    do {
        buf.ins((axx % 10) + '0');
        ++w;
    } while ((axx /= 10) != 0);
    buf.ins(xneg ? '-' : '+');
    buf.ins(f ? f : 'e');
    w += 2;
    while (QQ > Qd) {
        if (--gg == 0) {
            buf.ins(',');
            gg = 3;
            ++w;
        }
        buf.ins(*--QQ);
        ++w;
    }
    buf.ins('.');
    ++w;
    buf.ins(*--QQ);
    ++w;
    if (neg) {
        buf.ins('-');
        ++w;
    }
    return w;
}

// static
size_t
Worker::write_nums(Arr & buf, const decimal & a, char f)
{
    // output number in the form (d)(d)d.dddddd E+/-dddd
    // I.e. the number is written with digits in group
    // of 3 so that inteer part is in the range 1..999
    // and exponent is divisble by 3.
    Arr Q;
    dbuilder b(a);
    int neg = 0;

    switch (b.kls()) {
    case alf::decimal::SNAN:
    case alf::decimal::QNAN:
    case alf::decimal::PINF:
    case alf::decimal::NINF:
    case alf::decimal::PZERO:
    case alf::decimal::NEGZERO:
        return write_numz(buf, a, f);
    case alf::decimal::NREGULAR:
        neg = 1;
        break;
    }
    b.normalize();
    size_t bn = b.length();
    if (bn == 0) {
        buf.ins('0');
        buf.ins(neg ? '-' : '+');
        return 2;
    }
    // we first write out the digits of a
    // in Q and wait with the group chars until we
    // insert into buf.
    size_t ndigs = 0;
    while (bn > 0){
        if (b.data()[bn-1] == 0) --bn;
        if (bn == 0) break;

        digit z = b.div_(10);
        Q.ins(z + '0');
        ++ndigs;
    }

    if (ndigs == 0) {
        buf.ins('0');
        buf.ins(neg ? '-' : '+');
        return 2;
    }
    // first digit is before dot, then comes dot
    // and the remaining digits in groups of 3.
    ssize_t fdigs = ndigs - 1;
    size_t idigs = 1;
    size_t zdigs = 0;
    size_t w = 0;

    // exponent is simple enough except that
    // the b.x value is with the number as a whole number
    // while we regard it to be d.dddddd
    // if ndigs == 4 for example and ax == -2 the number
    // is really dd.dd and we display it as d.dddE+1
    // so adding fdigs to x should solve that issue.
    ssize_t ax = b.expon() + fdigs;
    // we require ax to be divisible by 3 if
    // ax % 3 is j then we add those to integer part.
    ssize_t pdigs = ax % 3;
    if (pdigs) {
        if (pdigs < 0) pdigs += 3;
        idigs += pdigs;
        fdigs -= pdigs;
        ax -= pdigs;
        if (fdigs < 0) {
            zdigs = -fdigs;
            fdigs = 0;
        }
    }
    pdigs = fdigs % 3;
    int gg = pdigs ? pdigs + 1 : 4;
    const char * Qp = Q.data();
    const char * Qd = Qp + idigs; // fractional part.
    const char * QQ = Q.endptr();

    // we first need to print exponent.
    int xneg = 0;
    if (ax < 0) {
        xneg = 1;
        ax = -ax;
    }
    size_t axx = ax;
    do {
        buf.ins((axx % 10) + '0');
        ++w;
    } while ((axx /= 10) != 0);
    buf.ins(xneg ? '-' : '+');
    buf.ins(f < 'a' ? 'E' : 'e');
    w += 2;
    // then we add zdigs - if zdigs > 0 we have
    // no fractional part.
    if (zdigs) {
        do {
            buf.ins('0');
            ++w;
        } while (--zdigs);
    } else if (QQ > Qd) {
        do {
            if (--gg == 0) {
                buf.ins(',');
                gg = 3;
                ++w;
            }
            buf.ins(*--QQ);
            ++w;
        } while(QQ > Qd);
        buf.ins('.');
        ++w;
    }
    while (QQ > Qp) {
        buf.ins(*--QQ);
        ++w;
    }
    if (neg) {
        buf.ins('-');
        ++w;
    }
    return w;
}

// static
size_t
Worker::write_numg(Arr & buf, const decimal & a, char f)
{
    // output number in the form d.dddddd E+/-dddd
    Arr A, B;

    size_t u = write_numf(A, a, f);
    size_t v = write_nume(B, a, f);
    if (u <= v) {
        buf.ins(A.data(), u);
        return u;
    }
    buf.ins(B.data(), v);
    return v;
}

// static
size_t
Worker::write_dnum(Arr & buf, const decimal & a, char f)
{
    switch (f) {
    case 'f': case 'F': return write_numf(buf, a, f);
    case 'e': case 'E': return write_nume(buf, a, f);
    case 's': case 'S': return write_nums(buf, a, f - 'S' + 'E');
    }
    return write_numg(buf, a, f < 'a' ? 'E' : 'e');
}

// ===========================
// Worker::Arr

void
Worker::Arr::ensure(size_t k)
{
    enum { SMALL = 4096, LARGE = 65536, };

    if (ptr - k < base) {
        size_t n = end + 1 - ptr;
        size_t m = end + 1 - base;
        size_t u = n + k;
        if (m < SMALL) m = SMALL;
        while (m < LARGE && m < u) m <<= 1;
        while (m < u) m += LARGE;
        char * p = new char[m];
        size_t pos = m - n;
        memset(p, 0, pos);
        memcpy(p + pos, ptr, n);
        if (ptr != vec) delete [] ptr;
        base = p;
        ptr = p + pos;
        end = p + m - 1;
    }
}

// ===========================
// Worker::Number

// virtual
Worker::Number::~Number()
{ }

// static
Worker::Number *
Worker::Number::read(const char * str)
{
    long double ldbl;
    double f64;
    unsigned long long int u64;
    long long int i64;
    integer i;
    decimal d;
    int i32;
    unsigned int u32;
    float f32;

    int ch = *str++;

    while (isspace(ch)) ch = *str++;
    const char * ss = str - 1;
    switch (ch) {
    case 'i':
        switch (ch = *str++) {
        case '3':
            if ((ch = *str++) == '2' && (ch = *str++) == ':') {
                i32 = (int)strtol(str, 0, 0);
                return new i32Number(i32);
            }
            break;
        case '6':
            if ((ch = *str++) == '4' && (ch = *str++) == ':') {
                i64 = strtoll(str, 0, 0);
                return new i64Number(i64);
            }
            break;
        case ':':
            i = read_inum(str);
            return new iNumber(i);
        }
        break;
    case 'f':
        switch (ch = *str++) {
        case '3':
            if ((ch = *str++) == '2' && (ch = *str++) == ':') {
                f32 = strtof(str, 0);
                return new f32Number(f32);
            }
            break;
        case '6':
            if ((ch = *str++) == '4' && (ch = *str++) == ':') {
                f64 = strtod(str, 0);
                return new f64Number(f64);
            }
            break;
        case ':':
            ss = str;
            break;
        }
        break;
    case 'l':
        if ((ch = *str++) == ':') {
            ldbl = strtold(str, 0);
            return new ldblNumber(ldbl);
        }
        break;
    case 'd':
        if ((ch = *str++) == ':')
            ss = str;
        break;
    case 'u':
        switch (ch = *str++) {
        case '3':
            if ((ch = *str++) == '2' && (ch = *str++) == ':') {
                u32 = (unsigned int)strtoul(str, 0, 0);
                return new u32Number(u32);
            }
            break;
        case '6':
            if ((ch = *str++) == '4' && (ch = *str++) == ':') {
                u64 = strtoull(str, 0, 0);
                return new u64Number(u64);
            }
            break;
        case ':':
            i = read_inum(str);
            return new iNumber(i);
        }
        break;
    }
    d = read_dnum(ss);
    return new dNumber(d);
}

// static
Worker::Number *
Worker::Number::mk(const decimal & d)
{
    return new dNumber(d);
}

// static
Worker::Number *
Worker::Number::mk(float x)
{
    return new f32Number(x);
}

// static
Worker::Number *
Worker::Number::mk(double x)
{
    return new f64Number(x);
}

// static
Worker::Number *
Worker::Number::mk(long double x)
{
    return new ldblNumber(x);
}

// static
Worker::Number *
Worker::Number::mk(const integer & k)
{
    return new iNumber(k);
}

// static
Worker::Number *
Worker::Number::mk(int k)
{
    return new i32Number(k);
}

// static
Worker::Number *
Worker::Number::mk(unsigned int k)
{
    return new u32Number(k);
}

// static
Worker::Number *
Worker::Number::mk(long long int k)
{
    return new i64Number(k);
}

// static
Worker::Number *
Worker::Number::mk(unsigned long long int k)
{
    return new u64Number(k);
}

// ===========================
// Worker::iNumber

// virtual
Worker::iNumber::~iNumber()
{ }

// virtual
size_t
Worker::iNumber::write(Arr & buf, char) const
{
    size_t z = write_inum(buf, k);
    buf.ins("int: ");
    return z + 5;
}

// virtual
Worker::Number *
Worker::iNumber::to_decimal() const
{
    decimal x(k);
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::iNumber::to_f32() const
{
    integer::builder b(k);
    float y;
    if (! b.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::iNumber::to_f64() const
{
    integer::builder b(k);
    double y;
    if (! b.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::iNumber::to_ldbl() const
{
    integer::builder b(k);
    long double y;
    if (! b.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::iNumber::to_integer() const
{
    return new iNumber(k);
}

// virtual
Worker::Number *
Worker::iNumber::to_i32() const
{
    integer::builder b(k);
    int y;
    if (! b.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::iNumber::to_i64() const
{
    integer::builder b(k);
    long long int y;
    if (! b.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::iNumber::to_u32() const
{
    integer::builder b(k);
    unsigned int y;
    if (! b.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::iNumber::to_u64() const
{
    integer::builder b(k);
    unsigned long long int y;
    if (! b.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::iNumber::sqrt(size_t prec) const
{
    decimal d(k);
    decimal y = d.sqrt_(prec);
    return Number::mk(y);
}

// ===========================
// Worker::dNumber

// virtual
Worker::dNumber::~dNumber()
{ }

// virtual
size_t
Worker::dNumber::write(Arr & buf, char f) const
{
    size_t z = write_dnum(buf, x, f);
    buf.ins("dec: ");
    return z + 5;
}

// virtual
Worker::Number *
Worker::dNumber::to_decimal() const
{
    return new dNumber(x);
}

// virtual
Worker::Number *
Worker::dNumber::to_f32() const
{
    float y;
    if (! x.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::dNumber::to_f64() const
{
    double y;
    if (! x.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::dNumber::to_ldbl() const
{
    long double y;
    if (! x.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::dNumber::to_integer() const
{
    alf::integer::builder b;
    integer z;

    if (b.assign(x) >= 0)
        z = integer(b);
    return Number::mk(z);
}

// virtual
Worker::Number *
Worker::dNumber::to_i32() const
{
    int y;
    if (! x.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::dNumber::to_i64() const
{
    long long y;
    if (! x.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::dNumber::to_u32() const
{
    unsigned int y;
    if (! x.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::dNumber::to_u64() const
{
    unsigned long long y;
    if (! x.get(y)) return 0;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::dNumber::sqrt(size_t prec) const
{
    decimal y = x.sqrt_(prec);
    return Number::mk(y);
}

// ===========================
// Worker::i32Number

// virtual
Worker::i32Number::~i32Number()
{ }

// virtual
size_t
Worker::i32Number::write(Arr & buf, char) const
{
    char b[1024];
    size_t z = snprintf(b, sizeof(b), "i32: %d", k);
    buf.ins(b, z);
    return z;
}

// virtual
Worker::Number *
Worker::i32Number::to_decimal() const
{
    decimal x(k);
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i32Number::to_f32() const
{
    float x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i32Number::to_f64() const
{
    double x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i32Number::to_ldbl() const
{
    long double x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i32Number::to_integer() const
{
    integer u(k);
    return Number::mk(u);
}

// virtual
Worker::Number *
Worker::i32Number::to_i32() const
{
    return new i32Number(k);
}

// virtual
Worker::Number *
Worker::i32Number::to_i64() const
{
    long long int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i32Number::to_u32() const
{
    unsigned int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i32Number::to_u64() const
{
    unsigned long long int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i32Number::sqrt(size_t prec) const
{
    decimal d(k);
    decimal y = d.sqrt_(prec);
    return Number::mk(y);
}

// ===========================
// Worker::u32Number

// virtual
Worker::u32Number::~u32Number()
{ }

// virtual
size_t
Worker::u32Number::write(Arr & buf, char) const
{
    char b[1024];
    size_t z = snprintf(b, sizeof(b), "u32: %u", k);
    buf.ins(b, z);
    return z;
}

// virtual
Worker::Number *
Worker::u32Number::to_decimal() const
{
    decimal x(k);
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u32Number::to_f32() const
{
    float x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u32Number::to_f64() const
{
    double x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u32Number::to_ldbl() const
{
    long double x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u32Number::to_integer() const
{
    integer x(k);
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u32Number::to_i32() const
{
    int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u32Number::to_i64() const
{
    long long int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u32Number::to_u32() const
{
    unsigned int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u32Number::to_u64() const
{
    unsigned long long int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u32Number::sqrt(size_t prec) const
{
    decimal d(k);
    decimal y = d.sqrt_(prec);
    return Number::mk(y);
}

// =========================
// Worker::i64Number

// virtual
Worker::i64Number::~i64Number()
{ }

// virtual
size_t
Worker::i64Number::write(Arr & buf, char) const
{
    char b[1024];
    size_t z = snprintf(b, sizeof(b), "i64: %lld", k);
    buf.ins(b, z);
    return z;
}

// virtual
Worker::Number *
Worker::i64Number::to_decimal() const
{
    decimal x(k);
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i64Number::to_f32() const
{
    float x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i64Number::to_f64() const
{
    double x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i64Number::to_ldbl() const
{
    long double x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i64Number::to_integer() const
{
    integer x(k);
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i64Number::to_i32() const
{
    int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i64Number::to_i64() const
{
    return new i64Number(k);
}

// virtual
Worker::Number *
Worker::i64Number::to_u32() const
{
    unsigned int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i64Number::to_u64() const
{
    unsigned long long int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::i64Number::sqrt(size_t prec) const
{
    decimal d(k);
    decimal y = d.sqrt_(prec);
    return Number::mk(y);
}

// ===========================
// Worker::u64Number

// virtual
Worker::u64Number::~u64Number()
{ }

// virtual
size_t
Worker::u64Number::write(Arr & buf, char) const
{
    char b[1024];
    size_t z = snprintf(b, sizeof(b), "u64: %llu", k);
    buf.ins(b, z);
    return z;
}

// virtual
Worker::Number *
Worker::u64Number::to_decimal() const
{
    decimal x(k);
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u64Number::to_f32() const
{
    float x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u64Number::to_f64() const
{
    double x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u64Number::to_ldbl() const
{
    long double x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u64Number::to_integer() const
{
    integer x(k);
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u64Number::to_i32() const
{
    int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u64Number::to_i64() const
{
    long long int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u64Number::to_u32() const
{
    unsigned int x = k;
    return Number::mk(x);
}

// virtual
Worker::Number *
Worker::u64Number::to_u64() const
{
    return new u64Number(k);
}

// virtual
Worker::Number *
Worker::u64Number::sqrt(size_t prec) const
{
    decimal d(k);
    decimal y = d.sqrt_(prec);
    return Number::mk(y);
}

// ===================
// Worker::f32Number

// virtual
Worker::f32Number::~f32Number()
{ }

// virtual
size_t
Worker::f32Number::write(Arr & buf, char f) const
{
    char b[1024];
    char fmt[20];
    switch (f) {
    default:
    case 's': f = 'e'; break;
    case 'S': f = 'E'; break;
    case 'F': f = 'f'; break;
    case 'f':
    case 'g':
    case 'e':
    case 'E':
    case 'G': break;
    }
    snprintf(fmt, sizeof(fmt), "f32: %%.7%c", f);
    size_t z = snprintf(b, sizeof(b), fmt, x);
    buf.ins(b, z);
    return z;
}

// virtual
Worker::Number *
Worker::f32Number::to_decimal() const
{
    decimal y(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f32Number::to_f32() const
{
    return new f32Number(x);
}

// virtual
Worker::Number *
Worker::f32Number::to_f64() const
{
    double y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f32Number::to_ldbl() const
{
    long double y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f32Number::to_integer() const
{
    integer y(x);
    return Number::mk(y);
    return 0;
}

// virtual
Worker::Number *
Worker::f32Number::to_i32() const
{
    int y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f32Number::to_i64() const
{
    long long int y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f32Number::to_u32() const
{
    unsigned int y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f32Number::to_u64() const
{
    unsigned long long int y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f32Number::sqrt(size_t prec) const
{
    decimal d(x);
    decimal y = d.sqrt_(prec);
    return Number::mk(y);
}

// ===================
// Worker::f64Number

// virtual
Worker::f64Number::~f64Number()
{ }

// virtual
size_t
Worker::f64Number::write(Arr & buf, char f) const
{
    char b[1024];
    char fmt[20];
    switch (f) {
    default:
    case 's': f = 'e'; break;
    case 'S': f = 'E'; break;
    case 'F': f = 'f'; break;
    case 'f':
    case 'g':
    case 'e':
    case 'E':
    case 'G': break;
    }
    snprintf(fmt, sizeof(fmt), "f64: %%.16%c", f);
    size_t z = snprintf(b, sizeof(b), fmt, x);
    buf.ins(b, z);
    return z;
}

// virtual
Worker::Number *
Worker::f64Number::to_decimal() const
{
    decimal y(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f64Number::to_f32() const
{
    float y = float(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f64Number::to_f64() const
{
    return new f64Number(x);
}

// virtual
Worker::Number *
Worker::f64Number::to_ldbl() const
{
    long double y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f64Number::to_integer() const
{
    integer y(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f64Number::to_i32() const
{
    int y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f64Number::to_i64() const
{
    long long int y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f64Number::to_u32() const
{
    unsigned int y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f64Number::to_u64() const
{
    unsigned long long int y = x;
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::f64Number::sqrt(size_t prec) const
{
    decimal d(x);
    decimal y = d.sqrt_(prec);
    return Number::mk(y);
}

// =========================
// Worker::ldblNumber

// virtual
Worker::ldblNumber::~ldblNumber()
{ }

// virtual
size_t
Worker::ldblNumber::write(Arr & buf, char f) const
{
    char b[1024];
    char fmt[20];
    switch (f) {
    default:
    case 's': f = 'e'; break;
    case 'S': f = 'E'; break;
    case 'F': f = 'f'; break;
    case 'f':
    case 'g':
    case 'e':
    case 'E':
    case 'G': break;
    }
    snprintf(fmt, sizeof(fmt), "ldbl: %%.20L%c", f);
    size_t z = snprintf(b, sizeof(b), fmt, x);
    buf.ins(b, z);
    return z;
}

// virtual
Worker::Number *
Worker::ldblNumber::to_decimal() const
{
    decimal y(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::ldblNumber::to_f32() const
{
    float y = float(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::ldblNumber::to_f64() const
{
    double y = double(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::ldblNumber::to_ldbl() const
{
    return new ldblNumber(x);
}

// virtual
Worker::Number *
Worker::ldblNumber::to_integer() const
{
    integer y(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::ldblNumber::to_i32() const
{
    int y = int(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::ldblNumber::to_i64() const
{
    long long int y = int(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::ldblNumber::to_u32() const
{
    unsigned int y = int(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::ldblNumber::to_u64() const
{
    unsigned long long int y = int(x);
    return Number::mk(y);
}

// virtual
Worker::Number *
Worker::ldblNumber::sqrt(size_t prec) const
{
    decimal d(x);
    decimal y = d.sqrt_(prec);
    return Number::mk(y);
}
