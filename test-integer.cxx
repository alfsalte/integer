
#include <bits/allocator.h>

#include <ctype.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdlib.h>

#include "integer.hxx"

namespace {

class Arr {
public:
    Arr() : base(vec), end(vec + sizeof(vec))
    { *(ptr = --end) = 0; }

    ~Arr() { if (base != vec) delete [] base; }

    Arr & clear() { ptr = end; return *this; }
    Arr & ins(char x) { ensure(1); *--ptr = x; return *this; }
    const char * data() const { return ptr; }
    size_t len() { return end - ptr; }

private:
    void ensure(size_t k);

    char vec[128];
    char * base;
    char * ptr;
    char * end;
};

class Worker {
public:
    typedef unsigned int digit;
    typedef unsigned long long int dbl_digit;
    typedef alf::integer::builder builder;
    typedef alf::integer integer;

    int Main(int argc, char * argv[]);
    integer read_num(const char * s);
    size_t write_num(Arr & buf, const integer & a);
};

};

int main(int argc, char * argv[])
{
    return Worker().Main(argc, argv);
}

int Worker::Main(int argc, char * argv[])
{
    enum {
        ABS, ADD, AND, ANDNOT, CMP, DIV, DIVMOD, FACT, INV, MOD, MUL,
        NEG, NEGBITS, NOT, OR, POW, POW3, SHL, SHR, SUB, XOR,
    };
    struct ent { const char * str; size_t mlen; int args, op; };
    static const ent optbl[] = {
        {"abs", 2, 1, ABS},
        {"add", 1, 2, ADD}, {"and", 2, 2, AND}, {"andnot", 4, 2, ANDNOT},
        {"cmp", 1, 2, CMP}, {"div", 1, 2, DIV}, {"divmod", 4, 2, DIVMOD},
        {"fact", 1, 1, FACT},
        {"inv", 1, 1, INV}, {"mod", 2, 2, MOD}, {"mul", 1, 2, MUL},
        {"neg", 2, 1, NEG}, {"negbits", 4, 1, NEGBITS}, {"not", 1, 1, NOT},
        {"or", 1, 2, OR}, {"pow", 1, 2, POW}, {"pow3", 4, 3, POW3},
        {"shl", 3, 2, SHL}, {"shr", 3, 2, SHR},
        {"sub", 1, 2, SUB}, {"xor", 1, 2, XOR},
        { 0, 0, 0},
    };

    if (argc < 3) {
        fprintf(stderr, "Missing args\n");
        return 1;
    }

    Arr buf;
    builder bb;
    integer a, b, c, d;
    const char * str = "";
    digit dig = 0;
    int j;
    char ch = 0;
    bool showc = true, showd = false, useb = true;

    const char * opstr = argv[1];
    size_t oplen = strlen(opstr);
    if (oplen == 0) {
        fprintf(stderr, "Missing operation.\n");
        return 1;
    }
    const ent * opq = optbl;
    int op = -1;
    while (opq->str!=0) {
        if (oplen >= opq->mlen && (op = strncmp(opstr, opq->str, oplen)) <= 0)
            break;
        ++opq;
    }
    if (op) {
        fprintf(stderr, "Unknown operation '%s'\n", opstr);
        return 1;
    }
    op = opq->op;
    printf( "Operation is: %s (%d: args: %d)\n", opq->str, op, opq->args);
    if (argc < opq->args + 2) {
        fprintf(stderr, "Missing args\n");
        return 1;
    }
    const char * a_str = argv[2];
    const char * b_str = 0;
    a = read_num(a_str);
    if (opq->args >= 2) b = read_num(b_str = argv[3]);
    if (opq->args >= 3) c = read_num(argv[4]);

    switch (op) {
    case ABS: c = a.abs(); break;
    case ADD:
        c = a + b;
        break;
    case AND:
        c = a & b;
        break;
    case ANDNOT:
        c = a.andnot(b);
        break;
    case CMP:
        j = a.cmp(b);
        showc = false;
        if (j > 0) printf("first value is larger\n");
        else if (j < 0) printf("second value is larger\n");
        else printf("the two values are equal\n");
        break;
    case DIV:
        c = a / b;
        break;
    case DIVMOD:
        j = integer::divmod(c, d, a, b);
        printf("retval from div_: %d\n", j);
        showd = true;
        break;
    case FACT:
        c = fact(a);
        break;
    case INV:
        c = ~a;
        break;
    case MOD:
        c = a % b;
        break;
    case MUL:
        c = a * b;
        break;
    case NEG:
        c = -a;
        break;
    case NEGBITS:
        bb.assign(a);
        bb.negbits();
        c = bb;
        break;
    case NOT:
        c = ~a;
        break;
    case OR:
        c = a | b;
        break;
    case POW:
        c = pow(a, b);
        break;
    case POW3:
        d = pow(a, b, c);
        showc = false;
        showd = true;
        break;
    case SHL:
        c = a << b;
        break;
    case SHR:
        c = a >> b;
        break;
    case SUB:
        c = a - b;
        break;
    case XOR:
        c = a ^ b;
        break;
    }

    if (showc) {
        buf.clear();
        write_num(buf, c);
        printf( "c = %s\n", buf.data());
    }
    if (showd) {
        buf.clear();
        write_num(buf, d);
        printf("d = %s\n", buf.data());
    }
}

alf::integer
Worker::read_num(const char * s)
{
    builder b;

    int ch = *s++;
    int neg = 0;
    while (ch != 0 && isspace(ch)) ch = *s++;
    switch (ch) {
    case '-': neg = 1; /* FALLTHRU */
    case '+':
        if ((ch = *s++) != 0) break;
        /* FALLTHRU */
    case 0: return integer(0);
    }

    while (isdigit(ch)) {
        digit d = ch - '0';
        b.mul_(10, d);
        ch = *s++;
    }
    if (neg)
        b.neg();
    return integer(b);
}

size_t Worker::write_num(Arr & buf, const integer & a)
{
    builder b(a);
    b.normalize();
    size_t bn = b.length();
    size_t w = 0;
    int gg = 4;
    do {
        digit z = b.div_(10);
        if (--gg == 0) {
            buf.ins(',');
            gg = 3;
            ++w;
        }
        buf.ins(z + '0');
        ++w;
    } while (bn > 0 && (b.data()[bn-1] != 0 || --bn > 0));
    if (a.s_len() < 0) {
        buf.ins('-');
        ++w;
    }
    return w;
}

void
Arr::ensure(size_t k)
{
    enum { SMALL = 4096, LARGE = 65536, };

    if (ptr - k < base) {
        size_t n = end + 1 - ptr;
        size_t m = end + 1 - base;
        size_t u = n + k;
        if (m < SMALL) m = SMALL;
        while (m < LARGE && m < u) m <<= 1;
        while (m < u) m += LARGE;
        char * p = new char[m];
        size_t pos = m - n;
        memset(p, 0, pos);
        memcpy(p + pos, ptr, n);
        if (ptr != vec) delete [] ptr;
        base = p;
        ptr = p + pos;
        end = p + m - 1;
    }
}
