
#include <bits/allocator.h>

#include <ctype.h>

#include <limits.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "integer.hxx"
#include "decimal.hxx"

namespace {

typedef alf::decimal::digit digit;
typedef alf::decimal::dbl_digit dbl_digit;

template <class T, T a, digit b>
struct cxpow_;

template <class T, T a>
struct cxpow_<T, a, 0> {
    enum { value = T(1) };
};

template <class T, T a, digit b>
struct cxpow_ {
    enum {
        v = b & 1 ? a : T(1),
        w = T(cxpow_<T, a, (b>>1)>::value),
        value = w*w*v,
    };
};


const alf::decimal::digit stens[] = {
    cxpow_<digit, 10, 0>::value, cxpow_<digit, 10, 1>::value,
    cxpow_<digit, 10, 2>::value, cxpow_<digit, 10, 3>::value,
    cxpow_<digit, 10, 4>::value, cxpow_<digit, 10, 5>::value,
    cxpow_<digit, 10, 6>::value, cxpow_<digit, 10, 7>::value,
    cxpow_<digit, 10, 8>::value, cxpow_<digit, 10, 9>::value
};

const alf::decimal::dbl_digit dtens[] = {
    cxpow_<dbl_digit, 10, 0>::value, cxpow_<dbl_digit, 10, 1>::value,
    cxpow_<dbl_digit, 10, 2>::value, cxpow_<dbl_digit, 10, 3>::value,
    cxpow_<dbl_digit, 10, 4>::value, cxpow_<dbl_digit, 10, 5>::value,
    cxpow_<dbl_digit, 10, 6>::value, cxpow_<dbl_digit, 10, 7>::value,
    cxpow_<dbl_digit, 10, 8>::value, cxpow_<dbl_digit, 10, 9>::value,
    cxpow_<dbl_digit, 10,10>::value, cxpow_<dbl_digit, 10,11>::value,
    cxpow_<dbl_digit, 10,12>::value, cxpow_<dbl_digit, 10,13>::value,
    cxpow_<dbl_digit, 10,14>::value, cxpow_<dbl_digit, 10,15>::value,
    cxpow_<dbl_digit, 10,16>::value, cxpow_<dbl_digit, 10,17>::value,
    cxpow_<dbl_digit, 10,18>::value
};

};

// static
alf::decimal::rep alf::decimal::qnanr_;
alf::decimal::rep alf::decimal::snanr_;
alf::decimal::rep alf::decimal::ninfr_;
alf::decimal::rep alf::decimal::pinfr_;

alf::decimal::repzero alf::decimal::nzeror_;
alf::decimal::repzero alf::decimal::pzeror_;

alf::decimal::digit * alf::decimal::qnan_ = 0;
alf::decimal::digit * alf::decimal::snan_ = 0;
alf::decimal::digit * alf::decimal::ninf_ = 0;
alf::decimal::digit * alf::decimal::pinf_ = 0;
alf::decimal::digit * alf::decimal::nzero_ = 0;
alf::decimal::digit * alf::decimal::pzero_ = 0;

size_t alf::decimal::prec = 200;
int alf::decimal::rnd = NEAREST_TIES_TO_EVEN;

const char * parseinfo_exp[] = { "e", "E", "&", "@", 0 };

alf::decimal::parse_info alf::decimal::parseinfo = {
	parseinfo_exp, { "+", "-" }, "\'", "." };

// ==========================
// decimal

// static
void
alf::decimal::initv()
{
	qnan_ = qnanr_.mkrep(0, 0, QNAN);
	snan_ = snanr_.mkrep(0, 0, SNAN);
	ninf_ = ninfr_.mkrep(0, 0, NINF);
	pinf_ = pinfr_.mkrep(0, 0, PINF);
	nzero_ = nzeror_.mkrepz(0, 0, 0, NEGZERO);
	pzero_ = pzeror_.mkrepz(0, 0, 0, PZERO);
}

// static
alf::decimal::digit *
alf::decimal::qnanv()
{ if (qnan_ == 0) initv(); return qnan_; }

// static
alf::decimal::digit *
alf::decimal::snanv()
{ if (snan_ == 0) initv(); return snan_; }

// static
alf::decimal::digit *
alf::decimal::pinfv()
{ if (pinf_ == 0) initv(); return pinf_; }

alf::decimal::digit *
alf::decimal::ninfv()
{ if (ninf_ == 0) initv(); return ninf_; }

alf::decimal::digit *
alf::decimal::nzerov()
{ if (nzero_ == 0) initv(); return nzero_; }

alf::decimal::digit *
alf::decimal::pzerov()
{ if (pzero_ == 0) initv(); return pzero_; }

// static
void
alf::decimal::rm(rep * r)
{
    if (r) {
        switch (r->klass) {
        default: delete [] (char *)r; /* FALLTHRU */
        case SNAN:
        case QNAN:
        case PINF:
        case NINF:
        case PZERO:
        case NEGZERO: break;
        }
    }
}


alf::decimal::digit *
alf::decimal::mk(const digit * d, size_t dn, ssize_t dx, int kls)
{
	int neg = 0;

	switch (kls) {
	case SNAN: return snanv();
	case QNAN: return qnanv();
	case PINF: return pinfv();
	case NINF: return ninfv();
	case PZERO: return pzerov();
	case NEGZERO: return nzerov();
	case NREGULAR: neg = 1; break;
    default: kls = PREGULAR; break;
	}
	dn = integer::builder::normalize_(d, dn);
	if (dn == 0)
		return neg ? nzerov() : pzerov();
	ssize_t ds = neg ? -dn : dn;
	size_t sz = sizeof(rep) + dn * sizeof(digit);
	char * cp = new char[sz];
	rep * r = (rep *)cp;
	digit * q = r->mkrep(ds, dx, kls);
	memcpy(q, d, dn*sizeof(digit));
	return q;
}

alf::decimal::digit *
alf::decimal::mk(const integer::builder & b)
{
	builder B(b);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(const integer & k)
{
	builder B(k);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(int k)
{
	builder B(k);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(unsigned int k)
{
	builder B(k);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(long int k)
{
	builder B(k);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(unsigned long int k)
{
	builder B(k);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(long long int k)
{
	builder B(k);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(unsigned long long int k)
{
	builder B(k);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(float x)
{
	builder B(x);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(double x)
{
	builder B(x);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal::digit *
alf::decimal::mk(long double x)
{
	builder B(x);
	return mk(B.p, B.n, B.x, B.klass);
}

alf::decimal &
alf::decimal::assign(builder & b)
{
    decref(dataptr.d);
	dataptr.d = mk(b.p, b.n, b.x, b.klass);
	return *this;
}


bool
alf::decimal::isneg() const
{
	switch (Rep()->klass) {
	case NINF:
	case PZERO:
	case NEGZERO:
	case NREGULAR: return true;
	}
	return false;
}

bool
alf::decimal::ispos() const
{
	switch (Rep()->klass) {
	case PINF:
	case PZERO:
	case NEGZERO:
	case PREGULAR: return true;
	}
	return false;
}

bool alf::decimal::isinf() const
{
	switch (Rep()->klass) {
	case PINF:
	case NINF: return true;
	}
	return false;
}

bool
alf::decimal::iszero() const
{
	switch (Rep()->klass) {
	case PZERO:
	case NEGZERO: return true;
	}
	return false;
}

bool
alf::decimal::isstrictpos() const
{
	switch (Rep()->klass) {
	case PINF:
	case PREGULAR: return true;
	}
	return false;
}

bool
alf::decimal::isstrictneg() const
{
	switch (Rep()->klass) {
	case NINF:
	case NREGULAR: return true;
	}
	return false;
}

bool
alf::decimal::isnan() const
{
	switch (Rep()->klass) {
	case SNAN:
	case QNAN: return true;
	}
	return false;
}

// static
alf::decimal alf::decimal::snanres()
{
	// TODO check if we should throw error.
	return decimal(snanv());
}

bool
alf::decimal::get(integer & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(integer::builder & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(signed char & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(short & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(int & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(long & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(long long & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(unsigned char & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(unsigned short & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(unsigned int & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(unsigned long & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(unsigned long long int & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(char & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(bool & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(float & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(double & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

bool
alf::decimal::get(long double & k) const
{
    builder b(data(), length(), slength(),
                expon(), klass());
    return b.get(k);
}

alf::decimal::builder &
alf::decimal::builder::sqrt(const builder & bb, size_t prec)
{
    builder b, c, d, eps;
    size_t pprec = prec + prec;
    assign(bb);
    switch (klass) {
    case SNAN:
    case QNAN:
    case PINF:
    case PZERO: return *this;
    case NEGZERO: neg(); return *this;
    case NINF:
    case NREGULAR: klass = SNAN; return *this;
    }
    // a rough estimate is x with half the exponent.
    x >>= 1;
    int u = 30; // we do at least this many rounds..
    eps.assign(1);
    eps.x = -pprec;
    while (--u >= 0) {
        // now compute b = (bb/y + y)/2
        b.div(pprec, rnd, bb.p, bb.n, bb.s, bb.x, bb.klass,
                p, n, s, x, klass);
        c.add(b.p, b.n, b.s, b.x, b.klass, p, n, s, x, klass);
        b.div(pprec, rnd, c.p, c.n, c.s, c.x, c.klass, 2);
        // and then compute one more round into y.
        // i.e. compute y = (bb/b + b)/2
        div(pprec, rnd, bb.p, bb.n, bb.s, bb.x, bb.klass,
                b.p, b.n, b.s, b.x, b.klass);
        c.add(p, n, s, x, klass, b.p, b.n, b.s, b.x, b.klass);
        div(pprec, rnd, c.p, c.n, c.s, c.x, c.klass, 2);
    }
    n = 1000;
    while (true) {
        b.div(pprec, rnd, bb.p, bb.n, bb.s, bb.x, bb.klass,
                p, n, s, x, klass);
        c.add(b.p, b.n, b.s, b.x, b.klass, p, n, s, x, klass);
        b.div(pprec, rnd, c.p, c.n, c.s, c.x, c.klass, 2);
        div(pprec, rnd, bb.p, bb.n, bb.s, bb.x, bb.klass,
                b.p, b.n, b.s, b.x, b.klass);
        c.add(p, n, s, x, klass, b.p, b.n, b.s, b.x, b.klass);
        div(pprec, rnd, c.p, c.n, c.s, c.x, c.klass, 2);
        d.sub(b.p, b.n, b.s, b.x, b.klass, p, n, s, x, klass);
        d.abs();
        int j = d.cmp(eps);
        if (j == -2) { klass = SNAN; return *this; }
        if (j <= 0) return *this;
        // no more than 1000 times.
        if (--u < 0) return *this;
    }
}

alf::decimal
alf::decimal::sqrt_(size_t prec) const
{
    builder b(*this);
    b.sqrt(prec);
    return decimal(b);
}

// ==========================
// builder
alf::decimal::builder::builder()
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{ }

alf::decimal::builder::builder(const parse_info & pi,
                    			parse_data & pd,
                    			const char * str)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
	parse(pi, pd, str);
}

alf::decimal::builder::builder(const char * str, char ** endptr)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
	parse(str, endptr);
}

alf::decimal::builder::builder(const char * str, size_t sz,
								char ** endptr)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
	parse(str, sz, endptr);
}

alf::decimal::builder::builder(const builder & b)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    ensure(b.n);
    memcpy(p, b.p, b.n*sizeof(digit));
    n = b.n;
    s = b.s < 0 ? -n : n;
    x = b.x;
    klass = b.klass;
}

alf::decimal::builder::builder(const integer::builder & b)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(b);
}

alf::decimal::builder::builder(const decimal & d)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(d);
}

alf::decimal::builder::builder(const integer & k)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(k);
}

alf::decimal::builder::builder(int k)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(k);
}

alf::decimal::builder::builder(unsigned int k)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(k);
}

alf::decimal::builder::builder(long k)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(k);
}

alf::decimal::builder::builder(unsigned long k)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(k);
}

alf::decimal::builder::builder(long long k)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(k);
}

alf::decimal::builder::builder(unsigned long long k)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(k);
}

alf::decimal::builder::builder(float x)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(x);
}

alf::decimal::builder::builder(double x)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(x);
}

alf::decimal::builder::builder(long double x)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(x);
}

alf::decimal::builder::builder(const digit * xp, size_t xn,
                                ssize_t xs, ssize_t xx,
                                int xkls)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(xp, xn, xs, xx, xkls);
}

alf::decimal::builder::builder(const digit * xp,
                                ssize_t xs, ssize_t xx,
                                int xk)
    : p(arr), n(0), m(INI_SIZE), s(0), x(0), klass(PZERO)
{
    assign(xp, size_t(xs < 0 ? -xs : xs), xs, xx, xk);
}

alf::decimal::builder::~builder()
{
    if (p != arr) delete [] p;
}

alf::decimal::builder &
alf::decimal::builder::mkclass()
{
    int neg = 0;
    int zero = 0;

    switch (klass) {
    case SNAN:
    case QNAN:
    case PINF:
    case NINF: return *this;
    case NEGZERO:
    case NREGULAR: neg = 1; break;
    }
    if ((n = normalize___(p, n)) == 0) {
        p[n++] = 0;
        zero = 1;
        x = 0;
    }
    s = neg ? -n : n;
    klass = (neg ? NEG : POS) | (zero ? ZERO : REGULAR);
}

alf::decimal::builder &
alf::decimal::builder::ensure(size_t k)
{
    size_t j = n + k;
    if (j > m) {
        while (m < j && m < LARGE_SIZE) m <<= 1;
        while (m < j) m += LARGE_SIZE;
        digit * q = new digit[m];
        memcpy(q, p, n*sizeof(digit));
        memset(q + n, 0, (m-n)*sizeof(digit));
        if (p != arr) delete [] p;
        p = q;
    }
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::length_at_least(size_t k)
{
    if (n < k) {
        size_t j = k - n;
        ensure(j);
        memset(p+n, 0, j*sizeof(digit));
    }
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::exp_at_most(ssize_t xx)
{
    if (x > xx) {
        size_t u = x - xx;
        size_t w = u / N;
        size_t v = u % N;
        if (w) {
            ensure(w);
            memmove(p + w, p, n*sizeof(digit));
            memset(p, 0, w*sizeof(digit));
            n += w;
        }
        // 10**v, v is 0..8.
        if (v) mul_(stens[v],0);
        x = xx;
    }
    return *this;
}

namespace {

size_t is_prefix(const char * str, const char * t)
{
	if (str == 0 || t == 0 || *t == 0) return 0;

	size_t p = strlen(t);
	if (strncmp(str, t, p) == 0) return p;
	return 0;
}

int is_prefix(const char * str, const char * const * tbl,
				size_t tbln, size_t & xlen)
{
	int x;
	const char * t;
	for (x = 0; (tbln != 0 && x < tbln) || (t = tbl[x]) != 0;
			++x) {
		size_t p = strlen(t);
		if (p == 0) continue;
		if (strncmp(str, t, p) == 0) {
			xlen = p;
			return x;
		}
	}
	return -1;
}

};

alf::decimal::builder &
alf::decimal::builder::parse(const parse_info & pi,
                    			parse_data & pd,
                    			const char * str)
{
    // given a state, what index in accept, action and nxt
    // do we start looking, also the following index
    // gives the index where to stop.
    // given state index, get first action index
    // for this state, state+1 give first action of next
    // state and thus the index where to stop.
    static const int st0[] = {
        // 1  2  3  4  5  6   7   8
        0, 4, 5, 6, 7, 8, 9, 10, 11,
        //  10  11
        11, 12, 13,
        //  13
        13, 14,
        // 14
        15
    };

    // given action index find the next accepted char.
    // we start from st0[state] and look for index that
    // matches next char until st0[state+1]. The resulting
    // index is action index.
    static const char accept[] = {
        // 0    1   2   3   4   5   6   7   8
        // 0..3 4   5   6   7   8   9   10 11
        "inqs" "n" "f" "i" "n" "i" "t" "y" ""
        // 9  10 11
        // 11 12 13
        "a" "n"  ""
        // 12 13
        // 13 14
        "n" "n"
        // 14
        // 15
    };

    // given action index, find action.
    // 0 - advance to next state, otherwise do nothing.
    // 1 - accept result is inf - do post processing.
    // 2 - accept result is nan - do post processing.
    // q - process q
    // s - process s.
    static const char action[] =
        // state 0  2   3   4   5   6   7  8
        //      4   5   6   7   8   9  10 11
        // inqs n   f   i   n   i   t   y
        "0000" "0" "0" "0" "0" "0" "0" "0" ""
        // state 9
        // 9  10 11
        //11  12 13
        //a   n 
        "0"  "0" ""
        // state 12
        // 13  14
        // n  n
        "q"  "s"
        // "state" 14
        // 15
        "";

    // given an action index get the next state.
    // states 0..11 incomplete states.
    // state 100 incomplete but no nan or inf.
    // state 101 error.
    // state 102 inf - complete.
    // state 103 nan - complete.
    static const int nxt[] = { 1, 9, 12, 13,
        // 5  6  7  8  9  10 11
        2, 3, 4, 5, 6, 7, 8,
        // state 9
        // 11 12 13
        10, 11,
        // state 12
        // 13  14
        9, 9
    };

    // 0 is incomplete, 1 is failed, 100 and above is
    // accepting states and the value indicate what state.
    // Given state return if we can accept this state and
    // what value we get.
    static const int accept_states[] = { 100, 1, 1,
        // 3:inf         8:inf      11:nan      12
        102, 1, 1, 1, 1, 102, 1, 1, 103, 1, 1, 101 };

	if (str == 0) return *this;
	pd.start = str;
	int ch = *str++;
	int neg = 0;
    int x = 0;
    int state = 0;
    int qsnan = 0;
    int dot = 0, need_dig = 1;
    size_t ndigs = 0;
    size_t fdigs = 0;
    size_t plen = 0;
    size_t qlen = 0;
    ssize_t j;
    const char * xp = 0;

    while (ch != 0 && isspace(ch)) ch = *str++;
    switch(neg = is_prefix(--str, pi.sign, 2, plen)) {
    case -1: neg = 0; break;
    case 0: neg = '+'; str += plen; break;
    case 1: neg = '-'; str += plen; break;
	}
	if ((ch = *str++) == 0) {
		klass = QNAN;
		return *this;
    }

    while (state < 100) {
        int strt = st0[state];
        int stp = st0[state+1];
        if (isupper(ch)) ch = tolower(ch);
        while (strt < stp && accept[strt] != ch) ++strt;
        if (strt == stp) {
            int ast = accept_states[state];
            state = ast < 100 ? 101 : ast;
            continue;
        }
        // start is action index.
        // get next state.
        state = nxt[strt];
        ch = *str++;
        switch (action[strt]) {
        case '0':
        case '1':
        case '2': continue;
        case 'q': qsnan = 'q'; continue;
        case 's': qsnan = 's'; continue;
        default: state = 101; continue; // oops.
        }
    }
    if (state >= 102 && isalpha(ch)) state = 101;
    switch (state) {
    case 101:
        // error.
    	pd.p = str - 1;
    	klass = QNAN; return *this;
    case 102: // inf, heed sign.
    	klass = neg == '-' ? NINF : PINF;
    	return *this;
    case 103:
        // sign before nan is ignored but we heed qsnan.
        // if just nan, we treat it as a quiet nan.
    	klass = qsnan == 's' ? SNAN : QNAN;
    	return *this;
    }
    // no need to remember '+'' any more.
    neg = neg == '-';

    // instead of multiplying by 10 we
    // make one run first where we count the number of
    // digits and also remember how many were integer part
    // and how many in fractional part.
    // we then make a new run multiplying up the numbers
    // as we then know the size of n etc.
    ndigs = fdigs = 0;
    pd.p = str - 1;
    while (isdigit(ch)) {
    	++ndigs;
    	need_dig = 0;
    	if (dot) ++fdigs;
    	if ((plen = is_prefix(str, pi.dot)) > 0) {
    		if (dot) {
    			// two dots in number.
    			pd.p = str;
    			klass = QNAN;
    			return *this;
    		}
    		str += plen;
			dot = need_dig = 1;
		} else if ((plen = is_prefix(str, pi.sep)) > 0) {
			str += plen;
			need_dig = 1;
		}
		ch = *str++;
	}
	if (need_dig || ndigs == 0) {
		// digit required here..invalid syntax.
		pd.p = str - 1;
		klass = QNAN;
		return *this;
	}
	str = pd.p; // do it again.
	ch = *str++;
	plen = ndigs / N;
	qlen = ndigs % N;
	if (qlen == 0) { --plen; qlen = N; }
	ensure(plen + 1);
	j = n = plen + 1;
	// qlen is the number of digits to collect in the
	// "current" index which is at j.
	while (--j >= 0) {
		digit dig = 0;
		while (qlen > 0) {
			// get the digit...
			if (isdigit(ch)) {
				dig = dig * 10 + (ch - '0');
				--qlen;
			} else if ((plen = is_prefix(str, pi.dot)) > 0) {
				str += plen;
			} else if ((plen = is_prefix(str, pi.sep)) > 0) {
				str += plen;
			}
			ch = *str++;
		}
		p[j] = dig;
		qlen = N;
	}
	x = 0;
	if ((state = is_prefix(str, pi.exp, 0, plen)) >= 0) {
		const char * xp = str;
		str += plen;
        int xneg = is_prefix(str, pi.sign, 2, plen);
        switch (xneg) {
        case -1: xneg = 0; break;
        case 0: str += plen; break; // +
        case 1: str += plen; break; // -
        }
        ch = *str++;
        if (! isdigit(ch)) {
        	pd.p = str - 1;
        	klass = QNAN;
        	return *this;
        }
        do {
            x = x * 10 + (ch - '0');
            if (x < 0) {
            	// too large exponent.
            	pd.p = str - 1;
            	klass = QNAN;
            	return *this;
            }
            ch = *str++;
        } while (isdigit(ch));
        if (xneg) x = -x;
    }
    x -= fdigs;
    n = normalize___(p, n);
    if (n == 0) {
    	p[n++] = 0;
    	klass = neg ? NZERO : PZERO;
    	return *this;
    }
    klass = neg ? NREGULAR : PREGULAR;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::parse(const char * str, char ** endptr)
{
	if (str == 0) return *this;
	parse_data pd;
	pd.end = 0;
	parse(parseinfo, pd, str);
	if (endptr) *endptr = (char *)pd.p;
	return *this;
}

alf::decimal::builder &
alf::decimal::builder::parse(const char * str, size_t sz,
								char ** endptr)
{
	if (str == 0) return *this;
	parse_data pd;
	pd.end = str + sz;
	parse(parseinfo, pd, str);
	if (endptr) *endptr = (char *)pd.p;
	return *this;
}


alf::decimal::builder &
alf::decimal::builder::assign(const builder & b)
{
    size_t j = b.n;
    ensure(j);
    memcpy(p, b.p, j*sizeof(digit));
    n = j;
    s = b.s < 0 ? -n : n;
    x = b.x;
    klass = b.klass;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::assign(const integer::builder & b)
{
    enum { BITS = 30, R = 1 << BITS, M = R - 1, };

    size_t blen = b.length();
    const digit * d = b.data();
    size_t j = blen;
    n = 0;
    while (j > 0)
        mul_(R, d[--j]);
    s = b.slength() < 0 ? -n : n;
    x = 0;
    klass = s < 0 ? NREGULAR : s > 0 ? PREGULAR : PZERO;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::assign(const decimal & d)
{
	int neg = 0;

    switch (klass = d.klass()) {
    case SNAN:
    case QNAN:
    case PINF:
    case NINF:
    	n = 0;
    	s = x = 0;
    	return *this;
    case PZERO:
    	n = 1;
    	s = 1;
    	x = 0;
    	*p = 0;
    	return *this;
    case NEGZERO:
    	n = 1;
    	s = -1;
    	x = 0;
    	*p = 0;
    	return *this;
    case NREGULAR: neg = 1; break;
    }

    size_t dlen = d.length();
    ensure(dlen);
    memcpy(p, d.data(), dlen*sizeof(digit));
    n = dlen;
    s = neg ? -n : n;
    x = d.expon();
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::assign(const integer & k)
{
    enum { BITS = 30, R = 1 << BITS, M = R - 1, };

    size_t klen = k.u_len();
    const digit * d = k.data();
    size_t j = klen;
    n = 0;
    while (j > 0)
        mul_(R, d[--j]);
    s = k.s_len() < 0 ? -n : n;
    x = 0;
    klass = s < 0 ? NREGULAR : s > 0 ? PREGULAR : PZERO;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::assign(int k)
{
    int neg = k < 0;
    unsigned int u = (unsigned int)(k < 0 ? -k : k);
    n = 0;
    if (u >= R) {
        p[n++] = u % R;
        p[n++] = u / R;
    } else if (u) {
        p[n++] = u;
    }
    s = neg ? -n : n;
    x = 0;
    klass = (neg ? NEG : POS) | (n ? REGULAR : ZERO);
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::assign(unsigned int k)
{
    unsigned int u = k;
    n = 0;
    if (u >= R) {
        p[n++] = u % R;
        p[n++] = u / R;
    } else if (u) {
        p[n++] = u;
    }
    s = n;
    x = 0;
    klass = k ? PREGULAR : PZERO;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::assign(long k)
{ return assign((long long)k); }

alf::decimal::builder &
alf::decimal::builder::assign(unsigned long k)
{ return assign((unsigned long long)k); }

alf::decimal::builder &
alf::decimal::builder::assign(long long k)
{
    int neg = k < 0;
    dbl_digit u = dbl_digit(k < 0 ? -k : k);
    n = 0;
    if (u >= R) {
        p[n++] = u % R;
        if ((u /= R) >= R) {
            p[n++] = u % R;
            p[n++] = u / R;
        } else
            p[n++] = u;
    } else if (u)
        p[n++] = u;
    s = neg ? -n : n;
    x = 0;
    klass = (neg ? NEG : POS) | (n ? REGULAR : ZERO);
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::assign(unsigned long long k)
{
    unsigned long long int u = k;
    n = 0;
    if (u >= R) {
        p[n++] = u % R;
        if ((u /= R) >= R) {
            p[n++] = u % R;
            p[n++] = u / R;
        } else p[n++] = u;
    } else if (u) p[n++] = u;
    s = n;
    x = 0;
    klass = k ? PREGULAR : PZERO;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::assign(float x)
{ return assign(double(x)); }

alf::decimal::builder &
alf::decimal::builder::assign(double num)
{
    enum {
        XM      = 0x7ff0000000000000ULL,
        MM      = 0x000fffffffffffffULL,
        HIDDEN  = 0x0010000000000000ULL,
        QUIET   = 0x0008000000000000ULL,
    };
    enum { B = 1023, };

    unsigned long long int u = *(unsigned long long int *) & num;
    unsigned long long int mm = u & MM;
    unsigned long long int xm = u & XM;
    long long i = (long long)u;
    int xv = 0;

    n = 0;
    *p = 0;
    s = x = 0;
    switch (xm) {
    case 0:
        // subnormal number.
        xv = 1 - B;
        break;
    case XM: // NaN or inf.
        if (mm == 0) { // inf.
            if (i < 0) {
                klass = NINF; n = 1; s = -1;
            } else {
                klass = PINF;
            }
            return *this;
        }
        // NaN.
        klass = mm & QUIET ? QNAN : SNAN;
        return *this;
    default: // normal number.
        xv = (xm >> 52) - B;
        mm |= HIDDEN;
        break;
    }
    // 2**xv * 1.m == 2**(xv-52)*mm
    xv -= 52;
    // now, number is 2**xv * mm where mm is an unsigned int.
    // of course, we want to use base 10 (sort of)
    // so we would like it to be 10**xv * mm but that
    // is a factor of 5**xv too much. If xv is negative this
    // means  we need to multiply by 5**-xv to compensate
    // but if xv is positive we don't want to divide by 5
    // so instead we scale down by dividing by 10**xv
    // and then multiply by 2**xv to compensate.
    // first place mm in the digit of *this.
    builder f;
    unsigned int xV;

    assign(mm);
    if (mm) {
        builder qq(*this);
        if (xv < 0) {
            // so V = 2**xv * mm = 10**xv * mm * 5**-xv
            f.assign(5);
            xV = -xv;
        } else {
            // V == 2**xv * mm = 10**xv * mm * 0.2**xv
            f.assign(2);
            f.x = -1;
            xV = xv;
        }
        f.pow(xV);
        mul(qq.p, qq.n, qq.s, qq.x, qq.klass,
                f.p, f.n, f.n, f.x, f.klass);
        x += xv;
    }
    // notice that x is very likely negative here
    // only case it is not is if xv was positive
    // and then x is most likely 0.
    s = i < 0 ? -n : n;
    klass = (i < 0 ? NEG : POS) | (mm ? REGULAR : ZERO);
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::assign(long double num)
{
    enum {
        XM      = 0x7fff,
        B       = 16383,
    };
    enum {
        ONE     = 0x8000000000000000ULL,
        QUIET   = 0x4000000000000000ULL,
    };
    struct ldbl {
        unsigned long long int m;
        short x;
        char notused[6];
    };

    union {
        ldbl quux;
        long double number;
    };

    number = num;
    unsigned long long int mm = quux.m;
    int xv = 0;

    n = 0;
    *p = 0;
    s = x = 0;
    switch (quux.x & XM) {
    case 0:
        // subnormal number.
        xv = 1 - B;
        break;
    case XM: // NaN or inf.
        if (mm == ONE) { // inf.
            if (quux.x < 0) {
                klass = NINF; n = 1; s = -1;
            } else {
                klass = PINF;
            }
            return *this;
        }
        // NaN.
        klass = mm & QUIET ? QNAN : SNAN;
        return *this;
    default: // normal number.
        xv = (quux.x & XM) - B;
        break;
    }
    // 2**xv * 1.m == 2**(xv-63)*mm
    xv -= 63;
    // now, number is 2**xv * mm where mm is an unsigned int.
    // of course, we want to use base 10 (sort of)
    // so we would like it to be 10**xv * mm but that
    // is a factor of 5**xv too much. If xv is negative this
    // means  we need to multiply by 5**-xv to compensate
    // but if xv is positive we don't want to divide by 5
    // so instead we scale down by dividing by 10**xv
    // and then multiply by 2**xv to compensate.
    // first place mm in the digit of *this.
    builder f;

    assign(mm);
    if (mm) {
        builder qq(*this);
        if (xv < 0) {
            // divide by 2**-xv is the same as multiply
            // by 5**-xv and then set x = xv.
            f.assign(5);
            f.pow((unsigned int)-xv);
            mul(qq.p, qq.n, qq.s, qq.x, qq.klass, f.p, f.n, f.n, 0, PREGULAR);
            x += xv;
        } else {
            // number num == 2**xv * mm but we want
            f.assign(2);
            f.pow((unsigned int)xv);
            mul(qq.p, qq.n, qq.s, qq.x, qq.klass, f.p, f.n, f.n, 0, PREGULAR);
        }
    }
    // notice that x is very likely negative here
    // only case it is not is xv was positive
    // and then x is most likely 0.
    s = quux.x < 0 ? -n : n;
    klass = (quux.x < 0 ? NEG : POS) | (mm ? REGULAR : ZERO);
    return *this;

}

alf::decimal::builder &
alf::decimal::builder::assign(const digit * xp, size_t xn,
                                ssize_t xs, ssize_t xx, int kls)
{
    n = 0;
    ensure(xn);
    memcpy(p, xp, xn*sizeof(digit));
    n = xn;
    s = xs < 0 ? -n : n;
    x = xx;
    klass = kls;
}

alf::decimal::builder &
alf::decimal::builder::assign(const digit * xp,
                                ssize_t xs, ssize_t xx,
                                int kls)
{ return assign(xp, size_t(xs < 0 ? -xs : xs), xs, xx, kls); }

bool
alf::decimal::builder::get(integer & k) const
{
	integer::builder z;
	if (! get(z)) return false;
	k = z;
	return true;
}

bool
alf::decimal::builder::get(integer::builder & k) const
{
	int neg = 0;

	k.clear();
	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) return true; // k == 0.
		qn -= u9;
		q += u9;
	}
	while (qq > q)
		k.mul_(R, *--qq);
	if (u9mod)
		k.div_(stens[u9mod]);
	return true;
}

bool
alf::decimal::builder::get(signed char & k) const
{
	int neg = 0;

	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: k = 0; return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) { k = 0; return true; }
		qn -= u9;
		q += u9;
	}
	unsigned long long int u = 0;
	if (qq > q) u = *--qq;
	if (qq > q) u = u * R + *--qq;
	if (qq > q) return false;
	if (u9mod)
		u = u / dtens[u9mod];
	if (u > 128) return false;
	if (u == 128 && ! neg) return false;
	k = (signed char)(neg ? -u : u);
	return true;
}

bool
alf::decimal::builder::get(short & k) const
{
	int neg = 0;

	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: k = 0; return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) { k = 0; return true; }
		qn -= u9;
		q += u9;
	}
	unsigned long long int u = 0;
	if (qq > q) u = *--qq;
	if (qq > q) u = u * R + *--qq;
	if (qq > q) return false;
	if (u9mod)
		u = u / dtens[u9mod];
	if (u > 32768) return false;
	if (u == 32768 && ! neg) return false;
	k = short(neg ? -u : u);
	return true;
}

bool
alf::decimal::builder::get(int & k) const
{
	int neg = 0;

	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: k = 0; return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) { k = 0; return true; }
		qn -= u9;
		q += u9;
	}
	dbl_digit u = 0;
	if (qq > q) u = *--qq;
	if (qq > q) u = u * R + *--qq;
	if (qq > q) return false;
	if (u9mod)
		u = u / dtens[u9mod];
	if (u > dbl_digit(INT_MAX)+1) return false;
	if (u == dbl_digit(INT_MAX) + 1 && ! neg) return false;
	k = int(neg ? -u : u);
	return true;
}

bool
alf::decimal::builder::get(long & k) const
{
	int neg = 0;

	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: k = 0; return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) { k = 0; return true; }
		qn -= u9;
		q += u9;
	}
	unsigned long long int u1 = 0;
	unsigned long long int u2 = 0;
	unsigned long long int u3 = 0;
	unsigned long long int v = 0;
	if (qq > q) u1 = *--qq;
	if (qq > q) { u2 = u1; u1 = *--qq; }
	if (qq > q) { u3 = u2; u2 = u1; u1 = *--qq; }
	if (qq > q) return false;
	if (u9mod) {
		unsigned long long int w = dtens[u9mod];
		u2 += (u3 % w)*R;
		u3 /= w;
		u1 += (u2 % w)*R;
		u2 /= w;
		u1 /= w;
	}
	if (u3) return false;
	u1 += u2*R;
	if (u1 > dbl_digit(LONG_MAX)+1) return false;
	if (u1 == dbl_digit(LONG_MAX) + 1 && ! neg) return false;
	k = long(neg ? -u1 : u1);
	return true;
}

bool
alf::decimal::builder::get(long long & k) const
{
	int neg = 0;

	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: k = 0; return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) { k = 0; return true; }
		qn -= u9;
		q += u9;
	}
	unsigned long long int u1 = 0;
	unsigned long long int u2 = 0;
	unsigned long long int u3 = 0;
	unsigned long long int v = 0;
	if (qq > q) u1 = *--qq;
	if (qq > q) { u2 = u1; u1 = *--qq; }
	if (qq > q) { u3 = u2; u2 = u1; u1 = *--qq; }
	if (qq > q) return false;
	if (u9mod) {
		unsigned long long int w = dtens[u9mod];
		u2 += (u3 % w)*R;
		u3 /= w;
		u1 += (u2 % w)*R;
		u2 /= w;
		u1 /= w;
	}
	// number == (u3 * R + u2)*R + u1
	// we want to compute it as one large number in u1
	// and overflow bits in u3.
	u2 += u3*R;
	// u is now u2 * R + u1 but u2 may be so large
	// that u2 * R overflows
	// we therefore split u2 into (u3 << 32) | u2
	// so that u3 have the high bits of u2 and u2
	// only the low bits.
	u3 = u2 >> 32;
	u2 &= 0xffffffff;
	// u is now (u3 << 32 | u2)
	// and we can multiply u3:u2 by R.
	u2 = u2 * R + u1;
	u1 = u2 & 0xffffffff;
	u2 >>= 32;
	u2 = u3 * R + u2;
	// now u1 have the lower 32 bits and u2 have
	// the bits 32..95, we want the higher bits of this
	// to be all 0 and we only want bit 31 set if the lower
	// bits and u1 are also all 0.
	if (u2 > 0x80000000) return false;
	u1 = (u2 << 32) | u1;
	if (u1 > -dbl_digit(LLONG_MIN)) return false;
	if (u1 == -dbl_digit(LLONG_MIN) && ! neg) return false;
	k = (long long)(neg ? -u1 : u1);
	return true;
}

bool
alf::decimal::builder::get(unsigned char & k) const
{
	int neg = 0;
	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: k = 0; return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) { k = 0; return true; }
		qn -= u9;
		q += u9;
	}
	unsigned long long int u = 0;
	if (qq > q) u = *--qq;
	if (qq > q) u = u * R + *--qq;
	if (qq > q) return false;
	if (u9mod)
		u = u / dtens[u9mod];
	if (u && neg) return false;
	if (u > 255) return false;
	k = (unsigned char)u;
	return true;
}

bool
alf::decimal::builder::get(unsigned short & k) const
{
	int neg = 0;
	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: k = 0; return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) { k = 0; return true; }
		qn -= u9;
		q += u9;
	}
	unsigned long long int u = 0;
	if (qq > q) u = *--qq;
	if (qq > q) u = u * R + *--qq;
	if (qq > q) return false;
	if (u9mod)
		u = u / dtens[u9mod];
	if (u && neg) return false;
	if (u > 65535) return false;
	k = (unsigned short)u;
	return true;
}

bool
alf::decimal::builder::get(unsigned int & k) const
{
	int neg = 0;
	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: k = 0; return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) { k = 0; return true; }
		qn -= u9;
		q += u9;
	}
	unsigned long long int u = 0;
	if (qq > q) u = *--qq;
	if (qq > q) u = u * R + *--qq;
	if (qq > q) return false;
	if (u9mod)
		u = u / dtens[u9mod];
	if (u && neg) return false;
	if (u > UINT_MAX) return false;
	k = (unsigned int)u;
	return true;
}

bool
alf::decimal::builder::get(unsigned long & k) const
{
	int neg = 0;

	switch (klass) {
	case SNAN: case QNAN: case PINF: case NINF: return false;
	case PZERO: case NZERO: k = 0; return true;
	case NREGULAR: neg = 1; break;
	}

	const digit * q = p;
	const digit * qq = p + n;
	while (qq[-1] == 0) --qq;
	size_t qn = qq - p;
	size_t u9mod = 0;
	if (x < 0) {
		size_t u = -x;
		size_t u9 = u / N;
		u9mod = u % N;
		if (u9mod) ++u9;
		if (qn < u9) { k = 0; return true; }
		qn -= u9;
		q += u9;
	}
	unsigned long long int u1 = 0;
	unsigned long long int u2 = 0;
	unsigned long long int u3 = 0;
	unsigned long long int v = 0;
	if (qq > q) u1 = *--qq;
	if (qq > q) { u2 = u1; u1 = *--qq; }
	if (qq > q) { u3 = u2; u2 = u1; u1 = *--qq; }
	if (qq > q) return false;
	if (u9mod) {
		unsigned long long int w = dtens[u9mod];
		u2 += (u3 % w)*R;
		u3 /= w;
		u1 += (u2 % w)*R;
		u2 /= w;
		u1 /= w;
	}
	// number == (u3 * R + u2)*R + u1
	// we want to compute it as one large number in u1
	// and overflow bits in u3.
	u2 += u3*R;
	// u is now u2 * R + u1 but u2 may be so large
	// that u2 * R overflows
	// we therefore split u2 into (u3 << 32) | u2
	// so that u3 have the high bits of u2 and u2
	// only the low bits.
	u3 = u2 >> 32;
	u2 &= 0xffffffff;
	// u is now (u3 << 32 | u2)
	// and we can multiply u3:u2 by R.
	u2 = u2 * R + u1;
	u1 = u2 & 0xffffffff;
	u2 >>= 32;
	u2 = u3 * R + u2;
	// now u1 have the lower 32 bits and u2 have
	// the bits 32..95, we want the higher bits (64..95) of
    // this to be all 0.
	if (u2 >= 0x100000000) return false;
	u1 = (u2 << 32) | u1;
	if (u1 <= ULONG_MAX) {
	   k = long(neg ? -u1 : u1);
       return true;
    }
	return false;
}

bool
alf::decimal::builder::get(unsigned long long int & k) const
{
    int neg = 0;

    switch (klass) {
    case SNAN: case QNAN: case PINF: case NINF: return false;
    case PZERO: case NZERO: k = 0; return true;
    case NREGULAR: neg = 1; break;
    }

    const digit * q = p;
    const digit * qq = p + n;
    while (qq[-1] == 0) --qq;
    size_t qn = qq - p;
    size_t u9mod = 0;
    if (x < 0) {
        size_t u = -x;
        size_t u9 = u / N;
        u9mod = u % N;
        if (u9mod) ++u9;
        if (qn < u9) { k = 0; return true; }
        qn -= u9;
        q += u9;
    }
    unsigned long long int u1 = 0;
    unsigned long long int u2 = 0;
    unsigned long long int u3 = 0;
    unsigned long long int v = 0;
    if (qq > q) u1 = *--qq;
    if (qq > q) { u2 = u1; u1 = *--qq; }
    if (qq > q) { u3 = u2; u2 = u1; u1 = *--qq; }
    if (qq > q) return false;
    if (u9mod) {
        unsigned long long int w = dtens[u9mod];
        u2 += (u3 % w)*R;
        u3 /= w;
        u1 += (u2 % w)*R;
        u2 /= w;
        u1 /= w;
    }
    // number == (u3 * R + u2)*R + u1
    // we want to compute it as one large number in u1
    // and overflow bits in u3.
    u2 += u3*R;
    // u is now u2 * R + u1 but u2 may be so large
    // that u2 * R overflows
    // we therefore split u2 into (u3 << 32) | u2
    // so that u3 have the high bits of u2 and u2
    // only the low bits.
    u3 = u2 >> 32;
    u2 &= 0xffffffff;
    // u is now (u3 << 32 | u2)
    // and we can multiply u3:u2 by R.
    u2 = u2 * R + u1;
    u1 = u2 & 0xffffffff;
    u2 >>= 32;
    u2 = u3 * R + u2;
    // now u1 have the lower 32 bits and u2 have
    // the bits 32..95, we want the higher bits of this
    // to be all 0.
    if (u2 >= 0x100000000) return false;
    u1 = (u2 << 32) | u1;
    if (u1 <= ULLONG_MAX) {
       k = long(neg ? -u1 : u1);
       return true;
    }
    return false;
}



bool
alf::decimal::builder::get(char & k) const
{
#if CHAR_MIN == 0
    unsigned char kk;
#else
    signed char kk;
#endif
    bool ret = get(kk);
    if (ret) k = (char)kk;
    return ret;
}

bool
alf::decimal::builder::get(bool & k) const
{
    int neg = 0;
    switch (klass) {
    case SNAN: case QNAN: case PINF: case NINF: return false;
    case PZERO: case NZERO: k = 0; return true;
    case NREGULAR: neg = 1; break;
    }

    const digit * q = p;
    const digit * qq = p + n;
    while (qq[-1] == 0) --qq;
    size_t qn = qq - p;
    size_t u9mod = 0;
    if (x < 0) {
        size_t u = -x;
        size_t u9 = u / N;
        u9mod = u % N;
        if (u9mod) ++u9;
        if (qn < u9) { k = 0; return true; }
        qn -= u9;
        q += u9;
    }
    unsigned long long int u = 0;
    if (qq > q) u = *--qq;
    if (qq > q) u = u * R + *--qq;
    if (qq > q) return false;
    if (u9mod)
        u = u / dtens[u9mod];
    if (u && neg) return false;
    if (u > 1) return false;
    k = u != 0;
    return true;
}

bool
alf::decimal::builder::get(float & k) const
{
    int neg = 0;
    int w;
    switch (klass) {
    case SNAN: w = 0x7f80ffff; k = *(float *) & w; return true;
    case QNAN: w = 0x7fc0ffff; k = *(float *) & w; return true;
    case PINF: w = 0x7f800000; k = *(float *) & w; return true;
    case NINF: w = 0xff800000; k = *(float *) & w; return true;
    case PZERO: w = 0x00000000; k = *(float *)& w; return true;
    case NEGZERO: w = 0x80000000; k=*(float *)& w; return true;
    case NREGULAR: neg = 1; break;
    }

    // a few values to define ranges.

    // Normal values are in the range from:
    // 2**(1-127) * 1.0 to 2**(254-127) * 1.1111..111
    // or 2**-126 * 1.0 to 2**127 * (2**24 - 1) * 2**-23
    // or 2**-126 to 2**104 * (2**24-1)
    // subnormal values are in the range from:
    // 2**(1-127) * 0.0 to 2**(1-127) * 0.1111...111
    // 2**-126 * 0 to 2**-149 * (2**23 - 1)
    // or 0 to 2**-149 * (2**23 - 1)

    // we first transform the number from the form
    // K * 10**x to M * 2**x by:
    // K * 10**x == K * 5**x * 2**x == (K *5**x) * 2**x.
    // so M == K * 5**x.
    // also since the final number only store 23 bits.
    // we only keep the upper 32 bits of the number.
    unsigned long long int u, v;
    size_t nn = normalize___(p, n);
    ssize_t xx = x;
    switch (nn) {
    case 0:
        w = neg ? 0x80000000 : 0;
        k = *(float *) & w;
        return true;
    }
    // so now the value is p[0..nn-1] * 10**xx.
    // we need to translate the number from p * 10**xx
    // to M * 2**xx
    // p * 10**xx == p * 5**xx * 2**xx
    // so we multiply p with 5**xx.
    // if xx is negative that means dividing by 5**-x.
    // or multiplying by 0.2**-x which is multiplying
    // by 2**-xx * 10**xx
    builder five(5);
    if (xx < 0) {
        five.assign(2);
        five.x = -1;
        xx = -xx;
    }
    if (xx > UINT_MAX) return false;
    five.pow((unsigned int)xx);
    builder mm, q;
    mm.mul(p, n, s, x, klass,
            five.p, five.n, five.s, five.x, five.klass);
    // the number is now mm * 2**x
    xx = mm.x;
    nn = mm.n;
    size_t j1 = nn, j2 = 0, j3 = 0;
    unsigned long long int RR = R;
    // mm is decimal using R, we need to turn it
    // binary using 1<<32 as base.
    // q possibly need less space than mm but who cares?
    q.ensure(nn);
    while (j1 > 0) {
        while (mm.p[j1-1] == 0) --j1;
        j2 = j1;
        u = 0;
        while (j2 > 0) {
            u = u * RR + mm.p[--j2];
            mm.p[j2] = digit(u >> 32);
            u &= 0xffffffff;
        }
        q.p[j3++] = digit(u);
    }
    while (j3 > 0 && q.p[j3-1] == 0) --j3;
    q.n = j3;
    if (j3 == 0) {
        w = neg ? 0x80000000 : 0;
        k = *(float *) & w;
        return true;
    }
    // Now q is a binary representation of the number mm.
    // so the number is q * 2**mm.x
    // if q.n > 2 drop the lower bits and increase x by
    // 32* the number of digits dropped.
    q.x = xx;
    if (j3 > 2) {
        j2 = j3 - 2;
        q.p[0] = q.p[j2];
        q.p[1] = q.p[j2+1];
        q.x = xx = xx + (j2 << 5);
    }
    // we want q to be two digits exactly so if it is less
    // than 2 we increase the digits to two and insert
    // a lower digit of 0 and decrease xx by 32.
    if (j3 < 2) {
        q.p[1] = q.p[0];
        q.p[0] = 0;
        q.x = xx = xx - 32;
    }
    // adjust q so that 2**23 <= q[1] < 2**24
    int lshft = 0, rshft = 0;
    digit q1 = q.p[1], q0 = q.p[0];
    if (q1 & 0xff000000) {
        // We need to shift down (left) q a bit to make it
        // in the specified range.
        q0 = q1;
        // need to find the most significant 1 bit in q0.
        // we know it is in bit 25..31.
        if (q0 & 0xf0000000) {
            lshft = 4;
            q0 >>= 4;
        }
        if (q0 & 0x0c000000) {
            lshft += 2;
            q0 >>= 2;
        }
        if (q0 & 0x02000000) {
            ++lshft;
            q0 >>= 1;
        }
        ++lshft;
        rshft = 32 - lshft;
        q1 = q.p[1]; q0 = q.p[0];
        q0 = (q0 >> lshft) | (q1 << rshft);
        q1 = q1 >> lshft;
        q.p[0] = q0;
        q.p[1] = q1;
        // adjust x accordingly.
        q.x = xx = xx + lshft;
    } else if ((q1 & 0x00800000) == 0) {
        // need to shift up (right) to get
        // q1 in the range 0x00800000 <= q1 <= 0x00ffffff
        q0 = q1;
        if ((q0 & 0x00fff000) == 0) {
            rshft = 12;
            q0 <<= 12;
        }
        if ((q0 & 0x00fc0000) == 0) {
            rshft += 6;
            q0 <<= 6;
        }
        if ((q0 & 0x00e00000) == 0) {
            rshft += 3;
            q0 <<= 3;
        }
        if ((q0 & 0x00c00000) == 0) {
            rshft += 2;
            q0 <<= 2;
        }
        if ((q0 & 0x00800000) == 0) {
            ++rshft;
            q0 <<= 1;
        }
        lshft = 32 - rshft;
        q1 = q.p[1]; q0 = q.p[0];
        q1 = (q1 << rshft) | (q0 >> lshft);
        q0 = q0 << rshft;
        q.p[1] = q1; q.p[0] = q0;
        q.x = xx = xx - rshft;
    }

    // Now, q1 == q.p[1] has the 24 most significant bits
    // of the number and q0 == q.p[0] has the next 32 bits.
    // and xx has the scaling and neg is non-zero if sign
    // is negative and 0 if positive.
    // check that xx is in range. -126 <= xx <= 127
    // for a normal number. Otherwise if xx is
    // -126-23 == -149 <= xx <= -127
    // the number is subnormal. If xx is < -149
    // we return ZERO. If xx >= 128 we return INF.
    if (xx < -149) {
        w = neg ? 0x80000000 : 0;
        k = *(float *) & w;
        return true;
    }
    if (xx >= 128) {
        w = neg ? 0xff800000 : 0x7f800000;
        k = *(float *) & w;
        return true;
    }
    if (xx < -126) {
        // subnormal....
        rshft = -126 - xx;
        q0 >>= rshft;
        xx = -127;
    }
    // remove hidden bit. (if subnormal this bit is 0 anyway)
    q0 &= ~0x800000;
    // -126 <= xx <= 127
    // add bias
    xx += 127;
    // 1 <= xx <= 254
    q0 |= (int(xx) << 23);
    if (neg) q0 |= 0x80000000;
    k = *(float *) & q0;
    return true;
}

bool
alf::decimal::builder::get(double & k) const
{
    enum {
        NEG = 0x800000000000000ULL,
        INF = 0x7ff000000000000ULL,
        Q   = 0x000800000000000ULL,
    };

    long long int neg = 0;
    long long int xxx = 0;
    long long int w;
    switch (klass) {
    case SNAN:
        w = INF | 0xffff;
        k = *(double *) & w;
        return true;
    case QNAN:
        w = INF | Q | 0xffff;
        k = *(double *) & w;
        return true;
    case PINF:
        w = INF;
        k = *(double *) & w;
        return true;
    case NINF:
        w = NEG | INF;
        k = *(double *) & w;
        return true;
    case PZERO:
        w = 0;
        k = *(double *)& w;
        return true;
    case NEGZERO:
        w = NEG;
        k = *(double *) & w;
        return true;
    case NREGULAR: neg = NEG; break;
    }

    // a few values to define ranges.

    // Normal values are in the range from:
    // 2**(1-1023) * 1.0 to 2**(2046-1023) * 1.11111..111
    // or 2**-1022 * 1.0 to 2**1023 * (2**53 - 1)*2**-52
    // or 2**-1022 to 2**971 * (2**53 - 1)

    // subnormal values are in the range from:
    // 2**(1-1023) * 0.0 to 2**(1-1023) * 0.1111...111
    // 2**-1022 * 0 to 2**-1022 * (2**52 - 1) * 2**-52
    // or 0 to 2**-1074 * (2**52 - 1)

    // we first transform the number from the form
    // K * 10**x to M * 2**x by:
    // K * 10**x == K * 5**x * 2**x == (K *5**x) * 2**x.
    // so M == K * 5**x.
    // also since the final number only store 52 bits.
    // we only keep the upper 64 bits of the number.
    unsigned long long int u, v;
    size_t nn = normalize___(p, n);
    ssize_t xx = x;
    switch (nn) {
    case 0:
        w = neg;
        k = *(double *) & w;
        return true;
    }
    // so now the value is p[0..nn-1] * 10**xx.
    // we need to translate the number from p * 10**xx
    // to M * 2**xx
    // p * 10**xx == p * 5**xx * 2**xx
    // so we multiply p with 5**xx.
    // if xx is negative that means dividing by 5**-x.
    // or multiplying by 0.2**-x which is multiplying
    // by 2**-xx * 10**xx
    builder five(5);
    if (xx < 0) {
        five.assign(2);
        five.x = -1;
        xx = -xx;
    }
    if (xx > UINT_MAX) return false;
    five.pow((unsigned int)xx);
    builder mm, q;
    mm.mul(p, n, s, x, klass,
            five.p, five.n, five.s, five.x, five.klass);
    // the number is now mm * 2**x
    xx = mm.x;
    nn = mm.n;
    size_t j1 = nn, j2 = 0, j3 = 0;
    unsigned long long int RR = R;
    // mm is decimal using R, we need to turn it
    // binary using 1<<32 as base.
    // q possibly need less space than mm but who cares?
    q.ensure(nn);
    while (j1 > 0) {
        while (mm.p[j1-1] == 0) --j1;
        j2 = j1;
        u = 0;
        while (j2 > 0) {
            u = u * RR + mm.p[--j2];
            mm.p[j2] = digit(u >> 32);
            u &= 0xffffffff;
        }
        q.p[j3++] = digit(u);
    }
    while (j3 > 0 && q.p[j3-1] == 0) --j3;
    q.n = j3;
    if (j3 == 0) {
        w = neg ? 0x80000000 : 0;
        k = *(float *) & w;
        return true;
    }
    // Now q is a binary representation of the number mm.
    // so the number is q * 2**mm.x
    // if q.n > 3 drop the lower bits and increase x by
    // 32* the number of digits dropped.
    q.x = xx;
    if (j3 > 3) {
        j2 = j3 - 3;
        q.p[0] = q.p[j2];
        q.p[1] = q.p[j2+1];
        q.p[2] = q.p[j2+2];
        q.x = xx = xx + (j2 << 5);
    }
    // we want q to be three digits exactly so if it is less
    // than 3 we increase the digits to three and insert
    // one or two lower digits of 0 and decrease xx by 32 or
    // 64 accordingly.
    switch (j3) {
    case 3: break;
    case 2:
        q.p[2] = q.p[1];
        q.p[1] = q.p[0];
        q.p[0] = 0;
        q.x = xx = xx - 32;
        break;
    case 1:
        q.p[2] = q.p[0];
        q.p[1] = q.p[0] = 0;
        q.x = xx = xx - 64;
        break;
    }
    // we want the number to have 1 in bit 52 as
    // the most significant bit - regarding q.p[1]
    // and q.p[2] as one 63 bit number.
    // for q.p[2] this means bit 52-32 = 1 in bit 20.
    // adjust q so that 2**20 <= q[1] < 2**21
    int lshft = 0, rshft = 0;
    digit q2 = q.p[2], q1 = q.p[1], q0 = q.p[0], qz;
    if ((qz = q2) & 0xffe00000) {
        // We need to shift down (left) q a bit to make it
        // in the specified range.
        // need to find the most significant 1 bit in q1.
        // we know it is in bit 22..31.
        if (qz & 0xf0000000) {
            lshft = 4;
            qz >>= 4;
        }
        if (qz & 0x0f000000) {
            lshft += 4;
            qz >>= 4;
        }
        if (qz & 0x00c00000) {
            lshft += 2;
            qz >>= 2;
        }
        if (qz & 0x00200000) {
            ++lshft;
            q0 >>= 1;
        }
        rshft = 32 - lshft;
        q0 = (q0 >> lshft) | (q1 << rshft);
        q1 = (q1 >> lshft) | (q2 << rshft);
        q2 = q2 >> lshft;
        q.p[0] = q0;
        q.p[1] = q1;
        q.p[2] = q2;
        // adjust x accordingly.
        q.x = xx = xx + lshft;
    } else if ((qz & 0x00100000) == 0) {
        // need to shift up (right) to get
        // q1 in the range 0x00100000 <= q1 <= 0x001fffff
        if ((qz & 0x000ffc00) == 0) {
            rshft = 10;
            q0 <<= 10;
        }
        if ((q0 & 0x00f80000) == 0) {
            rshft += 5;
            q0 <<= 5;
        }
        if ((q0 & 0x00e00000) == 0) {
            rshft += 3;
            q0 <<= 3;
        }
        if ((q0 & 0x00c00000) == 0) {
            rshft += 2;
            q0 <<= 2;
        }
        if ((q0 & 0x00800000) == 0) {
            ++rshft;
            q0 <<= 1;
        }
        lshft = 32 - ++rshft;
        q2 = (q2 << rshft) | (q1 >> lshft);
        q1 = (q1 << rshft) | (q0 >> lshft);
        q0 = q0 << rshft;
        q.p[2] = q2; q.p[1] = q1; q.p[0] = q0;
        q.x = xx = xx - rshft;
    }

    // Now, q2 == q.p[2] has the 21 most significant bits
    // of the number and q1 == q.p[1] has the next 32 bits
    // and q0 == q.p[0] has the next 32 bits
    // and xx has the scaling and neg is non-zero if sign
    // is negative and 0 if positive.

    // check that xx is in range. -1022 <= xx <= 1023
    // for a normal number. Otherwise if xx is
    // -1022-52 == -1074 <= xx <= -1023
    // the number is subnormal. If xx is < -1074
    // we return ZERO. If xx >= 1023 we return INF.
    if (xx < -1074) {
        k = *(double *) & neg;
        return true;
    }
    if (xx >= 1024) {
        w = neg | INF;
        k = *(double *) & w;
        return true;
    }
    if (xx < -1022) {
        // subnormal....
        rshft = -1022 - xx;
        if (rshft > 32) {
            // shift is more than 32, so q2 should become
            // 0 while q1 should become q2 >> (rshft - 32)
            rshft -= 32;
            lshft = 32 - rshft;
            q0 = (q1 >>rshft) | (q2 << lshft);
            q1 = q2 >>rshft;
            q2 = 0;
        } else if (rshft < 32) {
            // shift is less than 32.
            lshft = 32 - rshft;
            q0 = (q0 >> rshft) | (q1 << lshft);
            q1 = (q1 >> rshft) | (q2 << lshft);
            q2 = q2 >> rshft;
        } else { // shift is exactly 32.
            q0 = q1; q1 = q2; q2 = 0;
        }
        xx = -1023;
    }
    // remove hidden bit, if value is subnormal this
    // bit would be 0 already due to the shift.
    q2 &= ~0x00100000;
    // -1022 <= xx <= 1023
    // add bias
    xx += 1023;
    // 1 <= xx <= 2046
    q2 |= (int(xx) << 20);
    w = (((long long int)q2) << 32) | q1;
    w |= neg;
    k = *(double *)w;
    return true;
}

// this is written under assumption that long double
// is 80 bit extended format for IBM PC. If long double
// is ieee 754 binary128 format instead some other
// function needs to be written.
bool
alf::decimal::builder::get(long double & k) const
{
    enum {
        NEG = 0x8000,
        INF = 0x7fff,
    };
    enum {
        MNAN = 0x800000000000ffffULL,
        MINF = 0x8000000000000000ULL,
        Q    = 0x4000000000000000ULL,
    };

    struct ldbl_s {
        unsigned long long int m;
        short x;
        char notused[6];
    };
    union ldbl_u {
        ldbl_s s;
        long double x;
    };
    volatile ldbl_u zz;
    short neg = 0;
    short xxx = 0;

    switch (klass) {
    case SNAN:
        zz.s.m = MNAN;
        zz.s.x = INF;
        k = zz.x;
        return true;
    case QNAN:
        zz.s.m = Q | MNAN;
        zz.s.x = INF;
        k = zz.x;
        return true;
    case PINF:
        zz.s.m = MINF;
        zz.s.x = INF;
        k = zz.x;
        return true;
    case NINF:
        zz.s.m = MINF;
        zz.s.x = NEG | INF;
        k = zz.x;
        return true;
    case PZERO:
        zz.s.m = 0;
        zz.s.x = 0;
        k = zz.x;
        return true;
    case NEGZERO:
        zz.s.m = 0;
        zz.s.x = NEG;
        k = zz.x;
        return true;
    case NREGULAR: neg = NEG; break;
    }

    // a few values to define ranges.

    // Normal values are in the range from:
    // 2**(1-16383) * 1.0 to 2**(32766-16383) * 1.1111..111
    // or 2**-16382 * 1.0 to 2**16383 * (2**64 - 1)*2**-63
    // or 2**-16382 to 2**16320 * (2**64 - 1)

    // subnormal values are in the range from:
    // 2**(1-16383) * 0.0 to 2**(1-16383) * 0.1111...111
    // 2**-16382 * 0 to 2**-16382 * (2**63 - 1) * 2**-63
    // or 0 to 2**-16445 * (2**63 - 1)

    // we first transform the number from the form
    // K * 10**x to M * 2**x by:
    // K * 10**x == K * 5**x * 2**x == (K *5**x) * 2**x.
    // so M == K * 5**x.
    // also since the final number only store 63 bits.
    // we only keep the upper 64 bits of the number.
    unsigned long long int u, v;
    size_t nn = normalize___(p, n);
    ssize_t xx = x;
    switch (nn) {
    case 0:
        zz.s.x = neg;
        zz.s.m = 0;
        k = zz.x;
        return true;
    }
    // so now the value is p[0..nn-1] * 10**xx.
    // we need to translate the number from p * 10**xx
    // to M * 2**xx
    // p * 10**xx == p * 5**xx * 2**xx
    // so we multiply p with 5**xx.
    // if xx is negative that means dividing by 5**-x.
    // or multiplying by 0.2**-x which is multiplying
    // by 2**-xx * 10**xx
    builder five(5);
    if (xx < 0) {
        five.assign(2);
        five.x = -1;
        xx = -xx;
    }
    if (xx > UINT_MAX) return false;
    five.pow((unsigned int)xx);
    builder mm, q;
    mm.mul(p, n, s, x, klass,
            five.p, five.n, five.s, five.x, five.klass);
    // the number is now mm * 2**x
    xx = mm.x;
    nn = mm.n;
    size_t j1 = nn, j2 = 0, j3 = 0;
    unsigned long long int RR = R;
    // mm is decimal using R, we need to turn it
    // binary using 1<<32 as base.
    // q possibly need less space than mm but who cares?
    q.ensure(nn);
    while (j1 > 0) {
        while (mm.p[j1-1] == 0) --j1;
        j2 = j1;
        u = 0;
        while (j2 > 0) {
            u = u * RR + mm.p[--j2];
            mm.p[j2] = digit(u >> 32);
            u &= 0xffffffff;
        }
        q.p[j3++] = digit(u);
    }
    while (j3 > 0 && q.p[j3-1] == 0) --j3;
    q.n = j3;
    if (j3 == 0) {
        zz.s.m = 0;
        zz.s.x = neg;
        k = zz.x;
        return true;
    }
    // Now q is a binary representation of the number mm.
    // so the number is q * 2**mm.x
    // if q.n > 3 drop the lower bits and increase x by
    // 32* the number of digits dropped.
    q.x = xx;
    if (j3 > 3) {
        j2 = j3 - 3;
        q.p[0] = q.p[j2];
        q.p[1] = q.p[j2+1];
        q.p[2] = q.p[j2+2];
        q.x = xx = xx + (j2 << 5);
    }
    // we want q to be three digits exactly so if it is less
    // than 3 we increase the digits to three and insert
    // one or two lower digits of 0 and decrease xx by 32 or
    // 64 accordingly.
    switch (j3) {
    case 3: break;
    case 2:
        q.p[2] = q.p[1];
        q.p[1] = q.p[0];
        q.p[0] = 0;
        q.x = xx = xx - 32;
        break;
    case 1:
        q.p[2] = q.p[0];
        q.p[1] = q.p[0] = 0;
        q.x = xx = xx - 64;
        break;
    }
    // we want the number to have 1 in bit 63 as
    // the most significant bit - regarding q.p[1]
    // and q.p[2] as one 63 bit number.
    // for q.p[2] this means bit 63-32 = 1 in bit 31.
    // adjust q so that 2**31 <= q[1] < 2**32
    int lshft = 0, rshft = 0;
    digit q2 = q.p[2], q1 = q.p[1], q0 = q.p[0], qz;
    if (((qz = q2) & 0xffff0000) == 0) {
        lshft = 16;
        qz <<= 16;
    }
    if ((qz & 0xff000000) == 0) {
        lshft += 8;
        qz <<= 8;
    }
    if ((qz & 0xf0000000) == 0) {
        lshft += 4;
        qz <<= 4;
    }
    if ((qz & 0xc0000000) == 0) {
        lshft += 2;
        qz <<= 2;
    }
    if ((qz & 0x80000000) == 0) {
        ++lshft;
        qz <<= 1;
    }
    rshft = 32 - lshft;
    q2 = (q2 << lshft) | (q1 >> rshft);
    q1 = (q1 << lshft) | (q0 >> rshft);
    q0 = q0 << lshft;
    q.p[2] = q2; q.p[1] = q1; q.p[0] = q0;
    q.x = xx = xx - lshft;

    // Now, q2 == q.p[2] has the 32 most significant bits
    // of the number and q1 == q.p[1] has the next 32 bits
    // and q0 == q.p[0] has the next 32 bits
    // and xx has the scaling and neg is non-zero if sign
    // is negative and 0 if positive.

    // check that xx is in range. -16382 <= xx <= 16383
    // for a normal number. Otherwise if xx is
    // -16382-63 == -16445 <= xx <= -16383
    // the number is subnormal. If xx is < -16445
    // we return ZERO. If xx >= 16384 we return INF.
    if (xx < -16445) {
        zz.s.m = 0;
        zz.s.x = neg;
        k = zz.x;
        return true;
    }
    if (xx >= 16384) {
        zz.s.m = MINF;
        zz.s.x = neg | INF;
        k = zz.x;
        return true;
    }
    if (xx < -16382) {
        // subnormal....
        rshft = -16382 - xx;
        if (rshft > 32) {
            // shift is more than 32, so q2 should become
            // 0 while q1 should become q2 >> (rshft - 32)
            rshft -= 32;
            lshft = 32 - rshft;
            q0 = (q1 >>rshft) | (q2 << lshft);
            q1 = q2 >>rshft;
            q2 = 0;
        } else if (rshft < 32) {
            // shift is less than 32.
            lshft = 32 - rshft;
            q0 = (q0 >> rshft) | (q1 << lshft);
            q1 = (q1 >> rshft) | (q2 << lshft);
            q2 = q2 >> rshft;
        } else { // shift is exactly 32.
            q0 = q1; q1 = q2; q2 = 0;
        }
        xx = -16383;
    }
    // 80 bit extended have no hidden bit.

    // -16382 <= xx <= 16383
    // add bias
    xx += 16383;
    // 0 <= xx <= 2046
    unsigned long long int u1 = q2;
    u1 = (u1 << 32) | q1;
    zz.s.m = u1;
    zz.s.x = short(xx | neg);
    k = zz.x;
    return true;
}

alf::decimal::digit
alf::decimal::builder::add_(const digit * ap, size_t an)
{
    length_at_least(an);
    size_t j = 0;
    digit w = 0;
    while (j < an) {
        w += p[j] + ap[j];
        if (w >= R) {
            p[j++] = w - R;
            w = 1;
        } else {
            p[j++] = w;
            w = 0;
        }
    }
    while (j < n && w) {
        w += p[j];
        if (w == R) {
            p[j++] = 0;
            w = 1;
        } else {
            p[j++] = w;
            w = 0;
            j = n;
            break;
        }
    }
    if (w != 0) {
        ensure(1);
        p[n++] = 1;
    }
    return 0;
}

// static
void
alf::decimal::builder::add_(digit * cp, size_t cn,
                                const digit * ap, size_t an,
                                const digit * bp, size_t bn)
{
    digit w = 0;
    size_t j = 0;
    if (an < bn) {
        const digit * tmp = ap; ap = bp; bp = tmp;
        j = an; an = bn; bn = j; j = 0;
    }
    // an >= bn.
    while (j < bn) {
        w = w + ap[j] + bp[j];
        if (w >= R) {
            cp[j++] = w - R;
            w = 1;
        } else {
            cp[j++] = w;
            w = 0;
        }
    }
    if (cp != ap) {
        while (j < an) {
            w = w + ap[j];
            if (w >= R) {
                cp[j++] = w - R;
                w = 1;
            } else {
                cp[j++] = w;
                w = 0;
            }
        }
        if (j < cn && w != 0)
            cp[j++] = w;
    } else {
        while (j < an && w != 0) {
            if (++cp[j] < R) break;
            cp[j++] = 0;
        }
    }
}

// static
void
alf::decimal::builder::sub_(digit * cp, size_t cn,
                                const digit * ap, size_t an,
                                const digit * bp, size_t bn)
{
    enum { NINE = R - 1, };

    // we freak out if an < bn.
    if (an < bn) throw "an < bn in sub_";
    digit w = 1;
    size_t j = 0;
    while (j < bn) {
        w = w + NINE + ap[j] - bp[j];
        if (w >= R) {
            cp[j++] = w - R;
            w = 1;
        } else {
            cp[j++] = w;
            w = 0;
        }
    }
    while (j < an) {
        w = w + NINE + ap[j];
        if (w >= R) {
            cp[j++] = w - R;
            w = 1;
        } else {
            cp[j++] = w;
            w = 0;
        }
    }
    w = w ? 0 : NINE;
    while (j < cn)
        cp[j++] = w;
    if (w) throw "w is non-zero in sub_";
}


alf::decimal::builder &
alf::decimal::builder::add_(const digit * ap, size_t an, ssize_t as, ssize_t ax,
                                const digit * bp, size_t bn, ssize_t bs, ssize_t bx,
                                int neg)
{
    // we adjust the one with higher exponent
    // so that it is equal to the lower and then multiply
    // the difference.
    if (ax < bx) {
        assign(bp, bn, bn, bx, PREGULAR);
        exp_at_most(ax);
        add_(ap, an);
    } else {
        assign(ap, an, an, ax, 0);
        exp_at_most(bx);
        add_(bp, bn);
    }
    s = 1;
    normalize_();
    s = neg ? -n : n;
    klass = n == 0 ? PZERO : neg ? NREGULAR : PREGULAR;
    return *this;
}

alf::decimal::digit
alf::decimal::builder::sub_(const digit * ap, size_t an)
{
    enum { NINE = R - 1, };

    length_at_least(an);
    size_t j = 0;
    digit w = 1;
    while (j < an) {
        w += NINE + p[j] - ap[j];
        if (w >= R) {
            p[j++] = w - R;
            w = 1;
        } else {
            p[j++] = w;
            w = 0;
        }
    }
    while (j < n) {
        w += NINE + p[j];
        if (w >= R) {
            p[j++] = w - R;
            w = 1;
        } else {
            p[j++] = w;
            w = 0;
        }
    }
    return 1 - w;
}

alf::decimal::builder &
alf::decimal::builder::sub_(const digit * ap, size_t an, ssize_t as, ssize_t ax,
                                const digit * bp, size_t bn, ssize_t bs, ssize_t bx,
                                int neg)
{
    // we adjust the one with higher exponent
    // so that it is equal to the lower and then multiply
    // the difference.
    if (ax < bx) {
        assign(bp, bn, bn, bx, PREGULAR);
        exp_at_most(ax);
        // since we do b - a but we want a - b
        // we switch neg.
        neg = !neg;
        if (sub_(ap, an))
            neg = !neg;
    } else {
        assign(ap, an, an, ax, 0);
        exp_at_most(bx);
        if (sub_(bp, bn))
            neg = !neg;
    }
    s = 1;
    normalize_();
    s = neg ? -n : n;
    klass = n == 0 ? PZERO : neg ? NREGULAR : PREGULAR;
    return *this;
}

// This function does NOT negate the number. Instead it regards the number
// as a 2-complement value and negate the bits of the value. It then
// also negates the number.
alf::decimal::builder &
alf::decimal::builder::neg_()
{
    size_t j = 0;
    while (j < n && p[j] == 0) ++j;
    if (j < n) {
        p[j] = -p[j];
        while (++j < n) p[j] = ~p[j];
    }
    s = -s;
    return *this;
}

// remove leading zeroes.
// we do not remove low 0's as that would change the accuracy of the number.
// Note that if number is negative, we keep one 0 digit so that we can
// remember -0 as s == -1 and n == 1 and *p == 0.
alf::decimal::builder &
alf::decimal::builder::normalize()
{
	if ((n = normalize___(p, n)) == 0)
		p[n++] = 0;
    s = s >= 0 ? n : -n;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::add(const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls,
                            const digit * bp, size_t bn,
                            ssize_t bs, ssize_t bx, int bkls)
{
    if (akls == SNAN || akls == QNAN || bkls == SNAN || bkls == QNAN) {
        klass = QNAN;
        return *this;
    }
    if (akls == PINF) {
        if (bkls == NINF) return snan_();
        klass = PINF;
        return *this;
    }
    if (akls == NINF) {
        if (bkls == PINF) return snan_();
        klass = NINF;
        return *this;
    }
    if (bkls == PINF || bkls == NINF) {
        klass = bkls;
        return *this;
    }
    // neither a nor b is NAN or INF.
    if (as >= 0) {
        if (bs >= 0)
            return add_(ap, an, as, ax, bp, bn, bs, bx, 0);
        return sub_(ap, an, as, ax, bp, bn, bs, bx, 0);
    }
    if (bs >= 0)
        return sub_(bp, bn, bs, bx, ap, an, as, ax, 0);
    return add_(ap, an, as, ax, bp, bn, bs, bx, 1);
}

alf::decimal::builder &
alf::decimal::builder::sub(const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls,
                            const digit * bp, size_t bn,
                            ssize_t bs, ssize_t bx, int bkls)
{
    if (akls == SNAN || akls == QNAN || bkls == SNAN || bkls == QNAN) {
        klass = QNAN;
        return *this;
    }
    if (akls == PINF) {
        if (bkls == PINF) return snan_();
        klass = PINF;
        return *this;
    }
    if (akls == NINF) {
        if (bkls == NINF) return snan_();
        klass = NINF;
        return *this;
    }
    switch (bkls) {
    case PINF: klass = NINF; return *this;
    case NINF: klass = PINF; return *this;
    }
    // neither a nor b is NAN or INF.
    if (as >= 0) {
        if (bs >= 0)
            return sub_(ap, an, as, ax, bp, bn, bs, bx, 0);
        return add_(ap, an, as, ax, bp, bn, bs, bx, 0);
    }
    if (bs >= 0)
        return add_(bp, bn, bs, bx, ap, an, as, ax, 1);
    return sub_(bp, bn, bs, bx, ap, an, as, ax, 0);
}

bool
alf::decimal::builder::isneg() const
{
    switch (klass) {
    case NINF:
    case NEGZERO:
    case NREGULAR: return true;
    }
    return false;
}

bool alf::decimal::builder::ispos() const
{
    switch (klass) {
    case PINF:
    case PZERO:
    case PREGULAR: return true;
    }
    return false;
}

bool alf::decimal::builder::iszero() const
{
    switch (klass) {
    case PZERO:
    case NEGZERO: return true;
    }
    return false;
}

bool alf::decimal::builder::isinf() const
{
    switch (klass) {
    case PINF:
    case NINF: return true;
    }
    return false;
}

bool alf::decimal::builder::isnan() const
{
    switch (klass) {
    case SNAN:
    case QNAN: return true;
    }
    return false;
}

alf::decimal::builder &
alf::decimal::builder::neg()
{
    switch (klass) {
    case SNAN:
    case QNAN: return *this;
    case PINF: klass = NINF; s = -s; return *this;
    case NINF: klass = PINF; s = -s; return *this;
    case PZERO: klass = NEGZERO; s = -s; return *this;
    case NEGZERO: klass = PZERO; s = -s; return *this;
    case PREGULAR: klass = NREGULAR; s = -s; return *this;
    case NREGULAR: klass = PREGULAR; s = -s; return *this;
    }
    s = -s;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::abs()
{
    switch (klass) {
    case SNAN:
    case QNAN:
    case PINF:
    case PZERO:
    case PREGULAR: return *this;
    case NINF: klass = PINF; s = -s; return *this;
    case NEGZERO: klass = PZERO; s = -s; return *this;
    case NREGULAR: klass = PREGULAR; s = -s; return *this;
    }
    s = -s;
    return *this;
}

// *this = *this * f + z;
alf::decimal::builder &
alf::decimal::builder::mul_(digit f, digit z)
{
    dbl_digit w = z;
    dbl_digit ff = f;
    size_t j = 0;
    while (j < n) {
        w += ff*p[j];
        p[j++] = digit(w % R);
        w /= R;
    }
    if (w) {
        ensure(1);
        p[n++] = digit(w);
    }
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::muladd_(size_t k, const digit * ap, size_t an, digit b)
{
    dbl_digit w = 0;
    dbl_digit bb = b;
    size_t j = 0;
    digit * q = p + k;
    while (j < an) {
        w += bb*ap[j] + q[j];
        q[j++] = digit(w % R);
        w /= R;
    }
    j += k;
    while (j < n && w != 0) {
        w += p[j];
        if (w >= R) {
            p[j++] = digit(w - R);
            w = 1;
        } else {
            p[j++] = digit(w);
            w = 0;
            break;
        }
    }
    if (w) {
        ensure(1);
        p[n++] = digit(w);
    }
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::muladd_(size_t k, const digit * ap, size_t an,
                                const digit * bp, size_t bn)
{
    if (an == 0 || bn == 0) return *this;
    if (an == 1) return muladd_(k, bp, bn, *ap);
    if (bn == 1) return muladd_(k, ap, an, *bp);
    if (an > bn) {
        // switch a and b
        const digit * ppp = ap; ap = bp; bp = ppp;
        size_t j = an; an = bn; bn = j;
    }
    // an <= bn
    if (an + an > bn) {
        if (an >= 70) return k_muladd_(k, ap, an, bp, bn);
    } else {
        // an <= bn/2.
        return q_muladd_(k, bp, bn, ap, an);
    }
    return s_muladd_(k, ap, an, bp, bn);
}

alf::decimal::builder &
alf::decimal::builder::k_muladd_(size_t j, const digit * ap, size_t an,
                                    const digit * bp, size_t bn)
{
    builder albl, ahbh, Q;
    int sign = 1;

    size_t k = bn >> 1;
    size_t k2 = k + k;
    size_t k2j = k2 + j;
    size_t jk = j + k;
    size_t ank = an - k;
    size_t bnk = bn - k;
    // (al - ah)
    albl.sub(ap, k, k, 0, PREGULAR, ap + k, ank, ank, 0, PREGULAR);
    if (albl.isneg()) { sign = -sign; albl.neg(); }
    // (bh - bl)
    ahbh.sub(bp + k, bnk, bnk, 0, PREGULAR, bp, k, k, 0, PREGULAR);
    if (ahbh.isneg()) { sign = -sign; ahbh.neg(); }
    // (al - ah)(bh - bl) = albh + ahbl - albl - ahbh
    Q.mul_(albl.p, albl.n, ahbh.p, ahbh.n, 0);
    albl.clear().mul_(ap, k, bp, k, 0);
    ahbh.clear().mul_(ap + k, ank, bp + k, bnk, 0);

    // a*b == (ah Z + al)(bh Z + bl) == ahbh ZZ + albl + (ahbl + albh)Z
    //     == ahbh ZZ + albl + ( (al-ah)(bh-bl) - albl - ahbh)Z
    add_(p + k2j, n - k2j, p + k2j, n - k2j, ahbh.p, ahbh.n);
    add_(p + j, k2, p + j, k2, albl.p, albl.n);
    if (!sign)
        add_(p + jk, n - jk, p + jk, n - jk, Q.p, Q.n);
    else
        sub_(p + jk, n - jk, p + jk, n - jk, Q.p, Q.n);
    sub_(p + jk, n - jk, p + jk, n - jk, ahbh.p, ahbh.n);
    sub_(p + jk, n - jk, p + jk, n - jk, albl.p, albl.n);
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::q_muladd_(size_t k,
                                    const digit * ap, size_t an,
                                    const digit * bp, size_t bn)
{
    // an is assumed to be much larger than bn.
    // specificaly it is more than double of bn.
    size_t j = 0;
    while (j + bn <= an) {
        muladd_(k + j, ap + j, bn, bp, bn);
        j += bn;
    }
    if (j < an)
        muladd_(k + j, ap + j, an - j, bp, bn);
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::s_muladd_(size_t k, const digit * ap, size_t an,
                                    const digit * bp, size_t bn)
{
    size_t ak = an >> 1;
    size_t bk = bn >> 1;
    muladd_(k, ap, ak, bp, bk);
    muladd_(k + ak, ap + ak, an - ak, bp, bk);
    muladd_(k + bk, ap, ak, bp + bk, bn - bk);
    muladd_(k + ak + bk, ap + ak, an - ak, bp + bk, bn - bk);
}

alf::decimal::builder &
alf::decimal::builder::mul_(const digit * ap, size_t an,
                                const digit * bp, size_t bn,
                                int neg)
{
    size_t cn = an + bn;
    size_t j;

    n = 0;
    if (an == 0 || bn == 0) {
        x = s = 0;
        return *this;
    }
    x = 0;
    ensure(cn);
    memset(p, 0, cn*sizeof(digit));
    n = cn;
    muladd_(0, ap, an, bp, bn);
    n = integer::builder::normalize_(p, n);
    s = neg ? -n : n;
    if (n == 0)
        klass = neg ? NEGZERO : PZERO;
    else
        klass = neg ? NREGULAR : PREGULAR;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::mul_(const digit * ap, size_t an, ssize_t ax,
                                const digit * bp, size_t bn, ssize_t bx,
                                int neg)
{
    mul_(ap, an, bp, bn, neg);
    x = ax + bx;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::snan_()
{
    klass = SNAN;
    // TODO: Check if we should throw exception here.
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::mul(const digit * ap, size_t an,
                                ssize_t as, ssize_t ax, int akls,
                                digit b)
{
    dbl_digit w = 0;
    dbl_digit bb = b;
    size_t j = 0;
    int neg = 0;

    switch (klass = akls) {
    case SNAN:
    case QNAN: n = 0; s = x = 0; return *this;
    case PZERO: n = 1; s = 1; x = 0; *p = 0; return *this;
    case NEGZERO:  n = 1; s = -1; x = 0; *p = 0; return *this;
    case PINF:
        if (b == 0) return snan_();
        n = 0; s = x = 0; return *this;
    case NINF:
        if (b == 0) return snan_();
        n = 0; s = x = 0; return *this;
    case NREGULAR: neg = 1; break;
    }
    an = integer::builder::normalize_(ap, an);
    ensure(an + 1);
    while (j < an) {
        w += bb*ap[j];
        p[j] = digit(w % R);
        w /= R;
    }
    n = an;
    if (w) p[n++] = digit(w);
    x = ax;
    s = neg ? -n : n;
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::mul(const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls,
                            const digit * bp, size_t bn,
                            ssize_t bs, ssize_t bx, int bkls)
{
    if (akls == SNAN || akls == QNAN || bkls== SNAN || bkls == QNAN) {
        klass = QNAN;
        return *this;
    }
    switch (akls) {
    case PINF:
        switch (bkls) {
        case PZERO:
        case NEGZERO: return snan_();
        case NREGULAR:
        case NINF: klass = NINF; return *this;
        }
        klass = akls;
        return *this;
    case NINF:
        switch (bkls) {
        case PZERO:
        case NEGZERO: return snan_();
        case NREGULAR:
        case NINF: akls = PINF; break;
        }
        klass = akls;
        return *this;
    case PZERO:
        switch (bkls) {
        case PINF:
        case NINF: return snan_();
        case PZERO:
        case NEGZERO: klass = bkls; return *this;
        case PREGULAR: klass = akls; return *this;
        case NREGULAR: klass = NEGZERO; return *this;
        }
    case NEGZERO:
        switch (bkls) {
        case PINF:
        case NINF: return snan_();
        case PZERO: klass = akls; return *this;
        case NEGZERO: klass = PZERO; return *this;
        case PREGULAR: klass = akls; return *this;
        case NREGULAR: klass = PZERO; return *this;
        }
    case PREGULAR:
        switch (bkls) {
        case PINF:
        case NINF:
        case PZERO:
        case NEGZERO: klass = akls; return *this;
        case PREGULAR: return mul_(ap, an, ax, bp, bn, bx, 0);
        case NREGULAR: return mul_(ap, an, ax, bp, bn, bx, 1);
        }
    case NREGULAR:
        switch (bkls) {
        case PINF: klass = NINF; return *this;
        case NINF: klass = PINF; return *this;
        case PZERO: klass = NEGZERO; return *this;
        case NEGZERO: klass = PZERO; return *this;
        case PREGULAR: return mul_(ap, an, ax, bp, bn, bx, 1);
        case NREGULAR: return mul_(ap, an, ax, bp, bn, bx, 0);
        }
    }
}

alf::decimal::builder &
alf::decimal::builder::mulR(size_t j)
{
	ensure(j);
	memmove(p + j, p, n*sizeof(digit));
	memset(p, 0, j*sizeof(digit));
	n += j;
	return *this;
}

// *this = *this / f, return *this % f.
alf::decimal::digit
alf::decimal::builder::div_(digit f)
{
    dbl_digit w = 0;
    size_t j = n;
    while (j > 0) {
        w *= R;
        w += p[--j];
        p[j] = digit(w / f);
        w %= f;
    }
    return digit(w);
}

alf::decimal::builder &
alf::decimal::builder::div_(size_t prec, int rnd,
                                const digit * ap, size_t an,
                                digit b)
{
    dbl_digit w = 0;
    dbl_digit bb = b;
    dbl_digit bb2;

    // special version when bn == 1, easy div.
    size_t i = an; // Note an == cn in this case.
    size_t j = (prec + N - 1)/N + an;
    size_t cn = j;

    ensure(cn);

    while (i > 0) {
        w = w * R + ap[--i];
        p[--j] = digit(w / bb);
        w = w % bb;
    }
    // now, work on fraction, for each digit of
    // fraction we add, we decrease x by N.
    while (j > 0) {
        w = w * R;
        p[--j] = digit(w / bb);
        w = w % bb;
        x -= N;
    }
    n = cn;
    // round based on last remainder.
    // we support rounding the following rounding modes.
    int adjust = 0;

    switch (rnd) {
    case NEAREST_TIES_TO_EVEN:
        bb2 = bb >> 1;
        if (w > bb2) adjust = 1;
        else if (w < bb2) adjust = 0;
        else adjust = *p & 1;
        break;
    case NEAREST_TIES_AWAY_ZERO:
        adjust = (w >= bb2);
        break;
    case NEAREST_TIES_TO_ZERO:
        adjust = (w > bb2);
        break;
        // we assume positive value here, so it same as
        // TO_ZERO:
    case DOWN:
    case TO_ZERO: adjust = 0; break;
    case UP:
    case AWAY_ZERO: adjust = (w != 0); break;
    }
    if (adjust) {
        i = 0;
        digit u = 1;
        while (i < n && u) {
            u += p[i];
            if (u == R) {
                p[i++] = 0;
            } else {
                p[i++] = u;
                u = 0;
                break;
            }
        }
        if (u) {
            // rounding up lead to an additional digit!
            ensure(1);
            p[n++] = u;
        }
    }
    return *this;
}

// static
int alf::decimal::builder::cmp__(const digit * ap, size_t an,
                                    const digit * bp, size_t bn)
{
    an = integer::builder::normalize_(ap, an);
    bn = integer::builder::normalize_(bp, bn);
    int d = an - bn;
    if (d > 0) return 1;
    if (d < 0) return -1;
    ssize_t k = an;
    while (--k >= 0) {
        if ((d = ap[k] - bp[k]) > 0) return 1;
        else if (d < 0) return -1;
    }
    return 0;
}


alf::decimal::builder &
alf::decimal::builder::div_(size_t prec, int rnd,
                                const digit * ap, size_t an,
                                const digit * bp, size_t bn)
{
    enum { Rhalf = R >> 1 };

    builder a(ap, an, an, 0, PREGULAR);
    builder b(bp, bn, bn, 0, PREGULAR);
    size_t i, j, k, kk;
    dbl_digit w, A, B, AA, BB, AAx, qhatd;
    digit u, qhat;

    if (an + 1 < bn) {
        k = bn - an - 1;
        // we can multiply a by R**k and the value
        // should still be less than b.
        a.ensure(k);
        for (i = b.n, j = i + k; i > 0;)
            a.p[--j] = a.p[--i];
        memset(a.p, 0, k*sizeof(digit));
        an = a.n += k;
        x -= k*N;
    }
    // now an >= bn - 1.
    if (an < bn) {
        // estimate what power of 10 we need to
        // get a to overflow into bn.
        // We seek a value V that is a power of 10 between.
        // R/A <= V <= (10R - 1)/A
        // where A is the most significant digit of a
        // and R == 10**9.
        AA = A = a.p[a.n-1];
        BB = dbl_digit(b.p[b.n-1])*R + b.p[b.n-2];
        for (i = 0; ; ) {
            if (i > N) {
                fprintf( stderr, "i > N - shouldn't get that high.\n");
                throw "i > N and it shouldn't be";
            }
            u = stens[i];
            AAx = AA * u;
            if (AAx >= BB) break;
            ++i;
        }
        a.mul_(u,0);
        x -= i;
        an = a.n;
    }
    // an >= bn at this point.
    // it is still possible that a < b
    // but they should be close to each other.
    if (bn == 1)
        return div_(prec, rnd, a.p, a.n, b.p[0]);

    size_t bn1 = bn - 1;
    ssize_t cn = an - bn1;
    // multiply both a and b by a common factor to
    // make sure that the most significant digit of b
    // is at least R/2 == 500000000.
    // since there is no remainder here we just multiply both
    // a and b until most significant digit of b is large enough.
    u = b.p[bn1];

    // as long as we can multiply by a power of 10 we do not have to
    // involve a - just adjust the x value accordingly.
    while (1000ULL*u < Rhalf ) {
        b.mul_(1000,0);
        x -= 3;
        u = b.p[bn1];
    }
    while (10*u < Rhalf) {
        b.mul_(10,0);
        --x;
        u = b.p[bn1];
    }
    while (u < Rhalf) {
        b.mul_(2,0);
        a.mul_(2,0);
        u = b.p[bn1];
    }
    if (b.n != bn) {
        fprintf(stderr, "Panic, bn is %zd while b.n is %zd\n",
                bn, b.n);
        throw "Problem in decimal::div_";
    }
    // an >= bn and most significant digit of b >= R/2.
    // we are ready, to divide.
    // Integer part will be cn digits, any additional digits
    // are fraction and depends on precision.
    ensure(j = k = cn + prec);
    i = an;
    digit umost = a.p[an-1];
    dbl_digit uj = umost, ujj;
    uj = uj * R + a.p[an-2]; // ok since an>=bn>1.
    digit vmost = b.p[bn-1];
    digit vnext = b.p[bn-2]; // ok since bn > 1.
    dbl_digit vmostd = vmost;
    dbl_digit vnextd = vnext; // ok since bn > 1.
    dbl_digit RR = R;
    n = j;
    // j - i == - bn + 1 + prec
    // i == j + bn - 1 - prec
    while (j > 0) {
        if (j < prec) { a.mulR(1); --x; }

        // guess qhat 
        // if a.p[--i] == v[vn-1] set qhat = 0x3fffffff;
        // else qhat = a.p[i:i-1]/b.p[bn-1];
        --i;
        ujj = (dbl_digit(a.p[i]) *R + a.p[i-1]);
        digit qhat = umost == vmost ? R-1 : digit(ujj/vmostd);

        // now test if v2*qhat > (uuj - qhat*vmost)*R + u[j + bn-2]
        dbl_digit qhatd = qhat;
        dbl_digit qhatvmost = qhatd*vmostd;
        dbl_digit qhatvnext = qhatd*vnextd;
        dbl_digit zz0 = ((ujj - qhatvmost)*RR) + a.p[i-2];
        if (qhatvnext > zz0) {
            --qhat; // then repeat the test.
            qhatd = qhat;
            qhatvmost -= vmostd;
            qhatvnext -= vnextd;
            zz0 = ((ujj - qhatvmost)*RR) + a.p[i-2];
            if (qhatvnext > zz0)
                qhatd = --qhat;
        }

        // multiply and subtract.
        // u[j+vn..j] -= qhat * v[vn-1..0]
        // starting with least significant digit first (u[j]), v[0]
        dbl_digit w = qhatd;
        zz0 = qhatd*(R-1);
        size_t k;
        if (qhat) {
            for (kk = 0; kk < bn; ++kk) {
                w = zz0 + w + a.p[kk+i-bn] - qhatd * b.p[kk];
                a.p[kk+i-bn] = digit(w % R);
                w /= R;
            }
            // one extra round where v is zero.
            w = zz0 + w + a.p[i];
            a.p[i] = digit(w % R);
            w /= R;
        }
        // if everything is right, w should equal qhatd now.
        if (w != qhatd) {
            // need to add back.
            qhatd = --qhat;
            zz0 = 0;
            for (kk = 0; kk < bn; ++kk) {
                zz0 = zz0 + a.p[kk+i-bn] + b.p[kk];
                a.p[kk+i-bn] = digit(zz0 % R);
                zz0 /= R;
            }
            zz0 = zz0 + a.p[i];
            a.p[i] = digit(zz0 % R);
            zz0 /= R;
            w += zz0;
            if (w != qhatd + 1) {
                printf( "j = %zd, i = %zd, w = %lld, "
                            "qhatd = %lld\n", j, i, w, qhatd);
            }
        }
        // p[j] = qhat.
        p[--j] = qhat;
    }
    // Now we need to determine rounding.
    // we use the value left in a to determine that.
    a.normalize();
    if (a.n > 0) {
        int e;
        digit adj = 0;
        switch (rnd) {
        case NEAREST_TIES_TO_EVEN:
            // need to compare a with half of b.
            // also if b is odd we need to remember that.
            u = b.div_(2);
            e = cmp__(a.p, a.n, b.p, b.n);
            // nearest means:
            // a < b/2 - round down.
            // a > b/2 - round up.
            // if a == b/2 but u == 1 then a is actually
            // less than half so we round down.
            // if a == b/2 and u == 0 it is exactly even
            // and we round up if the number is odd.
            if (e > 0) adj = 1;
            else if (e < 0) adj = 0;
            else if (u) adj = 0;
            else adj = p[0] & 1;
            break;

        case NEAREST_TIES_AWAY_ZERO:
            // need to compare a with half of b.
            // also if b is odd we need to remember that.
            u = b.div_(2);
            e = cmp__(a.p, a.n, b.p, b.n);
            // nearest means:
            // a > b/2 - round up.
            // a < b/2 round down.
            // a == b/2 and u == 0 - round up.
            adj = (e > 0 || (e == 0 && u == 0));
            break;

        case NEAREST_TIES_TO_ZERO:
            // need to compare a with half of b.
            // also if b is odd we need to remember that.
            u = b.div_(2);
            e = cmp__(a.p, a.n, b.p, b.n);
            // nearest means:
            // a <= b/2 - round down.
            // a > b/2 - round up.
            adj = e > 0;
            break;

        case UP: case AWAY_ZERO: adj = 1; break;
        }
        if (adj) {
            for (kk = 0; kk < n;) {
                if (++p[kk] < R) break;
                p[kk++] = 0;
            }
            if (kk == n) {
                ensure(1);
                p[n++] = 1;
            }
        }
    }
}

alf::decimal::builder &
alf::decimal::builder::div_(size_t prec, int rnd,
                                const digit * ap, size_t an,
                                ssize_t ax,
                                const digit * bp, size_t bn,
                                ssize_t bx, int neg)
{
    ssize_t cx = ax - bx;

    n = 0;
    if (an == 0 || bn == 0) {
        x = s = 0;
        return *this;
    }
    x = cx;
    // need to change UP/DOWN according to sign.
    // rounding up for a negative number is rounding towards zero.
    // roudning down for a negative number is rounding away from
    // zero. If the number is positive it is the other way around.
    if (neg) {
        switch (rnd) {
        case UP: rnd = TO_ZERO; break;
        case DOWN: rnd = AWAY_ZERO; break;
        }
    }
    div_(prec, rnd, ap, an, bp, bn);
    n = normalize__();
    s = neg ? -n : n;
    klass = n ? (neg ? NREGULAR : PREGULAR) : neg ? NEGZERO : PZERO;
}

alf::decimal::builder &
alf::decimal::builder::div(size_t prec, int rnd,
                                const digit * ap, size_t an,
                                ssize_t as, ssize_t ax, int akls,
                                digit b)
{
	enum { RR = dbl_digit(R), };

    dbl_digit w = 0;
    dbl_digit bb = b;
    digit u, adj = 0;
    size_t j = an = integer::builder::normalize_(ap, an);
    int neg = 0;

    switch (klass = akls) {
    case SNAN:
    case QNAN:
    case PINF:
    case NINF:
    case PZERO:
    case NEGZERO: klass = akls; n = 0; s = x = 0; return *this;
    case NREGULAR:
        neg = 1;
        switch (rnd) {
        case DOWN: rnd = AWAY_ZERO; break;
        case UP: rnd = TO_ZERO; break;
        }
        break;
    }

    if (j == 0) {
        klass = neg ? NEGZERO : PZERO;
        n = 0;
        s = x = 0;
        return *this;
    }

    // also make sure we have right precision.
    // we have N digits for every digit 0..an-2 and then some digits in
    // an-1.
    // Some of these are integer part and do not count against precision
    // and some are fraction and count as precision.
    // ax determines fractional part. If ax is positive we first drop it
    // to 0 by inserting N*ax 0's at bottom then increase for precision.
    // if ax is negative but larger than precision we can keep those digits
    // for now but reduce at end.
    if (ax > -prec) {
        // need to increase precision
        size_t u = ax + prec;
        size_t uu = (u + N - 1) / N;
        // we may get some extra but that's ok, we just don't want fewer.
        // insert uu zeroes at bottom.
        ensure(j = an + uu);
        memcpy(p + uu, ap, an*sizeof(digit));
        memset(p, 0, uu*sizeof(digit));
        x = ax + uu*N;
    } else {
        ensure(j = an);
        memcpy(p, ap, an*sizeof(digit));
        x = ax;
    }
    while (j > 0) {
        w = w * RR + ap[--j];
        p[j] = digit(w / bb);
        w %= bb;
    }
    // remainder determines rounding.
    u = digit(w);
    if (u) {
        switch (rnd) {
        case NEAREST_TIES_TO_EVEN:
            if (u + u > b) adj = 1;
            else if (u + u < b) adj = 0;
            else adj = p[0] & 1;
            break;
        case NEAREST_TIES_TO_ZERO:
            if (u + u > b) adj = 1;
            break;
        case NEAREST_TIES_AWAY_ZERO:
            if (u + u >= b) adj = 1;
            break;
        case UP: case AWAY_ZERO:
            adj = 1;
            break;
        }
        if (adj) {
            j = 0;
            while (j < n) {
                if (++p[j] < R) break;
                p[j++] = 0;
            }
            if (j == n) {
                ensure(1);
                p[n++] = 1;
            }
        }
    }
    return *this;
}

alf::decimal::builder &
alf::decimal::builder::div(size_t prec, int rnd,
                            const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls,
                            const digit * bp, size_t bn,
                            ssize_t bs, ssize_t bx, int bkls)
{
    if (akls == SNAN || akls == QNAN || bkls== SNAN || bkls == QNAN) {
        klass = QNAN;
        return *this;
    }
    switch (akls) {
    case PINF:
        switch (bkls) {
        case PINF:
        case NINF: return snan_();
        case NEGZERO:
        case NREGULAR: klass = NINF; return *this;
        }
        klass = PINF;
        return *this;
    case NINF:
        switch (bkls) {
        case PINF:
        case NINF: return snan_();
        case PZERO:
        case PREGULAR: klass = NINF; return *this;
        }
        klass = PINF;
        return *this;

    case PZERO:
        switch (bkls) {
        case PINF: klass = PZERO; return *this;
        case NINF: klass = NEGZERO; return *this;
        case PZERO:
        case NEGZERO: return snan_();
        case PREGULAR: klass = PZERO; return *this;
        case NREGULAR: klass = NEGZERO; return *this;
        }
    case NEGZERO:
        switch (bkls) {
        case PINF: klass = NEGZERO; return *this;
        case NINF: klass = PZERO; return *this;
        case PZERO:
        case NEGZERO: return snan_();
        case PREGULAR: klass = NEGZERO; return *this;
        case NREGULAR: klass = PZERO; return *this;
        }
    case PREGULAR:
        switch (bkls) {
        case PINF: klass = PZERO; return *this;
        case NINF: klass = NEGZERO; return *this;
        case PZERO: klass = PINF; return *this;
        case NEGZERO: klass = NINF; return *this;
        case PREGULAR:
            return div_(prec, rnd, ap, an, ax, bp, bn, bx, 0);
        case NREGULAR:
            return div_(prec, rnd, ap, an, ax, bp, bn, bx, 1);
        }
    case NREGULAR:
        switch (bkls) {
        case PINF: klass = NEGZERO; return *this;
        case NINF: klass = PZERO; return *this;
        case PZERO: klass = NINF; return *this;
        case NEGZERO: klass = PINF; return *this;
        case PREGULAR:
            return div_(prec, rnd, ap, an, ax, bp, bn, bx, 1);
        case NREGULAR:
            return div_(prec, rnd, ap, an, ax, bp, bn, bx, 0);
        }
    }
}

// static
int
alf::decimal::builder::cmp(const digit * ap,
                            size_t an, ssize_t ax,
                            const digit * bp,
                            size_t bn, ssize_t bx)
{
    // comparing the numbers need to take into account
    // the scale.

    // if for example ax == 1 and bx == 3
    // then ap[3] correspond to bp[1] ap[4] to bp[2] etc.
    // So when deciding which is longest we have to take
    // an + ax and comapre with bn + bx.
    an = integer::builder::normalize_(ap, an);
    bn = integer::builder::normalize_(bp, bn);
    int d = ax + an - bn - bx;
    if (d > 0) return 1;
    if (d < 0) return -1;
    size_t j = an;
    size_t k = bn;
    if (j >= k) {
        // an >= bn so compare up to bn first digits.
        while (k > 0)
            if ((d = ap[--j] - bp[--k]) > 0) return 1;
            else if (d < 0) return -1;
        while (j > 0)
            if ((d = ap[--j]) > 0) return 1;
    } else {
        // an < bn so compare up to an first digits.
        while (j > 0)
            if ((d = ap[--j] - bp[--k]) > 0) return 1;
            else if (d < 0) return -1;
        while (k > 0)
            if ((d = bp[--k]) > 0) return -1;
    }
    return 0;
}

// static
int
alf::decimal::builder::cmp(const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls,
                            const digit * bp, size_t bn,
                            ssize_t bs, ssize_t bx, int bkls)
{
    if (akls == SNAN || akls == QNAN || bkls== SNAN || bkls == QNAN) {
        return -2;
    }

    switch (akls) {
    case PINF: return bkls == PINF ? -2 : 1;
    case NINF: return bkls == NINF ? -2 : -1;
    case PZERO:
    case NEGZERO:
        switch(bkls) {
        case NINF:
        case NREGULAR: return 1;
        case PZERO:
        case NEGZERO: return 0;
        }
        return -1;
    case PREGULAR:
        switch(bkls) {
        case PINF: return -1;
        case NINF:
        case NREGULAR:
        case PZERO:
        case NEGZERO: return 1;
        }
        return cmp(ap, an, ax, bp, bn, bx);
    case NREGULAR:
        switch(bkls) {
        case PINF:
        case PREGULAR:
        case PZERO:
        case NEGZERO: return -1;
        case NINF: return 1;
        }
        return cmp(bp, bn, bx, ap, an, ax);
    }
}

// computer a number a - floor(a/b)*b
alf::decimal::builder &
alf::decimal::builder::frem(size_t prec, int rnd,
                            const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls,
                            const digit * bp, size_t bn,
                            ssize_t bs, ssize_t bx, int bkls)
{
    builder c, d;
    c.div(prec, rnd, ap, an, as, ax, akls, bp, bn, bs, bx, bkls);
    d.floor(c.p, c.n, c.s, c.x, c.klass);
    c.mul(bp, bn, bs, bx, bkls, d.p, d.n, d.s, d.x, d.klass);
    sub(ap, an, as, ax, akls, c.p, c.n, c.s, c.x, c.klass);
}

// compute integer part.
alf::decimal::builder &
alf::decimal::builder::intp(int rnd,
                                const digit * ap, size_t an,
                                ssize_t as, ssize_t ax, int akls)
{
    enum { R2 = R >> 1 };

    switch (klass = akls) {
    case SNAN: klass = QNAN; /* FALLTHRU */
    case QNAN:
    case PINF:
    case NINF:
    case PZERO:
    case NEGZERO: return *this;
    case NREGULAR:
        // negative number. change DOWN/UP to TO/AWAY ZERO.
        switch (rnd) {
        case DOWN: rnd = AWAY_ZERO; break;
        case UP: rnd = TO_ZERO; break;
        }
        break;
    }
    an = normalize___(ap, an);
    if (ax >= 0) {
        if ((n = an) == 0) {
        	s = 0;
        	x = 0;
        	klass = akls == NREGULAR ? NEGZERO : PZERO;
        	return *this;
        }
        ensure(an);
        memcpy(p, ap, an*sizeof(digit));
        s = akls == NREGULAR ? -n : n;
        x = ax;
        return *this;
    }
    // ax is negative so we must chop off some digits.
    size_t u = -ax;
    size_t u9 = u / N;
    if (an <= u9) {
        // result is zero.
        klass = akls == NREGULAR ? NEGZERO : PZERO;
        return *this;
    }
    // chop off some digits, remember if they were non-zero.
    size_t j = u9 - 1;
    while (j > 0 && ap[--j] == 0);
    int highbit = 0;
    int lowbits = j >= 0;
    size_t u9mod = u % N;
    digit y;
    int adj = 0;

    digit zz = stens[u9mod];
    if (j >= 0) lowbits = 1;
    y = ap[u9-1];
    if (u9mod == 0) {
        highbit = y >= R2;
        if (lowbits == 0 && y != R2 && y != 0) lowbits = 1;
    }
    // copy number to *this.
    ensure(an-u9);
    n = an - u9;
    memcpy(p, ap + u9, n*sizeof(digit));
    if (u9mod) {
        if (y != 0) lowbits |= 1;
        y = div_(zz);
        highbit = y + y >= zz;
        if (lowbits == 0 && y > 0)
            if (y + y != zz) lowbits |= 1;
    }
    switch (rnd) {
    case NEAREST_TIES_TO_EVEN:
        if (highbit) {
            if (lowbits) adj = 1;
            else adj = p[0] & 1;
        }
        break;
    case NEAREST_TIES_TO_ZERO:
        if (highbit && lowbits) adj = 1;
        break;
    case NEAREST_TIES_AWAY_ZERO:
        if (highbit) adj = 1;
        break;
    case UP: case AWAY_ZERO: adj = 1; break;
    case DOWN: case TO_ZERO: adj = 0; break;
    }
    if (adj) {
        j = 0;
        while (j < n) {
            if (++p[j] < R) break;
            p[j++] = 0;
        }
        if (j == n)
            p[n++] = 1;
    }
    x = 0;
    s = klass == NREGULAR ? -n : n;
    return *this;
}

// This drops the integer part and keep only the fractional
// part of the number.
alf::decimal::builder &
alf::decimal::builder::fractionp(size_t prec, int rnd,
                                    const digit * ap, size_t an,
                                    ssize_t as, ssize_t ax, int akls)
{
    enum { R2 = R >> 1 };

    switch (klass = akls) {
    case SNAN: klass = QNAN; /* FALLTHRU */
    case QNAN:
    case PINF:
    case NINF:
    case PZERO:
    case NEGZERO: return *this;
    case NREGULAR:
        // negative number. change DOWN/UP to TO/AWAY ZERO.
        switch (rnd) {
        case DOWN: rnd = AWAY_ZERO; break;
        case UP: rnd = TO_ZERO; break;
        }
        break;
    }
    an = normalize___(ap, an);
    if (ax >= 0) {
        n = 0;
        s = 0;
        x = 0;
        klass = akls == NREGULAR ? NEGZERO : PZERO;
        return *this;
    }
    // ax is negative, we want there to be exactly prec digits.
    size_t u = -ax;
    // we want to chop off the integer part.
    // remember that ax count in 10's while an count in R's == 10**N.
    // but here we want to chop off any digits in a that is above
    // ax.
    size_t u9 = u / N;
    size_t u9mod = u % N;
    x = ax;
    // so we want to keep u9 words (0..u9-1) and p[u9] we want to
    // keep the u9mod digits of it, i.e. p[u9] % 10**u9mod
    // Any digits from p[u9+1..an-1] should be removed.
    // We also want to keep only prec digits so u9 - prec
    // should be dropped as well.
    // if an <= u9 we regard any digits above an to be 0
    // and we keep all of an.
    // if an == u9 + 1 we chop u9mod digits from top of highest
    // digit and if an > u9 we ignore any digits above u9.
  	if (an > u9) an = u9 + 1;
  	digit vf = 1;
  	digit rnd_lowbits = 0;
  	digit rnd_top_dig = 0;

  	if (u > prec) {
  		// can disregard u-prec lower digits.
  		size_t v = u - prec;
  		size_t v9 = v / N;
  		size_t v9mod = v % N;
  		// we can immediately disregard v9 lower digits.
  		// check if they are zero or not.
  		// we divide the digits in two groups.
  		// the most significant among those we discard
  		// and all the others.
  		// For all the others we only care if they are
  		// all 0 or not.
  		if (v9mod) {
  			rnd_lowbits = is_zero_(ap, v9) ? 0 : 1;
  			rnd_top_dig = ap[v9];
  			digit qqq = stens[v9mod-1];
  			if ((rnd_top_dig % qqq) && rnd_lowbits == 0)
  				rnd_lowbits = 1;
  			rnd_top_dig /= qqq;
  			rnd_top_dig %= 10;
  			// we also need to disregard v9mod digits of the
  			// lowest digit. After we copied ap over to *this
  			// we divide by vf to remove those digits, so
  			// vf == 10**v9mod.
  			vf = stens[v9mod];
  		} else {
  			rnd_top_dig = ap[v9-1];
  			rnd_lowbits = is_zero_(ap, v9-1) ? 0 : 1;
  			if ((rnd_top_dig % 100000000) != 0 && rnd_lowbits == 0)
  				rnd_lowbits = 1;
  			rnd_top_dig = rnd_top_dig / 100000000;
  		}
  		ap += v9;
  		an -= v9;
  		u9 -= v9;
  		x = -prec;
  	}
  	digit apu9 = ap[u9] % stens[u9mod];
  	// now we have that an <= u9+1.
  	ensure(an);
  	memcpy(p, ap, an*sizeof(digit));
  	n = an;
  	if (vf > 1) div_(vf);
  	digit adj = 0;
  	if (rnd_top_dig != 0 || rnd_lowbits) {
  		switch (rnd) {
  		case NEAREST_TIES_TO_EVEN:
  			if (rnd_top_dig > 5) adj = 1;
  			else if (rnd_top_dig == 5) {
  				if (rnd_lowbits) adj = 1;
  				else adj = p[0] & 1;
  			}
  			break;
  		case NEAREST_TIES_AWAY_ZERO:
  			if (rnd_top_dig >= 5) adj = 1;
  			break;
  		case NEAREST_TIES_TO_ZERO:
  			if (rnd_top_dig > 5) adj = 1;
  			else if (rnd_top_dig == 5 && rnd_lowbits) adj = 1;
  			break;
  		case UP: case AWAY_ZERO:
  			adj = 1;
  			break;
  		}
  		if (adj) {
  			size_t jj = 0;
  			while (jj < n) {
  				if (++p[jj] < R) break;
  				p[jj++] = 0;
  			}
  			if (jj == n) {
  				ensure(1);
  				p[n++] = 1;
  			}
  		}
  	}
  	n = normalize___(p, n);
  	s = akls == NREGULAR ? -n : n;
  	return *this;
}

// round to prec decimals, round down.
alf::decimal::builder &
alf::decimal::builder::floor(const digit * ap, size_t an,
                            	ssize_t as, ssize_t ax, int akls)
{ return round(0, DOWN, ap, an, as, ax, akls); }

alf::decimal::builder &
alf::decimal::builder::ceil(const digit * ap, size_t an,
                            ssize_t as, ssize_t ax, int akls)
{ return round(0, UP, ap, an, as, ax, akls); }

alf::decimal::builder &
alf::decimal::builder::round(size_t prec, int rnd,
                            	const digit * ap, size_t an,
                            	ssize_t as, ssize_t ax, int akls)
{
	int neg = 0;
	n = 0;
	switch (klass = akls) {
	case SNAN:
	case QNAN:
	case PINF:
	case NINF:
	case PZERO:
	case NEGZERO: return *this;
	case NREGULAR:
		neg = 1;
		switch (rnd) {
		case UP: rnd = TO_ZERO; break;
		case DOWN: rnd = AWAY_ZERO; break;
		}
		break;
	}
	if (ax >= 0) {
		an = integer::builder::normalize_(ap, an);
		ensure(an);
		if ((n = an) == 0) {
			s = x = 0;
			klass = neg ? NEGZERO : PZERO;
			return *this;
		}
		memcpy(p, ap, n*sizeof(digit));
		x = ax;
		s = neg ? -n : n;
		return *this;
	}
	// ax < 0, is ax < -prec?
	size_t u = -ax;
	digit vf = 1;
	digit adj = 0;
	digit top_dig = 0;
	digit lower_digs = 0;
	if (u > prec) {
		size_t v = u - prec;
		// chop away v low digits.
		size_t v9 = v / N;
		size_t v9mod = v % N;
		size_t vv9 = v9;
		digit q;
		if (an < vv9) vv9 = an;
		if (v9mod) {
			if (! is_zero_(ap, vv9)) lower_digs = 1;
			if (an > v9) top_dig = ap[v9];
			q = stens[v9mod-1];
			if (top_dig % q) lower_digs = 1;
			top_dig /= q;
			top_dig %= 10;
			vf = stens[v9mod];
		} else {
			if (! is_zero_(ap, vv9-1)) lower_digs = 1;
			if (an >= v9) top_dig = ap[v9-1];
			if ((q = top_dig % 100000000) != 0) lower_digs = 1;
			top_dig /= 100000000;
		}
		ap += v9;
		an -= v9;
		ax = -prec;
	}
	ensure(an);
	memcpy(p, ap, an*sizeof(digit));
	n = an;
	if (vf != 1) div_(vf);
	if (lower_digs != 0 || top_dig != 0) {
		switch (rnd) {
		case NEAREST_TIES_TO_EVEN:
			if (top_dig > 5) adj = 1;
			else if (top_dig < 5) adj = 0;
			else if (lower_digs) adj = 1;
			else adj = p[0] & 1;
			break;
		case NEAREST_TIES_TO_ZERO:
			if (top_dig > 5) adj = 1;
			else if (top_dig < 5) adj = 0;
			else if (lower_digs) adj = 1;
			break;
		case NEAREST_TIES_AWAY_ZERO:
			if (top_dig >= 5) adj = 1;
			break;
		case UP: case AWAY_ZERO:
			adj = 1;
			break;
		}
		if (adj) {
			size_t jj = 0;
			while (jj < n) {
				if (++p[jj] < R) break;
				p[jj++] = 0;
			}
			if (jj == n) {
				ensure(1);
				p[n++] = 1;
			}
		}
	}
	n = normalize___(p, n);
	if (n == 0) {
		s = x = 0;
		klass = klass == NREGULAR ? NEGZERO : PZERO;
		return *this;
	}
	s = neg ? -n : n;
	x = ax;
	return *this;
}

alf::decimal::builder &
alf::decimal::builder::pow(unsigned int j)
{
	builder f(*this), g, h;
	assign(1);
	if (j) {
		while (true) {
			if (j & 1) {
				g.clear();
				g.mul(p, n, s, x, klass,
						f.p, f.n, f.s, f.x, f.klass);
			} else
                g.assign(*this);
			if ((j >>= 1) == 0) { assign(g); break; }
			h.clear();
			h.mul(f.p, f.n, f.s, f.x, f.klass,
					f.p, f.n, f.s, f.x, f.klass);
			if (j & 1) {
				clear();
				mul(g.p, g.n, g.s, g.x, g.klass,
						h.p, h.n, h.s, h.x, h.klass);
			} else
                assign(g);
			if ((j >>= 1) == 0) break;
			f.clear();
			f.mul(h.p, h.n, h.s, h.x, h.klass,
					h.p, h.n, h.s, h.x, h.klass);
		}
	}
	return *this;
}

alf::decimal::builder &
alf::decimal::builder::pow(int j)
{
	if (j >= 0) return pow((unsigned int)j);
	// invert *this.
	builder u(1), v(*this);
	clear();
	div(prec, rnd, u.p, u.n, u.s, u.x, u.klass,
			v.p, v.n, v.s, v.x, v.klass);
	return pow((unsigned int)-j);
}

#if 0

alf::decimal::builder &
alf::decimal::builder::pow(const digit * ap, size_t an,
							ssize_t as, ssize_t ax, int akls,
                        	const digit * bp, size_t bn,
                        	ssize_t bs, ssize_t bx, int bkls)
{

    return *this;
}

namespace alf {

class decimal {
public:
    typedef unsigned int digit;

    class builder {
    public:


    private:

        friend class decimal;

        enum {
            INI_SIZE = 8,
            R = 1000000000,
            N = 9, // R == 10**N
        };


        digit arr[INI_SIZE];
        digit * p;
        size_t n;
        size_t m;
        ssize_t s;
        ssize_t x;
    };

    decimal();
    decimal(const builder & b);
    decimal(const integer::builder & b);
    decimal(const decimal & d);
    decimal(const integer & k);
    decimal(int k);
    decimal(unsigned int k);
    decimal(long int k);
    decimal(unsigned long int k);
    decimal(long long int k);
    decimal(unsigned long long int k);
    decimal(float x);
    decimal(double x);
    decimal(long double x);
    decimal(const digit * xp, size_t xn,
                ssize_t xs, ssize_t xx);
    decimal(const digit * xp, ssize_t xs, ssize_t xx);
    ~decimal() { }

private:

    /*
    ** this implementation stores the integer in a string object.
    ** just so that we can make use of std::string's ref counting
    ** etc. Just because we are too lazy to implement our own.
    */

    struct rep_base {
        _Atomic_word r;
        ssize_t len;
        ssize_t x;
    };

    struct rep : rep_base {
        bool is_leaked() { return r < 0; }
        bool is_shared() { return r > 0; }
        void set_leaked() { r = -1; }
        void set_sharable() { r = 0; }

        void set_length_sign_and_sharable(ssize_t n)
        {
            set_sharable();
            len = n;
        }

        digit * data() throw() { return reinterpret_cast<digit *>(this + 1); }

        digit * grab(const _Alloc & alloc1, const _Alloc & alloc2)
        {
            return (! is_leaked() && alloc1 == alloc2) ?
                        copy() : clone(alloc1);
        }

        static rep *
        create(ssize_t n, ssize_t sign, const _Alloc & alloc);

        void dispose(const _Alloc & a)
        {
            if (__gnu_cxx::__exchange_and_add_dispatch(& r, -1) < 0)
                delete [] (char *)this;
        }

        digit * copy() throw()
        {
            __gnu_cxx::__atomic_add_dispatch(& r, 1);
            return data();
        }

        digit * clone(const _Alloc & a, ssize_t res = 0);

        static rep * getrep(digit * d) { return (rep *)d - 1; }
        static digit * getdig(rep * r) { return (digit *)(r + 1); }
        digit * getdig() { return (digit *)(this + 1); }

        rep(ssize_t xlen, ssize_t xx);
    };

    struct _Alloc_hider : _Alloc {
        _Alloc_hider(digit * dd, const _Alloc & a) throw()
        : _Alloc(a), d(dd) { }

        digit * d;
    };

    digit * wdata() const throw() { return dataptr.d; }
    digit * wdata(digit * d) throw() { return dataptr.d = d; }
    rep * Rep() const { return reinterpret_cast<rep *>(wdata()) - 1; }

    void decref(digit * & p)
    {
        rep * r = rep::getrep(p);
        p = 0;
        if (__gnu_cxx::__exchange_and_add_dispatch(& r->r, -1) < 0)
            delete [] (char *)r;
    }

    digit * incref(digit * p)
    {
        __gnu_cxx::__atomic_add_dispatch(& rep::getrep(p)->r, 1);
        return p;
    }

    digit * mk();
    digit * mk(const digit * d, size_t dn, ssize_t ds, ssize_t dx);
    digit * mk(const digit * d, ssize_t ds, ssize_t dx);
    digit * mk(const builder & b);
    digit * mk(const integer::builder & b);
    digit * mk(const integer & k);
    digit * mk(int k);
    digit * mk(unsigned int k);
    digit * mk(long int k);
    digit * mk(unsigned long int k);
    digit * mk(long long int k);
    digit * mk(unsigned long long int k);
    digit * mk(float x);
    digit * mk(double x);
    digit * mk(long double x);
    digit * assign(builder & b);

    decimal(const digit * d, ssize_t ds, ssize_t dx)
        : dataptr(mk(d, size_t(ds < 0 ? -ds : ds), ds, dx), _Alloc())
    { }

    decimal(const digit * d, size_t dn, ssize_t ds, ssize_t dx)
        : dataptr(mk(d, dn, ds, dx), _Alloc())
    { }

    static rep nanr_;
    static rep ninfr_;
    static rep pinfr_; 
    static digit * nan_;
    static digit * ninf_;
    static digit * pinf_;

    mutable _Alloc_hider dataptr;
};

}

#endif
