#ifndef ALF_INTEGER_HXX
#define ALF_INTEGER_HXX

#include <ext/atomicity.h>

namespace alf {

class decimal;

class integer {
public:

    class builder;

private:

    friend class builder;

    typedef unsigned int digit;
    typedef unsigned long long int dbl_digit;
    typedef std::allocator<digit> _Alloc;

public:

    class builder {
    public:
        builder & assign(const builder & b);
        int assign(const decimal & d);
        builder & assign(int k);
        builder & assign(unsigned int k);
        builder & assign(long int k);
        builder & assign(unsigned long int k);
        builder & assign(long long int k);
        builder & assign(unsigned long long int k);
        int assign(float k);
        int assign(double k);
        int assign(long double k);
        builder & assign(const digit * a, size_t an, ssize_t as);
        builder & assign(const digit * a, ssize_t as);
        builder & assign(const integer & k);
        int assign(const digit * ap, size_t an, ssize_t as,
                            ssize_t ax, int akls);

        builder();
        builder(const builder & b);
        builder(int k);
        builder(unsigned int k);
        builder(long int k);
        builder(unsigned long int k);
        builder(long long int k);
        builder(unsigned long long int k);
        builder(const digit * a, size_t an, ssize_t as);
        builder(const digit * a, ssize_t as);
        builder(const integer & k);
        ~builder();

        bool get(signed char & k) const;
        bool get(unsigned char & k) const;
        bool get(char & k) const;
        bool get(short & k) const;
        bool get(unsigned short & k) const;
        bool get(int & k) const;
        bool get(unsigned int & k) const;
        bool get(long int & k) const;
        bool get(unsigned long int & k) const;
        bool get(long long int & k) const;
        bool get(unsigned long long int & k) const;
        bool get(float & k) const;
        bool get(double & k) const;
        bool get(long double & k) const;

        size_t length() const { return n; }
        ssize_t slength() const { return s; }
        const digit * data() const { return a; }
        digit * wdata() { return a; }

        static size_t normalize_(const digit * a, size_t an);
        static void negbits2_(digit * c, size_t cn, const digit * a, size_t an);
        static void invbits2_(digit * c, size_t cn, const digit * a, size_t an);

        static void negbits_(digit * a, size_t an)
        { negbits2_(a, an, a, an); }

        static void invbits_(digit * a, size_t an)
        { invbits2_(a, an, a, an); }

        static digit add1_(digit * a, size_t an);
        static bool sub1_(digit * a, size_t an);

        // c = a + b + carry, return new carry if necessary.
        // we must have cn >= max(an,bn).
        // c == a or c == b or c == a and c == b are all ok.
        static digit addc_(digit * c, size_t cn,
                            const digit * a, size_t an,
                            const digit * b, size_t bn, digit carry);

        // c = a - b - borrow, return new borrow if necessary.
        // we must have cn >= max(an,bn).
        // c == a or c == b or c == a and c == b are all ok.
        static digit subb_(digit * c, size_t cn,
                            const digit * a, size_t an,
                            const digit * b, size_t bn, digit borrow);

        // d = a*b + c
        static digit muladd4_(digit * d, size_t dn,
                                const digit * a, size_t an,
                                digit b, digit c);

        // c += a*b
        static void muladd_(digit * c, size_t cn,
                                const digit * a, size_t an,
                                digit b);


        // c += a*b
        // cn >= an + bn.
        static void muladd_(digit * c, size_t cn,
                                const digit * a, size_t an,
                                const digit * b, size_t bn);

        // c += a * b
        // cn >= an + bn.
        // Either an or bn are assumed to be less than a certain cutoff
        // value.
        static void muladd_short_(digit * c, size_t cn,
                                    const digit * a, size_t an,
                                    const digit * b, size_t bn);

        // c += a * b
        // cn >= an + bn.
        // bn is assumed to be much smaller than an so that
        // an is a sequence of (an + bn -1)/bn chunks.
        static void muladd_seq_(digit * c, size_t cn,
                                    const digit * a, size_t an,
                                    const digit * b, size_t bn);

        // c += a * b
        // cn >= an + bn
        // we assume an > bn/2 and bn > 70.
        static void muladd_karatsuba_(digit * c, size_t cn,
                                        const digit * a, size_t an,
                                        const digit * b, size_t bn);
        // c = a * b
        // cn >= an + bn
        // we assume an > bn/2 and bn > 70.
        static void mul_karatsuba_(digit * c, size_t cn,
                                    const digit * a, size_t an,
                                    const digit * b, size_t bn);

        // c = a*b, cn >= an, return carry if necessary.
        static digit mul_(digit * c, size_t cn,
                            const digit * a, size_t an, digit b)
        { return muladd4_(c, cn, a, an, b, 0); }

        // c = a*b, cn >= an+bn.
        static void mul_(digit * c, size_t cn,
                            const digit * a, size_t an,
                            const digit * b, size_t bn);

        // c = a*b cn >= an+bn, bn is assumed to be much smaller than an
        // so that the number a is considered to be a sequence of
        // (an + bn - 1)/bn digits - each being bn in length.
        static void mul_seq_(digit * c, size_t cn,
                                const digit * a, size_t an,
                                const digit * b, size_t bn);

        // c = a * b
        // cn >= an + bn.
        // Either an or bn are assumed to be less than a certain cutoff
        // value.
        static void mul_short_(digit * c, size_t cn,
                                const digit * a, size_t an,
                                const digit * b, size_t bn);

        // c = a / b, return a % b.
        // cn >= an
        static digit div_(digit * c, size_t cn,
                            const digit * a, size_t an, digit b);

        // q = a / b, r = a % b.
        // return -1 if b == 0.
        // return -2 if any sizes are too short to hold results.
        // Either q or r may be 0 meaning we're not interested in
        // that result.
        // Both q and r may equal either a or b but not same.
        // if q == r the modulus is discarded.
        static int  div_(digit * q, size_t * qnptr,
                            digit * r, size_t * rnptr,
                            const digit * a, size_t an,
                            const digit * b, size_t bn);

        // *this = *this * f + a
        builder & mul_(digit f, digit a);

        // ret = *this % f, *this = *this / f
        unsigned int div_(unsigned int f);

        void ensure(size_t k);
        builder & normalize(); // remove leading zeroes.

        // set length to be at least k long, insert leading zeroes
        // if necessary. If length is already longer than k, do nothing.
        builder & length_at_least(size_t k);

        // bits of the number are inverted + 1.
        builder & negbits();

        // length is negated. changing sign of num.
        builder & neg()
        { s = -s; return *this; }

        // bits of the number are
        // complemented/inverted.
        builder & invbits();

        builder & add1();
        builder & sub1();

        // *this = 0;
        builder & clear() { n = 0; s = 0; return *this; }

        static int cmp_(const digit * a, size_t an, ssize_t as,
                            const digit * b, size_t bn, ssize_t bs);

        int cmp(const digit * d, size_t dn, ssize_t ds) const;

        int cmp(const builder & b) const
        { return cmp(b.a, b.n, b.s); }

        int cmp(const integer & k) const
        { return cmp(k.data(), k.u_len(), k.s_len()); }

        int cmp(int k) const;
        int cmp(unsigned int k) const;
        int cmp(long int k) const;
        int cmp(unsigned long int k) const;
        int cmp(long long int k) const;
        int cmp(unsigned long long int k) const;

        // *this = (neg ? -1 : 1) * (|*this| + |d|)
        builder & add_(const digit * d, size_t dn, int neg);

        // *this = (neg ? -1 : 1) * (|*this| - |d|)
        builder & sub_(const digit * d, size_t dn, int neg);

        // *this = (neg ? -1 : 1) * (|d| - |*this|)
        builder & sub2_(const digit * d, size_t k, int neg);

        builder & add(const digit * aa, size_t an, ssize_t as,
                        const digit * bb, size_t bn, ssize_t bs);

        builder & add(const digit * d, size_t dn, ssize_t ds);

        builder & add(const builder & a, const builder & b);

        builder & add(const builder & b)
        { return add(b.a, b.n, b.s); }

        builder & add(const integer & k)
        { return add(k.data(), k.u_len(), k.s_len()); }

        builder & add(int k);
        builder & add(unsigned int k);
        builder & add(long int k);
        builder & add(unsigned long int k);
        builder & add(long long int k);
        builder & add(unsigned long long int k);

        builder & sub(const digit * aa, size_t an, ssize_t as,
                        const digit * bb, size_t bn, ssize_t bs);

        builder & sub(const digit * d, size_t dn, ssize_t ds);

        builder & sub(const builder & b)
        { return sub(b.a, b.n, b.s); }

        builder & sub(const builder & a, const builder & b);

        builder & sub(const integer & k)
        { return sub(k.data(), k.u_len(), k.s_len()); }

        builder & sub(int k);
        builder & sub(unsigned int k);
        builder & sub(long int k);
        builder & sub(unsigned long int k);
        builder & sub(long long int k);
        builder & sub(unsigned long long int k);

        builder & mul(const digit * aa, size_t an, ssize_t as,
                        const digit * ba, size_t bn, ssize_t bs);

        builder & mul(const digit * d, size_t dn, ssize_t ds)
        { return mul(a, n, s, d, dn, ds); }

        builder & mul(const builder & b)
        { return mul(a, n, s, b.a, b.n, b.s); }

        builder & mul(const builder & a, const builder & b)
        { return mul(a.a, a.n, a.s, b.a, b.n, b.s); }

        builder & mul(const integer & k)
        { return mul(k.data(), k.u_len(), k.s_len()); }

        builder & mul(const integer & a, const integer & b)
        {
            return mul(a.data(), a.u_len(), a.s_len(),
                        b.data(), b.u_len(), b.s_len());
        }

        builder & mul(int k);
        builder & mul(unsigned int k);
        builder & mul(long int k);
        builder & mul(unsigned long int k);
        builder & mul(long long int k);
        builder & mul(unsigned long long int k);

        static int divmod(digit * q, size_t * qn, ssize_t * qs,
                            digit * r, size_t * rn, ssize_t * rs,
                            const digit * a, size_t an, ssize_t as,
                            const digit * b, size_t bn, ssize_t bs);

        builder & div(const digit * d, size_t dn, ssize_t ds);

        builder & div(const builder & b)
        { return div(b.a, b.n, b.s); }

        builder & div(const integer & k)
        { return div(k.data(), k.u_len(), k.s_len()); }

        builder & div(int k);
        builder & div(unsigned int k);
        builder & div(long int k);
        builder & div(unsigned long int k);
        builder & div(long long int k);
        builder & div(unsigned long long int k);

        builder & mod(const digit * d, size_t dn, ssize_t ds);

        builder & mod(const builder & b)
        { return mod(b.a, b.n, b.s); }

        builder & mod(const integer & k)
        { return mod(k.data(), k.u_len(), k.s_len()); }

        builder & mod(int k);
        builder & mod(unsigned int k);
        builder & mod(long int k);
        builder & mod(unsigned long int k);
        builder & mod(long long int k);
        builder & mod(unsigned long long int k);

        static int divmod(builder & q, builder & r,
                            const digit * a, size_t an, ssize_t as,
                            const digit * b, size_t bn, ssize_t bs);

        static int divmod(builder & q, builder & r,
                            const builder & a,
                            const builder & b)
        { return divmod(q, r, a.a, a.n, a.s, b.a, b.n, b.s); }

        // *this = a**b % c, c can be 0 in which case we do no modulus.
        static int pow_simple(builder & res, const builder & a, digit b,
                                    const builder & c);

        // *this = a**b % c, c can be 0 in which case we do no modulus.
        // b cannot be too high and cannot be negative.
        int pow(const digit * a, size_t an, ssize_t as,
                    const digit * b, size_t bn, ssize_t bs,
                    const digit * c, size_t cn, ssize_t cs);

        int pow(const builder & a, const builder & b)
        { return pow(a.a, a.n, a.s, b.a, b.n, b.s, 0, 0, 0); }

        int pow(const builder & a, const builder & b, const builder & c)
        { return pow(a.a, a.n, a.s, b.a, b.n, b.s, c.a, c.n, c.s); }

        int fact(int k);
        int fact(unsigned int k);
        int fact(unsigned long long int k);
        int fact(const digit * k, size_t kn, ssize_t ks);
        int fact(const builder & b) { return fact(b.a, b.n, b.s); }
        int fact() { return fact(a, n, s); }

        static void bit_and_(digit * c, size_t * cnptr, ssize_t * csptr,
                            const digit * a, size_t an, ssize_t as,
                            const digit * b, size_t bn, ssize_t bs);

        builder & bit_and(const digit * d, size_t dn, ssize_t ds);

        builder & bit_and(const builder & b)
        { return bit_and(b.a, b.n, b.s); }

        builder & bit_and(const integer & k)
        { return bit_and(k.data(), k.u_len(), k.s_len()); }

        builder & bit_and(int k);
        builder & bit_and(unsigned int k);
        builder & bit_and(long int k);
        builder & bit_and(unsigned long int k);
        builder & bit_and(long long int k);
        builder & bit_and(unsigned long long int k);

        static void bit_andnot_(digit * c, size_t * cnptr, ssize_t * csptr,
                                const digit * a, size_t an, ssize_t as,
                                const digit * b, size_t bn, ssize_t bs);

        builder & bit_andnot(const digit * d, size_t dn, ssize_t ds);

        builder & bit_andnot(const builder & b)
        { return bit_andnot(b.a, b.n, b.s); }

        builder & bit_andnot(const integer & k)
        { return bit_andnot(k.data(), k.u_len(), k.s_len()); }

        builder & bit_andnot(int k);
        builder & bit_andnot(unsigned int k);
        builder & bit_andnot(long int k);
        builder & bit_andnot(unsigned long int k);
        builder & bit_andnot(long long int k);
        builder & bit_andnot(unsigned long long int k);

        static void bit_or_(digit * c, size_t * cnptr, ssize_t * csptr,
                            const digit * a, size_t an, ssize_t as,
                            const digit * b, size_t bn, ssize_t bs);

        builder & bit_or(const digit * d, size_t dn, ssize_t ds);

        builder & bit_or(const builder & b)
        { return bit_or(b.a, b.n, b.s); }

        builder & bit_or(const integer & k)
        { return bit_or(k.data(), k.u_len(), k.s_len()); }

        builder & bit_or(int k);
        builder & bit_or(unsigned int k);
        builder & bit_or(long int k);
        builder & bit_or(unsigned long int k);
        builder & bit_or(long long int k);
        builder & bit_or(unsigned long long int k);

        static void bit_xor_(digit * c, size_t * cnptr, ssize_t * csptr,
                            const digit * a, size_t an, ssize_t as,
                            const digit * b, size_t bn, ssize_t bs);

        builder & bit_xor(const digit * d, size_t dn, ssize_t ds);

        builder & bit_xor(const builder & b)
        { return bit_xor(b.a, b.n, b.s); }

        builder & bit_xor(const integer & k)
        { return bit_xor(k.data(), k.u_len(), k.s_len()); }

        builder & bit_xor(int k);
        builder & bit_xor(unsigned int k);
        builder & bit_xor(long int k);
        builder & bit_xor(unsigned long int k);
        builder & bit_xor(long long int k);
        builder & bit_xor(unsigned long long int k);

        builder & shl(const digit * d, size_t dn, ssize_t ds);

        builder & shl(const builder & b)
        { return shl(b.a, b.n, b.s); }

        builder & shl(const integer & k)
        { return shl(k.data(), k.u_len(), k.s_len()); }

        builder & shl(int k);
        builder & shl(unsigned int k);
        builder & shl(long int k);
        builder & shl(unsigned long int k);
        builder & shl(long long int k);
        builder & shl(unsigned long long int k);

        builder & shr(const digit * d, size_t dn, ssize_t ds);

        builder & shr(const builder & b)
        { return shr(b.a, b.n, b.s); }

        builder & shr(const integer & k)
        { return shr(k.data(), k.u_len(), k.s_len()); }

        builder & shr(int k);
        builder & shr(unsigned int k);
        builder & shr(long int k);
        builder & shr(unsigned long int k);
        builder & shr(long long int k);
        builder & shr(unsigned long long int k);

    private:

        enum {
            SMALL_NUMS_BIAS = 64,
            SMALL_NUMS_SIZE = SMALL_NUMS_BIAS + 256,
            INI_SIZE = 8,
            LARGER_SIZE = 1024,
            BITS = 30,
            R = 1 << BITS,
            BITS_MASK = R - 1,
            KARATSUBA_MUL_CUTOFF = 70,
            KARATSUBA_SQURE_CUTOFF = KARATSUBA_MUL_CUTOFF << 1,
        };
        enum {
            R2 = dbl_digit(R),
            DBITS_MASK = dbl_digit(BITS_MASK),
        };

        friend class integer;
        friend class decimal;

        static digit * small_nums[SMALL_NUMS_SIZE];

        digit arr[INI_SIZE];
        digit * a;
        size_t n;
        size_t m;
        ssize_t s; // length with sign.
    };


    integer() : dataptr(mk(), _Alloc()) { }
    integer(const builder & b) : dataptr(mk(b), _Alloc()) { }

    integer(const integer & k)
        : dataptr(incref(k.dataptr.d), k.get_allocator())
    { }

    integer(const decimal & k) : dataptr(mk(k), _Alloc()) { }

    integer(int k) : dataptr(mk(k), _Alloc()) { }
    integer(unsigned int k) : dataptr(mk(k), _Alloc()) { }
    integer(long int k) : dataptr(mk(k), _Alloc()) { }
    integer(unsigned long int k) : dataptr(mk(k), _Alloc()) { }
    integer(long long int k) : dataptr(mk(k), _Alloc()) { }
    integer(unsigned long long int k) : dataptr(mk(k), _Alloc()) { }
    integer(float k) : dataptr(mk(k), _Alloc()) { }
    integer(double k) : dataptr(mk(k), _Alloc()) { }
    integer(long double k) : dataptr(mk(k), _Alloc()) { }
    ~integer() { decref(dataptr.d); }

    _Alloc get_allocator() const { return dataptr; }

    bool get(char & x) const;
    bool get(signed char & x) const;
    bool get(short int & x) const;
    bool get(int & x) const;
    bool get(long int & x) const;
    bool get(long long & x) const;
    bool get(unsigned char & x) const;
    bool get(unsigned short int & x) const;
    bool get(unsigned int & x) const;
    bool get(unsigned long int & x) const;
    bool get(unsigned long long int & x) const;
    bool get(float & x) const;
    bool get(double & x) const;
    bool get(long double & x) const;

    integer & operator = (const integer & k)
    {
        if (k.dataptr.d != dataptr.d) {
            decref(dataptr.d);
            dataptr.d = incref(k.dataptr.d);
        }
        return *this;
    }

    integer & operator = (const builder & b)
    {
        digit * q = mk(b);
        if (dataptr.d != q) {
            decref(dataptr.d);
            dataptr.d = incref(q);
        }
        decref(q);
        return *this;
    }

    integer & operator = (int k)
    {
        digit * q = mk(k);
        if (dataptr.d != q) {
            decref(dataptr.d); dataptr.d = incref(q);
        }
        decref(q);
        return *this;
    }

    integer & operator = (unsigned int k)
    {
        digit * q = mk(k);
        if (dataptr.d != q) {
            decref(dataptr.d); dataptr.d = incref(q);
        }
        decref(q);
        return *this;
    }

    integer & operator = (long int k)
    {
        digit * q = mk(k);
        if (dataptr.d != q) {
            decref(dataptr.d); dataptr.d = incref(q);
        }
        decref(q);
        return *this;
    }

    integer & operator = (unsigned long int k)
    {
        digit * q = mk(k);
        if (dataptr.d != q) {
            decref(dataptr.d); dataptr.d = incref(q);
        }
        decref(q);
        return *this;
    }

    integer & operator = (long long int k)
    {
        digit * q = mk(k);
        if (dataptr.d != q) {
            decref(dataptr.d); dataptr.d = incref(q);
        }
        decref(q);
        return *this;
    }

    integer & operator = (unsigned long long int k)
    {
        digit * q = mk(k);
        if (dataptr.d != q) {
            decref(dataptr.d); dataptr.d = incref(q);
        }
        decref(q);
        return *this;
    }

    integer & operator += (const integer & k);
    integer & operator += (const builder & b);
    integer & operator += (int k);
    integer & operator += (unsigned int k);
    integer & operator += (long int k);
    integer & operator += (unsigned long int k);
    integer & operator += (long long int k);
    integer & operator += (unsigned long long int k);

    integer operator + (const integer & k) const;
    integer operator + (const builder & b) const;
    integer operator + (int k) const;
    integer operator + (unsigned int k) const;
    integer operator + (long int k) const;
    integer operator + (unsigned long int k) const;
    integer operator + (long long int k) const;
    integer operator + (unsigned long long int k) const;

    integer & operator -= (const integer & k);
    integer & operator -= (const builder & b);
    integer & operator -= (int k);
    integer & operator -= (unsigned int k);
    integer & operator -= (long int k);
    integer & operator -= (unsigned long int k);
    integer & operator -= (long long int k);
    integer & operator -= (unsigned long long int k);

    integer operator - (const integer & k) const;
    integer operator - (const builder & b) const;
    integer operator - (int k) const;
    integer operator - (unsigned int k) const;
    integer operator - (long int k) const;
    integer operator - (unsigned long int k) const;
    integer operator - (long long int k) const;
    integer operator - (unsigned long long int k) const;

    integer & operator *= (const integer & k);
    integer & operator *= (const builder & b);
    integer & operator *= (int k);
    integer & operator *= (unsigned int k);
    integer & operator *= (long int k);
    integer & operator *= (unsigned long int k);
    integer & operator *= (long long int k);
    integer & operator *= (unsigned long long int k);

    integer operator * (const integer & k) const;
    integer operator * (const builder & b) const;
    integer operator * (int k) const;
    integer operator * (unsigned int k) const;
    integer operator * (long int k) const;
    integer operator * (unsigned long int k) const;
    integer operator * (long long int k) const;
    integer operator * (unsigned long long int k) const;

    integer & operator /= (const integer & k);
    integer & operator /= (const builder & b);
    integer & operator /= (int k);
    integer & operator /= (unsigned int k);
    integer & operator /= (long int k);
    integer & operator /= (unsigned long int k);
    integer & operator /= (long long int k);
    integer & operator /= (unsigned long long int k);

    integer operator / (const integer & k) const;
    integer operator / (const builder & b) const;
    integer operator / (int k) const;
    integer operator / (unsigned int k) const;
    integer operator / (long int k) const;
    integer operator / (unsigned long int k) const;
    integer operator / (long long int k) const;
    integer operator / (unsigned long long int k) const;

    integer & operator %= (const integer & k);
    integer & operator %= (const builder & b);
    integer & operator %= (int k);
    integer & operator %= (unsigned int k);
    integer & operator %= (long int k);
    integer & operator %= (unsigned long int k);
    integer & operator %= (long long int k);
    integer & operator %= (unsigned long long int k);

    integer operator % (const integer & k) const;
    integer operator % (const builder & b) const;
    integer operator % (int k) const;
    integer operator % (unsigned int k) const;
    integer operator % (long int k) const;
    integer operator % (unsigned long int k) const;
    integer operator % (long long int k) const;
    integer operator % (unsigned long long int k) const;

    static integer pow(const integer & a, const integer & b);
    static integer pow(const integer & a,
                        const integer & b, const integer & c);
    static integer fact(integer n);

    integer & operator &= (const integer & k);
    integer & operator &= (const builder & b);
    integer & operator &= (int k);
    integer & operator &= (unsigned int k);
    integer & operator &= (long int k);
    integer & operator &= (unsigned long int k);
    integer & operator &= (long long int k);
    integer & operator &= (unsigned long long int k);

    integer operator & (const integer & k) const;
    integer operator & (const builder & b) const;
    integer operator & (int k) const;
    integer operator & (unsigned int k) const;
    integer operator & (long int k) const;
    integer operator & (unsigned long int k) const;
    integer operator & (long long int k) const;
    integer operator & (unsigned long long int k) const;

    integer & operator |= (const integer & k);
    integer & operator |= (const builder & b);
    integer & operator |= (int k);
    integer & operator |= (unsigned int k);
    integer & operator |= (long int k);
    integer & operator |= (unsigned long int k);
    integer & operator |= (long long int k);
    integer & operator |= (unsigned long long int k);

    integer operator | (const integer & k) const;
    integer operator | (const builder & b) const;
    integer operator | (int k) const;
    integer operator | (unsigned int k) const;
    integer operator | (long int k) const;
    integer operator | (unsigned long int k) const;
    integer operator | (long long int k) const;
    integer operator | (unsigned long long int k) const;

    integer & operator ^= (const integer & k);
    integer & operator ^= (const builder & b);
    integer & operator ^= (int k);
    integer & operator ^= (unsigned int k);
    integer & operator ^= (long int k);
    integer & operator ^= (unsigned long int k);
    integer & operator ^= (long long int k);
    integer & operator ^= (unsigned long long int k);

    integer operator ^ (const integer & k) const;
    integer operator ^ (const builder & b) const;
    integer operator ^ (int k) const;
    integer operator ^ (unsigned int k) const;
    integer operator ^ (long int k) const;
    integer operator ^ (unsigned long int k) const;
    integer operator ^ (long long int k) const;
    integer operator ^ (unsigned long long int k) const;

    integer & operator <<= (const integer & k);
    integer & operator <<= (const builder & b);
    integer & operator <<= (int k);
    integer & operator <<= (unsigned int k);
    integer & operator <<= (long int k);
    integer & operator <<= (unsigned long int k);
    integer & operator <<= (long long int k);
    integer & operator <<= (unsigned long long int k);

    integer operator << (const integer & k) const;
    integer operator << (const builder & b) const;
    integer operator << (int k) const;
    integer operator << (unsigned int k) const;
    integer operator << (long int k) const;
    integer operator << (unsigned long int k) const;
    integer operator << (long long int k) const;
    integer operator << (unsigned long long int k) const;

    integer & operator >>= (const integer & k);
    integer & operator >>= (const builder & b);
    integer & operator >>= (int k);
    integer & operator >>= (unsigned int k);
    integer & operator >>= (long int k);
    integer & operator >>= (unsigned long int k);
    integer & operator >>= (long long int k);
    integer & operator >>= (unsigned long long int k);

    integer operator >> (const integer & k) const;
    integer operator >> (const builder & b) const;
    integer operator >> (int k) const;
    integer operator >> (unsigned int k) const;
    integer operator >> (long int k) const;
    integer operator >> (unsigned long int k) const;
    integer operator >> (long long int k) const;
    integer operator >> (unsigned long long int k) const;

    integer & andnot(const integer & k);
    integer & andnot(const builder & b);
    integer & andnot(int k);
    integer & andnot(unsigned int k);
    integer & andnot(long int k);
    integer & andnot(unsigned long int k);
    integer & andnot(long long int k);
    integer & andnot(unsigned long long int k);

    integer andnot(const integer & k) const;
    integer andnot(const builder & b) const;
    integer andnot(int k) const;
    integer andnot(unsigned int k) const;
    integer andnot(long int k) const;
    integer andnot(unsigned long int k) const;
    integer andnot(long long int k) const;
    integer andnot(unsigned long long int k) const;

    integer & operator + () { return *this; }
    integer operator - () const;
    integer operator ~ () const;
    integer & operator ++();
    integer & operator --();
    integer operator ++(int);
    integer operator --(int);

    int cmp(const digit * d, size_t dn, ssize_t ds) const
    { return builder::cmp_(data(), u_len(), s_len(), d, dn, ds); }

    int cmp(const integer & k) const
    {
        return builder::cmp_(data(), u_len(), s_len(),
                                k.data(), k.u_len(), k.s_len());
    }

    int cmp(const builder & b) const
    { return cmp(b.a, b.n, b.s); }

    int cmp(int k) const;
    int cmp(unsigned int k) const;
    int cmp(long int k) const;
    int cmp(unsigned long int k) const;
    int cmp(long long int k) const;
    int cmp(unsigned long long int k) const;

    bool operator == (const integer & k) const
    { return cmp(k) == 0; }

    bool operator == (const builder & b) const
    { return cmp(b) == 0; }

    bool operator == (int k) const
    { return cmp(k) == 0; }

    bool operator == (unsigned int k) const
    { return cmp(k) == 0; }

    bool operator == (long int k) const
    { return cmp(k) == 0; }

    bool operator == (unsigned long int k) const
    { return cmp(k) == 0; }

    bool operator == (long long int k) const
    { return cmp(k) == 0; }

    bool operator == (unsigned long long int k) const
    { return cmp(k) == 0; }

    bool operator != (const integer & k) const
    { return cmp(k) != 0; }

    bool operator != (const builder & b) const
    { return cmp(b) != 0; }

    bool operator != (int k) const
    { return cmp(k) != 0; }

    bool operator != (unsigned int k) const
    { return cmp(k) != 0; }

    bool operator != (long int k) const
    { return cmp(k) != 0; }

    bool operator != (unsigned long int k) const
    { return cmp(k) != 0; }

    bool operator != (long long int k) const
    { return cmp(k) != 0; }

    bool operator != (unsigned long long int k) const
    { return cmp(k) != 0; }

    bool operator >= (const integer & k) const
    { return cmp(k) >= 0; }

    bool operator >= (const builder & b) const
    { return cmp(b) >= 0; }

    bool operator >= (int k) const
    { return cmp(k) >= 0; }

    bool operator >= (unsigned int k) const
    { return cmp(k) >= 0; }

    bool operator >= (long int k) const
    { return cmp(k) >= 0; }

    bool operator >= (unsigned long int k) const
    { return cmp(k) >= 0; }

    bool operator >= (long long int k) const
    { return cmp(k) >= 0; }

    bool operator >= (unsigned long long int k) const
    { return cmp(k) >= 0; }

    bool operator > (const integer & k) const
    { return cmp(k) > 0; }

    bool operator > (const builder & b) const
    { return cmp(b) > 0; }

    bool operator > (int k) const
    { return cmp(k) > 0; }

    bool operator > (unsigned int k) const
    { return cmp(k) > 0; }

    bool operator > (long int k) const
    { return cmp(k) > 0; }

    bool operator > (unsigned long int k) const
    { return cmp(k) > 0; }

    bool operator > (long long int k) const
    { return cmp(k) > 0; }

    bool operator > (unsigned long long int k) const
    { return cmp(k) > 0; }

    bool operator < (const integer & k) const
    { return cmp(k) < 0; }

    bool operator < (const builder & b) const
    { return cmp(b) < 0; }

    bool operator < (int k) const
    { return cmp(k) < 0; }

    bool operator < (unsigned int k) const
    { return cmp(k) < 0; }

    bool operator < (long int k) const
    { return cmp(k) < 0; }

    bool operator < (unsigned long int k) const
    { return cmp(k) < 0; }

    bool operator < (long long int k) const
    { return cmp(k) < 0; }

    bool operator < (unsigned long long int k) const
    { return cmp(k) < 0; }

    bool operator <= (const integer & k) const
    { return cmp(k) <= 0; }

    bool operator <= (const builder & b) const
    { return cmp(b) <= 0; }

    bool operator <= (int k) const
    { return cmp(k) <= 0; }

    bool operator <= (unsigned int k) const
    { return cmp(k) <= 0; }

    bool operator <= (long int k) const
    { return cmp(k) <= 0; }

    bool operator <= (unsigned long int k) const
    { return cmp(k) <= 0; }

    bool operator <= (long long int k) const
    { return cmp(k) <= 0; }

    bool operator <= (unsigned long long int k) const
    { return cmp(k) <= 0; }

    static int divmod(integer & q, integer & r,
                        const integer & a, const integer & b);

    const digit * data() const { return dataptr.d; }

    ssize_t s_len() const { return Rep()->len; }

    size_t  u_len() const
    { ssize_t k = s_len(); return size_t(k < 0 ? -k : k); }

    // -1 if number is negative, 1 if positive and 0 if number is 0.
    int sign() const
    { ssize_t k = s_len(); return k > 0 ? 1 : k < 0 ? -1 : 0; }

    integer abs() const
    { return integer(data(), u_len(), ssize_t(u_len())); }

private:

	/*
	** this implementation stores the integer in a string object.
	** just so that we can make use of std::string's ref counting
	** etc. Just because we are too lazy to implement our own.
	*/

    struct rep_base {
        _Atomic_word r;
        ssize_t len;
    };

    struct rep : rep_base {
        bool is_leaked() { return r < 0; }
        bool is_shared() { return r > 0; }
        void set_leaked() { r = -1; }
        void set_sharable() { r = 0; }

        void set_length_sign_and_sharable(ssize_t n)
        {
            set_sharable();
            len = n;
        }

        digit * data() throw() { return reinterpret_cast<digit *>(this + 1); }

        digit * grab(const _Alloc & alloc1, const _Alloc & alloc2)
        {
            return (! is_leaked() && alloc1 == alloc2) ?
                        copy() : clone(alloc1);
        }

        static rep *
        create(ssize_t n, ssize_t sign, const _Alloc & alloc);

        void dispose(const _Alloc & a)
        {
            if (__gnu_cxx::__exchange_and_add_dispatch(& r, -1) < 0)
                delete [] (char *)this;
        }

        digit * copy() throw()
        {
            __gnu_cxx::__atomic_add_dispatch(& r, 1);
            return data();
        }

        digit * clone(const _Alloc & a, ssize_t res = 0);

        static rep * getrep(digit * d) { return (rep *)d - 1; }
        static digit * getdig(rep * r) { return (digit *)(r + 1); }
        digit * getdig() { return (digit *)(this + 1); }
    };

    struct _Alloc_hider : _Alloc {
        _Alloc_hider(digit * dd, const _Alloc & a) throw()
        : _Alloc(a), d(dd) { }

        digit * d;
    };

    mutable _Alloc_hider dataptr;

    digit * wdata() const throw() { return dataptr.d; }
    digit * wdata(digit * d) throw() { return dataptr.d = d; }
    rep * Rep() const { return reinterpret_cast<rep *>(wdata()) - 1; }

    //void leak_hard();
    //void leak() { if (! Rep()->is_leaked()) leak_hard(); }

    void decref(digit * & p)
    {
        rep * r = rep::getrep(p);
        p = 0;
        if (__gnu_cxx::__exchange_and_add_dispatch(& r->r, -1) < 0)
            delete [] (char *)r;
    }

    digit * incref(digit * p)
    {
        __gnu_cxx::__atomic_add_dispatch(& rep::getrep(p)->r, 1);
        return p;
    }

    digit * mk();
    digit * mk(const digit * d, size_t dn, ssize_t ds);
    digit * mk(const digit * d, ssize_t ds);
    digit * mk(const builder & b);
    digit * mk(const decimal & d);
    digit * mk(int k);
    digit * mk(unsigned int k);
    digit * mk(long int k);
    digit * mk(unsigned long int k);
    digit * mk(long long int k);
    digit * mk(unsigned long long int k);
    digit * mk(float k);
    digit * mk(double k);
    digit * mk(long double k);
    digit * assign(builder & b);

    integer(const digit * d, size_t dn, size_t ds)
        : dataptr(mk(d, dn, ds), _Alloc())
    { }
};

};

inline alf::integer abs(alf::integer x) { return x.abs(); }

inline alf::integer pow(alf::integer a, alf::integer b)
{ return alf::integer::pow(a, b); }

inline alf::integer pow(alf::integer a, alf::integer b, alf::integer c)
{ return alf::integer::pow(a, b, c); }

inline int divmod(alf::integer & q, alf::integer & r,
                    const alf::integer & a, const alf::integer & b)
{ return alf::integer::divmod(q, r, a, b); }

inline alf::integer fact(alf::integer x)
{ return alf::integer::fact(x); }

#endif
